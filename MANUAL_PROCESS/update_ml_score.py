#import elasticsearch
import time
from MANUAL_PROCESS.read_process_index import read_projects
from FILTER.profanity_lang_filter import get_sentiment_score
from FILTER.get_classifier_score import get_score, get_class_label_n_score
from CONNECTIONS.ESConnection import *


#es = elasticsearch.Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")

def update_ml_score():
    projects = read_projects()
    for p in projects:
        index_name = p[0]+"-"+p[1]
        page = es.search(
            index=index_name,
            scroll='60s',
            size=100)

        sid = page['_scroll_id']
        scroll_size = page['hits']['total']
        total_records = page['hits']['total']
        print("----", scroll_size, total_records)

        get_ml_score(index_name, page, p[2])

        while (scroll_size > 0):
            time.sleep(10)
            print(index_name , " Scrolling...")
            try:
                page = es.scroll(scroll_id=sid, scroll='60s')
                sid = page['_scroll_id']
                scroll_size = len(page['hits']['hits'])

                print("scroll size: " + str(scroll_size))
                if scroll_size > 0:
                    get_ml_score(index_name, page, p[2])
            except:
                continue

def get_ml_score(index_name, page, search_terms):
    print(len(page['hits']['hits']))
    for item in page['hits']['hits']:
        guid = item['_id']
        #print("===item===",guid, item)

        if item['_source']:

            if "ml_aggregrate" in item['_source']:
                ml_aggr = str(item['_source']['ml_aggregrate'])
            else:
                ml_aggr = ''

            if ml_aggr:
                sentiment_score = get_sentiment_score(ml_aggr)
                print("******-----------sentiment score-------------*****", sentiment_score)

                ## getting classfifer scores ##
                bayesian_score = get_score("bayesian", index_name, ml_aggr)
                tree_score = get_score("tree", index_name, ml_aggr)
                logistic_score = get_score("logistic", index_name, ml_aggr) ## updating to 'score_nn'

                score_subject, classification_label = get_class_label_n_score(index_name, ml_aggr)
                print("score_subject, classification_label ", score_subject, classification_label)

                if classification_label == "subject":
                    sent_2_vec_score = 1
                else:
                    sent_2_vec_score = 0

                ##calculating ml score
                print("------======-------", classification_label, sentiment_score, bayesian_score, tree_score, logistic_score)
                ml_score = float(sentiment_score) + float(bayesian_score) + float(tree_score) + \
                           float(sent_2_vec_score) + float(logistic_score)

                print("------ml_score-----", ml_score)

                data = item['_source']
                data.update({'score_emotion': sentiment_score})
                data.update({'score_naive': bayesian_score})
                data.update({'score_nn': logistic_score})
                data.update({'score_tree': tree_score})
                data.update({'score_ml' : ml_score})
                data.update({'predict02': str(classification_label)})
                data.update({'score_medic': "{:.2f}".format(score_subject)})
                print("===data===", data)
                print("===guid===", guid)

                try:
                    es.update(index=index_name, doc_type="processed", body={"doc": data}, id=guid)
                except elasticsearch.exceptions.NotFoundError:
                    continue
                except elasticsearch.exceptions.ConnectionTimeout:
                    continue
                except:
                    continue

            else:
                print("No Content Found")
                continue



update_ml_score()