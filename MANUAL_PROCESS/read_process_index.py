# from elasticsearch import Elasticsearch
#
# es = Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
from CONNECTIONS.ESConnection import *

def read_projects():
    doc = {"query": { "bool": { "filter": { "term": { "isactive": True }  } } } }
    try:
        resp_data = es.search(index="crawling-pattern", body=doc, request_timeout=100)
    except:
        return

    projects = []
    for res in resp_data["hits"]["hits"]:
        user = res['_source']['user']
        project = res['_source']['project']
        keywords = res['_source']['keyword-search']
        contexts = res['_source']['context-search']

        key_contexts = []
        key_contexts.extend([i.lower().replace('"','').strip() for i in keywords.split(",")])
        key_contexts.extend([i.lower().replace('"','').strip() for i in contexts.split(",")])

        projects.append((user, project, key_contexts))
    print(projects)
    return projects


def read_big_projects():
    doc = {"query": { "bool": { "filter": { "term": { "isactive": True }  } } } }
    try:
        resp_data = es.search(index="crawling-pattern", body=doc, request_timeout=100)
    except:
        return

    projects = []
    for res in resp_data["hits"]["hits"]:
        user = res['_source']['user']
        project = res['_source']['project']
        keywords = res['_source']['keyword-search']
        contexts = res['_source']['context-search']

        key_contexts = []
        key_contexts.extend([i.lower().replace('"','').strip() for i in keywords.split(",")])
        key_contexts.extend([i.lower().replace('"','').strip() for i in contexts.split(",")])

        try:
            records = es.count(index=user+"-"+project)['count']
        except:
            records = 0

        if records >= 20000:
            projects.append((user, project, key_contexts))

    return projects