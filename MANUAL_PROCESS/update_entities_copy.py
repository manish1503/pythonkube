import datetime
# import elasticsearch
from SCHEDULE.get_logger import get_log_object
from CONNECTIONS.ESConnection import *
# es = elasticsearch.Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
recent          = "now-8w/w" ##120days or 16weeks
recent_update   = "" ##dynamic lookup for row
log_path        = "../../LOGS"
current_date    = datetime.datetime.now().date()

def update_entity(index):
    """"updating entities for records"""
    scroll_records(index)
    """ sme updation completed """
    return True


def scroll_records(index_name):
    """scrolling index"""

    doc = {"_source": {"includes" : ["entity_person", "entity_org", "entity_location"]},
           "query": {"bool": {"must": [{"range": {"date": {"gte": recent, "lt": "now/d"}}}]}}}
    page = es.search(
        index=index_name,
        body=doc,
        scroll='90m', size=100, sort="date:desc")

    sid = page['_scroll_id']
    scroll_size = page['hits']['total']

    update_entities(index_name, page)

    records_processed = 100
    while (scroll_size > 0):
        try:
            page = es.scroll(scroll_id=sid, scroll='100m')
            sid = page['_scroll_id']
            scroll_size = len(page['hits']['hits'])
            if scroll_size > 0:
                records_processed += 100
                update_entities(index_name, page)
            else:
                break
        except:
            continue

def update_entities(index_name, page):
    """ calculating and updating sme scores """

    for item in page['hits']['hits']:
        guid = item['_id']
        print("guid : ", guid)
        if item['_source']:
            data = item['_source']
            data.update({'modifiedDate': current_date})
            data.update({'modifiedBy': 'update_entities_copy.py'})

            if "entity_location" in item['_source']:
                entity_location = str(item['_source']['entity_location'])
                if len(entity_location.split(",")) > 10:
                    data.update({'entity_location' : entity_location.split(",")[:10]})
            if "entity_org" in item['_source']:
                entity_org = str(item['_source']['entity_org'])
                if len(entity_org.split(",")) > 10:
                    data.update({'entity_org' : entity_org.split(",")[:10]})
            if "entity_person" in item['_source']:
                entity_person = str(item['_source']['entity_person'])
                if len(entity_person.split(",")) > 10:
                    data.update({'entity_person' : entity_person.split(",")[:10]})
            try:
                es.update(index=index_name, doc_type="processed", body={"doc": data}, id=guid)
            except elasticsearch.exceptions.NotFoundError:
                continue
            except elasticsearch.exceptions.ConnectionTimeout:
                continue
            except:
                continue
        else:
            es.delete(index=index_name, doc_type="processed", id=guid)


update_entity("boston-med3d")