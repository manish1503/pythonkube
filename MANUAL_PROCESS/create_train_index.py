# import elasticsearch
from CONNECTIONS.ESConnection import *
import time
# from MANUAL_PROCESS.read_process_index import read_projects
# es = elasticsearch.Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")

def create_temp_index():
    #projects = read_projects()
    projects = [('boston', 'med3d', '3d Glassless Autostereoscopy')]

    total_records = 10000
    compn_records = 0

    for p in projects:
        index_name = p[0] + "-"+ p[1]
        db_rec = get_source_count(index_name, "Diffbot")
        wh_rec = get_source_count(index_name, "Webhose")

        compn_records += 0 if (5000 - int(db_rec)) < 0 else (5000 - int(db_rec))
        compn_records += 0 if (5000 - int(wh_rec)) < 0 else (5000 - int(wh_rec))
        tw_rec = total_records + compn_records
        print("diffbot : ",db_rec, "\b webhose : ",wh_rec, "\n twitter : ",tw_rec)

        '''creating temp training index'''
        create_index("temp-training-"+index_name)

        '''update sme 4-5 records'''
        dbu_rec = get_sme_five_count(index_name)
        if dbu_rec:
            scroll_index_source(index_name, {"query": {"range": {"sme": {"gte": 4, "lte": 5}}}}, int(dbu_rec))

        '''scroll indexes from sources'''
        if db_rec:
            scroll_index_source(index_name, {
                "query": {
                    "bool": {"must": [{"match": {"type": "Diffbot"}}], "filter": [{"range": {"sme": {"lte": 3}}}]}}},
                                int(db_rec))
        if wh_rec:
            scroll_index_source(index_name, {
                "query": {
                    "bool": {"must": [{"match": {"type": "Webhose"}}], "filter": [{"range": {"sme": {"lte": 3}}}]}}},
                                int(wh_rec))
        if tw_rec:
            scroll_index_source(index_name, {
                "query": {
                    "bool": {"must": [{"match": {"type": "Twitter"}}], "filter": [{"range": {"sme": {"lte": 3}}}]}}},
                                int(tw_rec))


def get_source_count(index, source):
    doc = {"query":{"bool":{"must":[{"match":{"type":source}}], "filter": [{"range":{"sme":{"lte": 3}}}]}}}
    try:
        res = es.count(index=index, body=doc)
        return res['count']
    except:
        return 0

def get_sme_five_count(index):
    doc = {"query": {"range": {"sme": {"gte": 4, "lte": 5}}}}
    try:
        res = es.count(index=index, body=doc)
        return res['count']
    except:
        return 0

def scroll_index_source(index_name, doc, size):
    """scrolling index"""
    page = es.search(
        index=index_name,
        body=doc,
        scroll='90s',
        size=size, sort="date:desc")
    sid = page['_scroll_id']
    scroll_size = page['hits']['total']
    total_records = page['hits']['total']

    print("----", scroll_size, total_records)
    prepare_index(index_name, page)

    while (scroll_size > 0):
        time.sleep(15)
        print(index_name , " Scrolling...")
        try:
            page = es.scroll(scroll_id=sid, scroll='120s')
            sid = page['_scroll_id']
            scroll_size = len(page['hits']['hits'])

            print("scroll size: " + str(scroll_size))
            if scroll_size > 0:
                prepare_index(index_name, page)
        except:
            continue


def prepare_index(index_name, page):
    print(len(page['hits']['hits']))
    for item in page['hits']['hits']:
        try:
            guid = item['_id']
            print("===item id===",guid)
            try:
                res = es.index(index="temp-training-"+index_name, doc_type="train", id=guid, body=item['_source'])
                print(res['created'])
            except elasticsearch.exceptions.NotFoundError:
                continue
            except elasticsearch.exceptions.ConnectionTimeout:
                continue
            except:
                continue
        except:
            print("Error")
            continue

def create_index(index):
    body = { "settings": { "index": { "number_of_shards": 1, "number_of_replicas": 2 } } }

    if es.indices.exists(index=index):
        return True
    else:
        try:
            es.indices.create(index=index, ignore=400, body=body)
        except:
            print("Error in index creation")
        return True

create_temp_index()
