#import elasticsearch
import time
from nltk import word_tokenize, pos_tag, ne_chunk, tree2conlltags
from CONNECTIONS.ESConnection import *
from MANUAL_PROCESS.read_process_index import read_projects
#es = elasticsearch.Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")

def update_entities():
    #projects = read_projects()
    projects = [('sergio', 'briight', ['briight', 'device diabetes'])]#, ('sergio', 'luminary', ['"Naval Ravikant"', 'block chain', 'internet 3.0']), ('pritesh', 'soccer', ['soccer', 'Barcelona']), ('ankit', 'cricket', ['cricket', 'bat', 'cricket equipment', 'hat trick', 'maiden over', 'stump', 'bowl', 'score runs'])]
    for p in projects:
        index_name = p[0]+"-"+p[1]
        page = es.search(
            index=index_name,
            scroll='60s',
            size=100,
            request_timeout = 100)

        sid = page['_scroll_id']
        scroll_size = page['hits']['total']
        total_records = page['hits']['total']
        print("----", scroll_size, total_records)

        scroll_records(index_name, page)

        while (scroll_size > 0):
            print(index_name, "Scrolling...")
            try:
                page = es.scroll(scroll_id=sid, scroll='60s',request_timeout=100)
                sid = page['_scroll_id']
                scroll_size = len(page['hits']['hits'])

                print("scroll size: " + str(scroll_size))
                if scroll_size > 0:
                    scroll_records(index_name, page)
            except:
                continue


def scroll_records(index_name, page):
    empty_source = []

    print(len(page['hits']['hits']))
    for item in page['hits']['hits']:
        guid = item['_id']

        print("===item(guid)===",guid)

        if item['_source']:
            #print("=============",item['_source'])

            try:
                raw_text = str(item['_source']['description'])
            except:
                raw_text = str(item['_source']['text'])

            if raw_text:
                data = item['_source']
                person, org, location = get_entity(raw_text)

                # if person:
                #     data.update({'entity_person': person})
                # if org:
                #     data.update({'entity_org': org})
                # if location:
                #     data.update({'entity_location': location})

                #print("===data===", data)
                data.update({'entity_person': person})
                data.update({'entity_org': org})
                data.update({'entity_location': location})

                try:
                    es.update(index=index_name, doc_type="processed", body={"doc": data}, id=guid,request_timeout=100)
                except elasticsearch.exceptions.NotFoundError:
                    continue
                except elasticsearch.exceptions.ConnectionTimeout:
                    continue
                except:
                    continue

        else:
            empty_source.append(guid)
            print("No Content Found")
            #continue
    print(empty_source)


def get_entity(sample):
    location = []
    person   = []
    org      = []

    ne_tree = ne_chunk(pos_tag(word_tokenize(sample)))
    iob_tagged = tree2conlltags(ne_tree)

    for i in iob_tagged:
        if i[2] != "O":
            try:
                val = i[2]#.split("-",1)[1]
                if val == "B-PERSON":
                    if i[0] not in person:
                        person.append(i[0])
                elif val == "B-ORGANIZATION":
                    if i[0] not in org:
                        org.append(i[0])
                elif val in ["B-GPE", "B-LOCATION", "B-FACILITY"]:
                    if i[0] not in location:
                        location.append(i[0])
                elif val in ["DATE","TIME","MONEY","PERCENT"]:
                    pass
                else:
                    pass
            except:
                pass

    person = ", ".join(person)
    location = ", ".join(location)
    org = ", ".join(org)
    print("person : ",person, "\n location : ", location, "\n org : ", org)
    return person, org, location


update_entities()