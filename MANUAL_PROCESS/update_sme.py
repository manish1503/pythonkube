# import elasticsearch
from CONNECTIONS.ESConnection import *
from FILTER.get_context import get_context_terms
from MANUAL_PROCESS.read_process_index import read_projects

# es = elasticsearch.Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")

def update_sme():
    projects = read_projects()
    for p in projects:
        index_name = p[0]+"-"+p[1]
        page = es.search(
            index=index_name,
            scroll='60s',
            size=100)

        sid = page['_scroll_id']
        scroll_size = page['hits']['total']
        total_records = page['hits']['total']
        print("----", scroll_size, total_records)

        get_sme_score(index_name, page, p[2])

        while (scroll_size > 0):
            print(index_name , " Scrolling...")
            try:
                page = es.scroll(scroll_id=sid, scroll='60s')
                sid = page['_scroll_id']
                scroll_size = len(page['hits']['hits'])

                print("scroll size: " + str(scroll_size))
                if scroll_size > 0:
                    get_sme_score(index_name, page, p[2])
            except:
                continue

def get_sme_score(index_name, page, search_terms):
    print(len(page['hits']['hits']))
    for item in page['hits']['hits']:
        guid = item['_id']
        #print("===item===",guid, item)

        if item['_source']:
            sme = 0
            if "ml_aggregrate" in item['_source']:
                ml_aggr = str(item['_source']['ml_aggregrate'])
            else:
                ml_aggr = ''

            ##diffbot exclude##
            if "type" in item['_source']:
              source_type = str(item['_source']['type'])
              if source_type.lower == "dropbox":
                  sme = 5
                  continue

            if ml_aggr:
                ### term search ###
                total_terms = len(search_terms)
                for term in search_terms:
                    if term in ml_aggr:
                        total_terms -= 1
                if total_terms <= 1:
                    sme = 4
                    print("search terms..", search_terms)

                if sme != 4:
                    ###contenxt search
                    similar_terms = get_context_terms(search_terms[0], index_name)
                    similar_terms.extend(search_terms)
                    if similar_terms:
                        total_similar_terms = len(similar_terms)
                        for sim_term in similar_terms:
                            if sim_term in ml_aggr:
                                total_similar_terms -= 1
                        if total_similar_terms <= 1:
                            sme = 4
                            print("similar terms : ",similar_terms)

                if sme == 4:
                    print("------sme-----", sme, guid, source_type)

                    data = item['_source']
                    data.update({'sme' : sme})
                    #print("===data===", data)

                    try:
                        es.update(index=index_name, doc_type="processed", body={"doc": data}, id=guid)
                        print("record updated.")
                    except elasticsearch.exceptions.NotFoundError:
                        continue
                    except elasticsearch.exceptions.ConnectionTimeout:
                        continue
                    except:
                        continue

            else:
                print("No Content Found")
                continue

update_sme()