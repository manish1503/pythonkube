import datetime
import threading
from math import ceil
import re
# import elasticsearch

from FILTER.get_context import get_context_terms
from SCHEDULE.get_logger import get_log_object
from CONNECTIONS.ESConnection import *

# es = elasticsearch.Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
recent          = "now-16w/w" ##120days or 16weeks
recent_update   = "" ##dynamic lookup for row
log_path        = "../LOGS"
current_date    = datetime.datetime.now().date()

def one_update_sme(projects):
    """" updating sme values for records """
    for p in projects:
        index_name      = p[0] + "-" + p[1]
        search_terms    = p[2]
        similar_terms   = get_context_terms(p[2][0])  #to call generic model get_context_terms(p[2][0], index_name) get_context_terms(p[2][0], index_name)
        scroll_records(index_name, search_terms, similar_terms)
        # t = threading.Thread(target=scroll_records, args=(index_name, search_terms, similar_terms))
        # t.start()
    """ sme updation completed """
    return True


def scroll_records(index_name, search_terms, similar_terms):
    """scrolling index"""
    if index_name:
        log = get_log_object(log_path, "one_pt_one-" + index_name)
        log.fatal("main and context terms : %s, similar terms : %s", search_terms, similar_terms)
        doc = {"_source": {"includes" : ["ml_aggregrate", "type", "sme"]},"query": {"bool": {"must": [{"range": {"date": {"gte": recent, "lt": "now/d"}}},
                                            {"range": {"sme": {"lte": 5}}}]}}}
        page = es.search(
            index=index_name,
            body=doc,
            scroll='90m', size=10, sort="date:desc",request_timeout=60)

        sid = page['_scroll_id']
        scroll_size = page['hits']['total']
        total_records = page['hits']['total']
        log.info("scroll_size : %s, total_records : %s", scroll_size, total_records)

        update_sme_score(index_name, page, search_terms, similar_terms, log)

        records_processed = 10
        while (scroll_size > 0):
            try:
                page = es.scroll(scroll_id=sid, scroll='100m')
                sid = page['_scroll_id']
                scroll_size = len(page['hits']['hits'])
                if scroll_size > 0:
                    records_processed += 10
                    log.fatal("Number of Records Processed : %s", records_processed)
                    update_sme_score(index_name, page, search_terms, similar_terms, log)
                else:
                    break
            except:
                continue

def update_sme_score(index_name, page, search_terms, similar_terms, log):
    """ calculating and updating sme scores """
    total_terms = []
    total_terms.extend(search_terms)
    total_terms.extend(similar_terms)

    sme_four_count = 0
    sme_three_count = 0
    sme_two_count = 0

    for item in page['hits']['hits']:
        guid = item['_id']

        total_terms_size = len(total_terms)
        actual_total_terms_size = len(total_terms)

        # log.info("total terms size : %s",total_terms_size)
        # log.info("actual total terms size : %s",actual_total_terms_size)

        high_value = 1 / 3 * total_terms_size
        low_value = 1


        if item['_source']:

            ''' sme-5 excluded '''
            if "sme" in item['_source']:
                sme_val = str(item['_source']['sme'])
                if sme_val in [5,"5"]:
                    continue

            ''' dropbox excluded '''
            if "type" in item['_source']:
                source_type = str(item['_source']['type'])
                if source_type in ["Dropbox", "dropbox", "bing", "Bing"]:
                    continue

            ''' status as sme excluded '''
            if "status" in item['_source']:
                sme_status = str(item['_source']['status'])
                if sme_status.lower() == "sme":
                    continue

            if "ml_aggregrate" in item['_source']:
                ml_aggr = str(item['_source']['ml_aggregrate'])
            else:
                ml_aggr = ''

            if ml_aggr:
                #log.debug("ml_aggregate : %s", ml_aggr)
                ml_aggr_size = len(ml_aggr.split())
                if ml_aggr_size <= 20:
                    high_value = high_value*2

                ''' term search '''
                for term in search_terms:
                    term_match = re.findall("\\b"+term+"\\b", ml_aggr)
                    if term_match: #re.match("\\b"+term+"\\b", ml_aggr): #if term in ml_aggr
                        #log.fatal("search term : %s, %s",term, len(term_match))
                        total_terms_size -= high_value
                        #log.info(total_terms_size)

                ''' contenxt search '''
                for sim_term in similar_terms:
                    sim_match = re.findall("\\b"+sim_term+"\\b", ml_aggr)
                    if sim_match:#sim_term in ml_aggr:
                        #log.fatal("similar term : %s, %s",sim_term, len(sim_match))
                        total_terms_size -= low_value
                        #log.info(total_terms_size)

                #log.info("high value : %s, low value : %s, total terms size : %s", high_value, low_value, total_terms_size)

                if total_terms_size <= ceil(actual_total_terms_size*30/100):  ##closest integer [ceil]
                    sme = 4
                    sme_four_count += 1
                elif ceil(actual_total_terms_size*30/100) <= ceil(total_terms_size) <= ceil(actual_total_terms_size*60/100):
                    sme = 3
                    sme_three_count += 1
                elif ceil(actual_total_terms_size*60/100) <= ceil(total_terms_size) <= ceil(actual_total_terms_size*80/100):
                    sme = 2
                    sme_two_count += 1
                else:
                    sme = 1

                if sme == 4:
                    log.fatal("sme calculated = : %s",sme)

                data = item['_source']
                data.update({'sme' : sme})
                data.update({'modifiedDate' : current_date})
                data.update({'modifiedBy': 'one_pt_one_sme_four_n_five.py'})

                try:
                    es.update(index=index_name, doc_type="processed", body={"doc": data}, id=guid)
                    #log.info("record updated.")
                except elasticsearch.exceptions.NotFoundError:
                    continue
                except elasticsearch.exceptions.ConnectionTimeout:
                    continue
                except:
                    continue

            else:
                log.warning("No Content Found")
                continue

    log.info("sme_four :%s, sme_three :%s, sme_two: %s", sme_four_count, sme_three_count, sme_two_count)
