import elasticsearch
from gensim.models import Word2Vec #doc2vec
import os
import datetime
import time
import threading
import multiprocessing
import matplotlib
matplotlib.use("Agg")
from CONNECTIONS.ESConnection import *
from SCHEDULE.get_logger import get_log_object
from CLASSIFIER.utils import filter_stop_words

w2v_dimension = 50
w2v_windown = 5
num_workers = multiprocessing.cpu_count()
doc_iter = 30

log_path   = "../LOGS"
model_path = "../MODEL"

# es = elasticsearch.Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
today_date  = datetime.datetime.now().date()
words_w2v           = []

##creating w2v similarity model

def three_pt_zero_create_w2v_model(projects):
    """training model for big projects"""
    for p in projects:
        index   = p[0]+ "-" +p[1]
        scroll_index(index)
        words_w2v = []
        global  words_w2v
    ''' model training completes '''
    return True


def scan_shard(es, index, doc, query, routing, func,shard):
    """
    Scans a specific shard by routing numberץ
    """
    log = get_log_object(log_path, "three_pt_zero_w2v-" + index)
    page = es.search(
        index=index, doc_type="processed",
        scroll='10m', body=query,from_=0,
        size=100, request_timeout=30,routing=routing)

    sid = page['_scroll_id']
    scroll_size = page['hits']['total']

    log.info("scrolling index : %s, scroll_size: %s", index, scroll_size)
    prepare_labeled_data(page)
    records_processed = 100
    while (scroll_size > records_processed):
        if records_processed % 100000 == 0:
            time.sleep(10)
        try:
            es.cluster.health(wait_for_status='yellow', request_timeout=1000)
            page = es.scroll(scroll_id=sid, scroll='10m')
        except elasticsearch.exceptions.ConnectionTimeout:
            log.error("Elasticsearch.exceptions.connectionTimeout")
            es.indices.clear_cache(index=index)
            page = es.search(
                index=index, doc_type="processed",
                scroll='10m', body=query, from_=records_processed,
                size=100, request_timeout=30, routing=routing)
        except elasticsearch.exceptions.NotFoundError:
            print("elastisearch.exception.NotFondError")
            es.indices.clear_cache(index=index)
            page = es.search(
                    index=index, doc_type="processed",
                    scroll='10m', body=query,from_=records_processed,
                    size=100, request_timeout=30,routing=routing)
        except elasticsearch.exceptions.TransportError:
            log.error("Elasticsearch.exceptions.TransportError")
            es.indices.clear_cache(index=index)
        except elasticsearch.exceptions.__all__:
            log.error("Some Error has occured")
            es.indices.clear_cache(index=index)
        except Exception as e:
            continue
        sid = page['_scroll_id']
        scroll_length = len(page['hits']['hits'])

        if scroll_length > 0:
            records_processed += scroll_length
            log.fatal("Number of Records Processed : %s scroll length : %s Total record : %s shard : %s and routing : %s ", records_processed, scroll_length, scroll_size, shard, routing)
            prepare_labeled_data(page)

def scroll_index(index_name):
    log = get_log_object(log_path, "three_pt_zero_w2v-" + index_name)
    doc = {"_source": {"includes": ["ml_aggregrate", "sme"]}}
    shards_info = es.search_shards(index=index_name,doc_type='processed')
    number_of_shards = len(shards_info['shards'])
    print(number_of_shards)

    shards_to_routing ={}
    i = 0
    count = 1
    while len(shards_to_routing.keys()) < number_of_shards :
        result = es.search_shards(index=index_name,doc_type='processed',routing=i)
        shard_number = result['shards'][0][0]['shard']
        if shard_number not in shards_to_routing:
            shards_to_routing[shard_number]= i
        i +=1

    print(shards_to_routing)
    for shard, routing in shards_to_routing.items():
        scan_shard(es, index_name, 'processed', doc, routing, lambda doc: print(doc),shard)

    log.info("Total fetched data from %s is %s ", index_name, len(words_w2v))
    log.fatal("*!*!*!*!*!* building sentence and word similarity models *!*!*!*!*!*")
    build_train_sent_n_word_vec_model(words_w2v, index_name, log)
    log.info("labeled data prepared for : %s", index_name)

def prepare_labeled_data(page):
    """preparing labled data"""
    for item in page['hits']['hits']:
        ml_aggr = ''

        if item['_source']:
            if "ml_aggregrate" in item['_source']:
                ml_aggr = str(item['_source']['ml_aggregrate'])

        mlwords = filter_stop_words(ml_aggr)
        words_w2v.append(mlwords)


def build_train_sent_n_word_vec_model(words_w2v, project_name, log):
    """training and storing model in disk"""
    log.fatal("*!*!*!*!*!*!*Creating Word2Vec Model*!*!*!*!*!*!* Training Start")
    model_w2v = Word2Vec(words_w2v, size=w2v_dimension, window=w2v_windown, workers=num_workers, iter=doc_iter)
    #doc2vec.Word2Vec(words_w2v, size=w2v_dimension, window=w2v_windown, workers=num_workers, iter=doc_iter)
    log.info("word2vec processed on all recent data")
    model_name = project_name+'_w2v.model'

    for f in next(os.walk(model_path))[2]:
        if f.startswith(project_name+"_w2v"):
            src = model_path + '/' + f
            log.info("renaming files : %s", src)
            if os.path.exists(src):
                dest = model_path + "/" + str(today_date) + "_" + f
                try:
                    os.rename(src, dest)
                except:
                    os.remove(dest)
                    os.rename(src, dest)

    model_w2v.save(os.path.join(model_path, model_name))

    log.info("Model files are created, Model is Ready : %s", str(project_name))
    return True
