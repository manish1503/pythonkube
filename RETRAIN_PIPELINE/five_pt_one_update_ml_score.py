# import elasticsearch
import threading
import datetime
from FILTER.get_classifier_score import get_scores
from FILTER.profanity_lang_filter import get_sentiment_score
from FILTER.get_classifier_score import load_model
from SCHEDULE.get_logger import get_log_object
from CONNECTIONS.ESConnection import *

# es = elasticsearch.Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
log_path    = "../LOGS"
recent      = "now-16w/w" #120days
current_date = datetime.datetime.now().date()
##batch processing of populating ml_scores

def five_update_ml_score(projects):
    """ getting active large projects """
    #projects = read_big_projects()
    for p in projects:
        index_name = p[0]+"-"+p[1]
        process_records(index_name)
        # t = threading.Thread(target=process_records, args=(index_name,))
        # t.start()
    return True

def process_records(index_name):
    """scrolling index to process records"""
    load_model(index_name)
    log = get_log_object(log_path, "five_pt_one_update_ml_score-"+index_name)
    doc = {"_source": {"includes" : ["ml_aggregrate"]}, "query": {"bool": {"must": [{"range": {"date": {"gte": recent,"lt": "now/d"}}},{"range": {"sme": {"lte": 5}}}]}}}
    page = es.search(
        index=index_name,
        body=doc,
        scroll='20m',
        size=10, sort="date:desc")

    sid = page['_scroll_id']
    scroll_size = page['hits']['total']
    total_records = page['hits']['total']
    log.fatal("total_records : %s", total_records)

    update_scores(index_name, page, log)
    recrods_updated = 10

    while (scroll_size > 0):
        try:
            page = es.scroll(scroll_id=sid, scroll='20m')
            sid = page['_scroll_id']
            scroll_size = len(page['hits']['hits'])

            recrods_updated += scroll_size
            log.fatal("records updated : %s", recrods_updated)

            if scroll_size > 0:
                update_scores(index_name, page, log)
        except Exception as e:
            log.error(e.args)
            log.error("Error In Scroll API")
            continue


def update_scores(index_name, page, log):
    """updating ml_scores"""
    for item in page['hits']['hits']:
        guid = item['_id']
        if item['_source']:

            if "ml_aggregrate" in item['_source']:
                ml_aggr = str(item['_source']['ml_aggregrate'])
            else:
                ml_aggr = ''

            if ml_aggr:
                sentiment_score = get_sentiment_score(ml_aggr)

                ''' getting classfifer scores '''
                tree_score, logistic_score, bayesian_score, score_subject, classification_label = get_scores(ml_aggr)
                # bayesian_score  = get_score_local("bayesian", index_name, ml_aggr)
                # tree_score      = get_score_local("tree", index_name, ml_aggr)
                # logistic_score  = get_score_local("logistic", index_name, ml_aggr) ## updating to 'score_nn'
                # bool, score_subject, classification_label = get_class_label_n_score_local(index_name, ml_aggr)

                # log.info("bayesian_score :%s, tree_score: %s, logistic_score :%s", bayesian_score, tree_score, logistic_score)
                # log.info("sentiment score : %s, score_subject :%s, classification_label :%s", sentiment_score, score_subject, classification_label)

                sent_2_vec_score = 1 if classification_label == "subject" else 0
                
                ''' calculating ml-score '''
                ml_score = float(sentiment_score) + float(bayesian_score) + float(tree_score) + \
                           float(sent_2_vec_score) + float(logistic_score)

                data = item['_source']
                data.update({'score_emotion': sentiment_score})
                data.update({'score_naive': bayesian_score})
                data.update({'score_nn': logistic_score})
                data.update({'score_tree': tree_score})
                data.update({'score_ml' : ml_score})
                data.update({'predict02': str(classification_label)})
                data.update({'score_medic': "{:.2f}".format(score_subject)})
                data.update({'modifiedDate': current_date})
                data.update({'modifiedBy': 'five_pt_one_update_ml_score.py'})
                #log.fatal("guid : %s, score_ml : %s", guid, ml_score)

                try:
                    es.update(index=index_name, doc_type="processed", body={"doc": data}, id=guid)
                except elasticsearch.exceptions.NotFoundError:
                    log.error("elasticsearch.exceptions.NotFoundError")
                    continue
                except elasticsearch.exceptions.ConnectionTimeout:
                    log.error("elasticsearch.exceptions.ConnectionTimeout")
                    continue
                except Exception as e:
                    #log.debug(e.args)
                    continue
            else:
                log.warning("No Content Found")
                continue
        else:
            log.warning("No Data for guid :%s", guid)
