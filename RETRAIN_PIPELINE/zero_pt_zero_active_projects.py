# from elasticsearch import Elasticsearch
from SCHEDULE.get_logger import get_log_object
from DATABASE.processed_index_db import *
from FILTER.utils import dict_comparison, \
                         update_modified_processed_db, update_processed_index_db
from CONNECTIONS.ESConnection import *
# es = Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
log = get_log_object("../LOGS", "zero_pt_zero_active_projects")
retrain_size = 15000

def read_big_projects():
    doc = {"query": { "bool": { "filter": { "term": { "isactive": True }  } } } }
    try:
        resp_data = es.search(index="crawling-pattern", body=doc)
    except:
        return

    projects = []
    for res in resp_data["hits"]["hits"]:
        user = res['_source']['user']
        project = res['_source']['project']
        keywords = res['_source']['keyword-search']
        contexts = res['_source']['context-search']

        key_contexts = []
        key_contexts.extend([i.lower().replace('"','').strip() for i in keywords.split(",") if i])
        key_contexts.extend([i.lower().replace('"','').strip() for i in contexts.split(",") if i])

        records, size = get_index_size(user + "-" + project)

        if records >= retrain_size:
            projects.append((user, project, key_contexts))

    return projects


##-----------------------
def get_index_size(index):
    try:
        log.info("getting size of index : %s", index)
        res = es.cat.indices(index=index)
        log.info(res)
        data = res.split()
        return int(data[6]), data[8]
    except:
        return int(0), '0mb'

##-----------------------
def get_index_db(indexes):
    live_processed_db = {}

    for i in indexes:
        no_records, size = get_index_size(i[0]+"-"+i[1])
        if int(no_records) > 0:
            live_processed_db.update({i[0]+"-"+i[1] : [int(no_records), size]})

    return live_processed_db

##-----------------------
def get_active_projects():
    doc = {"query": {"bool": {"filter": {"term": {"isactive": True } } } } }
    try: resp_data = es.search(index="crawling-pattern", size=50, body=doc,  request_timeout=100)
    except: return

    log.info("searching active projects")
    indexes = []
    for res in resp_data["hits"]["hits"]:
        user    = res['_source']['user']
        project = res['_source']['project']

        records, size = get_index_size(user + "-" + project)
        if records >= retrain_size:
            indexes.append((user, project))

    processed_db = get_index_db(indexes)
    log.info("list of indexes : %s, and processed db : %s", indexes, processed_db)

    return processed_db


##-----------------------
def zero_projects_to_retrain():
    """
    Getting Updates through Comparison
    :return: True
    """
    d1 = get_active_projects()
    d2 = processed_index_db
    add, remove, modified = dict_comparison(d1, d2)

    log.info("new added : %s, new modified : %s", add, modified)

    mod_list = []
    if modified:
        m_list = update_modified_processed_db(modified)
        mod_list.extend(m_list)
    if add:
        for i in add:
            mod_list.append(i)
            try:
                update_processed_index_db({i: [d1.get(i)[0], d1.get(i)[1]]})
            except:
                log.error("Alert 1.0 please check.")

    log.info("mod_list : %s", mod_list)

    projects = []
    if mod_list:
        for mod in mod_list:
            project    = mod.split("-",1)[1]
            user       = mod.split("-",1)[0]
            print("user", user)
            doc     = {"query": {"bool": {"filter": {"term": {"project": project}}}}}
            resp_data = es.search(index="crawling-pattern", body=doc,request_timeout=100)

            for res in resp_data["hits"]["hits"]:
                user        = res['_source']['user']
                project     = res['_source']['project']
                keywords    = res['_source']['keyword-search']
                contexts    = res['_source']['context-search']

                key_contexts = []
                key_contexts.extend([i.lower().replace('"', '').strip() for i in keywords.split(",") if i])
                key_contexts.extend([i.lower().replace('"', '').strip() for i in contexts.split(",") if i])
                projects.append((user, project, key_contexts))

    return projects
