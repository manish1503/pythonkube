import random
# import elasticsearch
from gensim.models import doc2vec
import numpy as np
from math import floor
import os
import datetime
import multiprocessing
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use("Agg")
os.chdir("..")
from sklearn.tree import DecisionTreeClassifier
from sklearn.cross_validation import train_test_split
from sklearn.externals import joblib
from  sklearn import preprocessing
from sklearn import linear_model
from sklearn.naive_bayes import GaussianNB
from SCHEDULE.get_logger import get_log_object
from CONNECTIONS.ESConnection import *
w2v_dimension = 50
w2v_windown = 5
num_workers = multiprocessing.cpu_count()
doc_min = 2
doc_iter = 30
MIN_WORDS = 20  # ignore file with fewer words

##Decision Tree Values
random_state=100
max_depth=3
min_samples_leaf=5
test_size=0.3
random_state=100

#Bayesian Values
priors=None

log_path   = "../LOGS"
model_path = "../MODEL"

# es = elasticsearch.Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
today_date  = datetime.datetime.now().date()

sentence_d2v        = []
labeled_data        = []

##creating similarity models and classifiers

def generate_X_Y(index):
    index_name = index
    log = get_log_object(log_path, "generate_X_Y-" + index_name)

    scroll_index(index_name, True, log)  ##Temp Training Index and Classifiers  with training data
    log.info("process completed for : %s", index)


def scroll_index(index_name, lr, log):
    lr_index = index_name
    if lr:
        index_name = "temp-training-"+index_name

    # es.cluster.health(wait_for_status='yellow', request_timeout=100)
    doc = {"_source": {"includes": ["ml_aggregrate", "sme"]}}
    page = es.search(
        index=index_name,
        scroll='200m', body=doc,
        size=500)

    sid = page['_scroll_id']
    scroll_size = page['hits']['total']

    log.info("scrolling index : %s, scroll_size: %s", index_name, scroll_size)
    prepare_labeled_data(page, lr)

    records_processed = 500
    while (scroll_size > 0):

        page = es.scroll(scroll_id=sid, scroll='200s')
        sid = page['_scroll_id']
        scroll_size = len(page['hits']['hits'])

        if scroll_size > 0:
            records_processed += scroll_size
            log.fatal("Number of Records Processed : %s", records_processed)
            prepare_labeled_data(page, lr)

    if lr:
        log.fatal("*!*!*!*!*!* building (tree, logistic and bayesian)classifiers *!*!*!*!*!*")
        prepare_feature_set_n_build_classifiers(lr_index, labeled_data, log)

    log.info("labeled data prepared for : %s", lr_index)


def prepare_labeled_data(page, lr):
    """scrolling records and preparing labeled data """

    for item in page['hits']['hits']:
        ml_aggr = ''
        label   = "no-subject"

        if item['_source']:
            if "ml_aggregrate" in item['_source']:
                ml_aggr = str(item['_source']['ml_aggregrate'])
            if "sme" in item['_source']:
                sme = str(item['_source']['sme'])

                try:
                    if int(sme) >= 4:
                        label = "subject"
                except:
                    pass

        if lr:
            labeled_data.append((ml_aggr, sme))


def prepare_feature_set_n_build_classifiers(index_name, labled_data, log):
    model = doc2vec.Doc2Vec.load(model_path + "/" + index_name+'_sentence_d2v.model')
    random.shuffle(labled_data)
    log.info("preparing train and test data set for index : %s", index_name)

    ### #80%
    log.info("Taking 80%-20% ratio of traing and test corpus")
    train_corpus = labled_data[0 : floor(4*len(labled_data)/5)] #80%
    #test_corpus  = labled_data[floor(4*len(labled_data)/5) : ]

    train_targets, train_regressors = zip(*[(doc[0], doc[1]) for doc in train_corpus])
    #test_targets, test_regressors   = zip(*[(doc[0], doc[1]) for doc in test_corpus])

    X = []
    for i in range(len(train_targets)):
        X.append(model.infer_vector(train_targets[i]))

    #train_x  = np.asarray(X)
    Y = np.asarray(train_regressors)

    plt.hist(Y, bins=5)
    plt.ylabel("SME Histogram")
    print("Y : \n" , Y)
    log.fatal("|----|----|----|--> X & Y ready <--|----|----|----|----|")


generate_X_Y("boston-med3d")


