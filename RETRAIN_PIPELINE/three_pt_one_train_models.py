import random
import elasticsearch
from gensim.models import doc2vec
from gensim.models.doc2vec import LabeledSentence
import numpy as np
from math import floor
import os
import datetime
import threading
import time
import multiprocessing
import matplotlib
matplotlib.use("Agg")
from CONNECTIONS.ESConnection import *
from sklearn.tree import DecisionTreeClassifier
from sklearn.cross_validation import train_test_split
# from sklearn.model_selection import train_test_split
from sklearn.externals import joblib
from  sklearn import preprocessing
from sklearn import linear_model
from sklearn.naive_bayes import GaussianNB
from SCHEDULE.get_logger import get_log_object
from CLASSIFIER.utils import filter_stop_words

w2v_dimension = 50
w2v_windown = 5
num_workers = multiprocessing.cpu_count()
doc_min = 2
doc_iter = 30
MIN_WORDS = 20  # ignore file with fewer words

##Decision Tree Values
random_state=100
max_depth=3
min_samples_leaf=5
test_size=0.3
random_state=100

#Bayesian Values
priors=None

log_path   = "../LOGS"
model_path = "../MODEL"

# es = elasticsearch.Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
today_date  = datetime.datetime.now().date()

sentence_d2v        = []
labeled_data        = []

##creating similarity models and classifiers

def three_pt_one_create_models(projects):
    """training model for big projects"""

    for p in projects:
        index   = p[0]+ "-" +p[1]
        scroll_temp_training_index(index)
        # t       = threading.Thread(target=scroll_temp_training_index, args=(index,))
        # t.start()
    ''' model training completes '''
    return True

def scroll_temp_training_index(index):
    index_name = index
    log = get_log_object(log_path, "three_pt_one_classifiers-" + index_name)

    scroll_index(index_name, False, log) ##Forcing Training to do Sent2Vec and Word2Vec - with all recent processed data
    scroll_index(index_name, True, log)  ##Temp Training Index and Classifiers  with training data
    log.info("process completed for : %s", index)


def scan_shard(es, index_name, doc,type, routing,shard,log,lr):
    page = es.search(
        index=index_name,doc_type=type,
        scroll='2m', body=doc,from_=0,
        size=10,request_timeout=60,routing=routing)
    sid = page['_scroll_id']
    scroll_size = page['hits']['total']

    log.info("scrolling index : %s, scroll_size: %s", index_name, scroll_size)
    prepare_labeled_data(page, lr)
    records_processed = 10
    while (scroll_size > records_processed):
        if records_processed % 100000 == 0:
            time.sleep(10)
        try:
            es.cluster.health(wait_for_status='yellow', request_timeout=1000)
            page = es.scroll(scroll_id=sid, scroll='2m')
        except elasticsearch.exceptions.ConnectionTimeout:
            log.error("Elasticsearch.exceptions.connectionTimeout")
            es.indices.clear_cache(index=index_name)
            page = es.search(
                index=index_name,doc_type=type,
                scroll='10m', body=doc, from_=records_processed,
                size=10, request_timeout=60, routing=routing)
        except elasticsearch.exceptions.NotFoundError:
            print("elastisearch.exception.NotFondError")
            es.indices.clear_cache(index=index_name)
            page = es.search(
                index=index_name,doc_type=type,
                scroll='10m', body=doc, from_=records_processed,
                size=10, request_timeout=60, routing=routing)
        except elasticsearch.exceptions.TransportError:
            log.error("Elasticsearch.exceptions.TransportError")
            es.indices.clear_cache(index=index_name)
        except elasticsearch.exceptions.__all__:
            log.error("Some Error has occured")
            es.indices.clear_cache(index=index_name)
        except Exception as e:
            continue
        sid = page['_scroll_id']
        scroll_length = len(page['hits']['hits'])

        if scroll_length > 0:
            records_processed += scroll_length
            log.fatal(
                "Number of Records Processed : %s scroll length : %s Total record : %s shard : %s and routing : %s ",
                records_processed, scroll_length, scroll_size, shard, routing)
            prepare_labeled_data(page, lr)

def scroll_index(index_name, lr, log):
    lr_index = index_name
    type = 'processed'
    if lr:
        index_name = "temp-training-"+index_name
        type = "train"

    es.cluster.health(wait_for_status='yellow', request_timeout=100)
    doc = {"_source": {"includes": ["ml_aggregrate", "sme"]}}

    shards_info = es.search_shards(index=index_name,doc_type=type)
    number_of_shards = len(shards_info['shards'])
    print(number_of_shards)
    shards_to_routing = {}
    i = 0
    while len(shards_to_routing.keys()) < number_of_shards:
        result = es.search_shards(index=index_name,doc_type=type, routing=i)
        shard_number = result['shards'][0][0]['shard']
        if shard_number not in shards_to_routing:
            shards_to_routing[shard_number] = i
        i += 1

    print(shards_to_routing)
    for shard, routing in shards_to_routing.items():
        scan_shard(es, index_name, doc,type, routing, shard,log,lr)

    if lr:
        log.fatal("*!*!*!*!*!* building (tree, logistic and bayesian)classifiers *!*!*!*!*!*")
        prepare_feature_set_n_build_classifiers(lr_index, labeled_data, log)
    else:
        log.fatal("*!*!*!*!*!* building sentence similarity models *!*!*!*!*!*")
        build_train_sent_n_word_vec_model(sentence_d2v, index_name, log)

    log.info("labeled data prepared for : %s", lr_index)


def prepare_labeled_data(page, lr):
    #log.info("scrolling records and preparing labeled data")

    for item in page['hits']['hits']:
        ml_aggr = ''
        label   = "no-subject"

        if item['_source']:
            if "ml_aggregrate" in item['_source']:
                ml_aggr = str(item['_source']['ml_aggregrate'])
            if "sme" in item['_source']:
                sme = str(item['_source']['sme'])

                try:
                    if int(sme) >= 4:
                        label = "subject"
                        #log.info("label : %s", label)
                except:
                    pass

        if lr:
            labeled_data.append((ml_aggr, label))
        else:
            mlwords = filter_stop_words(ml_aggr)
            #words_w2v.append(mlwords)
            sentence_d2v.append(LabeledSentence(mlwords, [u'{}'.format(label)]))

    #log.info("data-set preprared with labels")


def build_train_sent_n_word_vec_model(sentences_d2v, project_name, log):
    log.fatal("*!*!*!*!*!*!*Creating Sent2Vec Model*!*!*!*!*!*!*\n Training Start")

    model_d2v = doc2vec.Doc2Vec(sentences_d2v, size=w2v_dimension, window=w2v_windown, workers=num_workers, min_count=doc_min,
                        iter=doc_iter)
    log.info("sentence2vec processed on all recent data")
    ''' Storing Model in Disk '''
    model_d2v.save(os.path.join(model_path, "new-" + project_name + '_sentence_d2v.model'))

    log.info("Model files are created, Model is Ready : %s", str(project_name))
    return True


def prepare_feature_set_n_build_classifiers(index_name, labled_data, log):
    model = doc2vec.Doc2Vec.load(model_path + "/" + "new-"+index_name+'_sentence_d2v.model')
    random.shuffle(labled_data)
    log.info("preparing train and test data set for index : %s", index_name)

    ### #80%
    log.info("Taking 80%-20% ratio of traing and test corpus")
    train_corpus = labled_data[0 : floor(4*len(labled_data)/5)] #80%
    test_corpus  = labled_data[floor(4*len(labled_data)/5) : ]

    train_targets, train_regressors = zip(*[(doc[0], doc[1]) for doc in train_corpus])
    test_targets, test_regressors   = zip(*[(doc[0], doc[1]) for doc in test_corpus])

    X = []
    for i in range(len(train_targets)):
        X.append(model.infer_vector(train_targets[i]))

    train_x  = np.asarray(X)
    Y = np.asarray(train_regressors)

    le = preprocessing.LabelEncoder()
    le.fit(Y)
    train_y = le.transform(Y)

    ##training##
    log.info("Creating Logistic Regression Classifier")
    logreg = linear_model.LogisticRegression()
    logreg.fit(train_x, train_y)

    log.info("Creating Bayesian Classifier")
    bayesian_clf = GaussianNB()
    bayesian_clf.fit(X, Y)
    GaussianNB(priors=priors)

    ''' Scikit-learn method to implement the decsion tree classifier '''
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=test_size, random_state=random_state)

    ''' Decision model with Gini index critiria '''
    log.info("Creating Decision Tree Classifier")
    decision_tree_clf = DecisionTreeClassifier(criterion="gini", random_state=random_state, max_depth=max_depth, min_samples_leaf=min_samples_leaf)
    decision_tree_clf.fit(X_train, y_train)

    log.info("storing classifiers to disk")

    joblib.dump(logreg, model_path+"/"+"new-"+index_name+'-logistic.model')
    joblib.dump(bayesian_clf, model_path+"/"+"new-"+index_name+'-bayesian.model')
    joblib.dump(decision_tree_clf, model_path+"/"+"new-"+index_name+'-tree.model')

    log.info("classifiers building done.")

    test_list = []
    for i in range(len(test_targets)):
        test_list.append(model.infer_vector(test_targets[i]))
    test_x = np.asarray(test_list)

    test_Y = np.asarray(test_regressors)
    test_y = le.transform(test_Y)

    preds = logreg.predict(test_x)

    log.info(np.mean(test_y))
    log.info(sum(preds == test_y) / len(test_y))
    log.fatal("|----|----|----|--LR Classification Completes--|----|----|----|----|")

