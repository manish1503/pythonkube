# import elasticsearch
import os
import shutil
import gensim
from sklearn.externals import joblib
import datetime
import threading
from FILTER.profanity_lang_filter import filter_stop_words
from CLASSIFIER.utils import most_frequent_words
from SCHEDULE.get_logger import get_log_object
from CONNECTIONS.ESConnection import *
# es = elasticsearch.Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
today_date = datetime.datetime.now().strftime("%Y-%m-%d")

log_path    = "../LOGS"
model_path  = "../MODEL"

## testing models and classifiers and update the model if test is successful, unsuccessful log

def three_pt_two_test_models(projects):
    """ preparing data data """
    for p in projects:
        index_name      = p[0] + "-" + p[1]
        test_cases(index_name)
        # t = threading.Thread(target=test_cases, args=(index_name,))
        # t.start()
    """ Testing Complete """
    return True

def test_cases(index):
    """ test cases prepared """
    copy_to_api = False
    log = get_log_object(log_path, "three_pt_two_test_models-"+index)
    test_sentences = []

    log.info("getting sample sentence for test for sme-5 and sme-1")
    sme_five_sentence = get_records(index, 5)
    if not sme_five_sentence or len(sme_five_sentence) < 10:
        sme_five_sentence = get_records(index, 4)
    sme_one_sentence  = get_records(index, 1)

    test_sentences.extend(sme_five_sentence)
    test_sentences.extend(sme_one_sentence)

    log.debug("test sentence size : %s", len(test_sentences))
    test_five = test_doc_to_vec(index, sme_five_sentence, log, test=True)
    if test_five:
        copy_to_api = True

    test_doc_to_vec(index, sme_one_sentence, log)
    test_word_to_vec(index, test_sentences, log)
    test_classifiers(index, test_sentences, log)

    if copy_to_api:
        move_to_api(index, log)
    else:
        log.error("************ MODEL NOT TRAINED PROPERLY, PLEASE CHECK ******************* %s", index)

    log.info("test process completed for : %s", index)

def get_records(index, sme_value):
    sample_sentences    = []
    doc = {"_source": {"includes": ["ml_aggregrate"]}, "query": {"bool": {"filter": {"term": {"sme": sme_value}}}}}
    page = es.search(index=index, body=doc, size=250, sort="date:desc")

    count = 0
    for p in page['hits']['hits']:
        if "ml_aggregrate" in p['_source']:
            sample_sentences.append(p['_source']['ml_aggregrate'])
            count += 1
            if count >= 50:
                break
    return sample_sentences

def test_doc_to_vec(index, test_sentences, log, test=None):
    log.info("doc to vec testing starts")
    model_name = "new-"+index + "_sentence_d2v.model"
    subject_list = []
    try:
        model_d2v = gensim.models.Doc2Vec.load(os.path.join(model_path, model_name), mmap='r')
        for sentence in test_sentences:

            tokens = filter_stop_words(sentence)
            new_vector = model_d2v.infer_vector(tokens)
            synonyms = model_d2v.docvecs.most_similar([new_vector])
            if test:
                if float(synonyms[0][1]) >= float(synonyms[1][1]):
                    label = synonyms[0][0]
                else:
                    label = synonyms[1][0]

                if label == "subject":
                    subject_list.append(label)
            log.info("Similarity in Project: %s for the sentence : %s", str(synonyms), sentence[:200])

        if subject_list:
            return True
    except Exception as e:
        log.error(e.args)
        return False

def test_word_to_vec(index, test_sentences, log):
    log.info("word to vec testing starts")
    test_words = most_frequent_words(test_sentences)
    model_name = index + '_w2v.model'
    try:
        model_w2v = gensim.models.Word2Vec.load(os.path.join(model_path, model_name), mmap='r')
        for word in test_words:
            synonyms = model_w2v.most_similar(word)
            log.info("Similarity in Project: %s for the word : %s", str(synonyms), word)
    except Exception as e:
        log.error(e.args)

def test_classifiers(index, test_sentences, log):
    log.info("testing logistic, tree and bayesian classifiers")
    try:
        model_d2v       = gensim.models.Doc2Vec.load(os.path.join(model_path, "new-"+index+"_sentence_d2v.model"), mmap='r')
        logistic_model  = joblib.load(os.path.join(model_path, "new-"+index+'-logistic.model'))
        tree_model      = joblib.load(os.path.join(model_path, "new-"+index + '-tree.model'))
        bayesian_model  = joblib.load(os.path.join(model_path, "new-"+index + '-bayesian.model'))

        log.info("classifiers are loaded")
    except Exception as e:
        log.error(e.args)

    for sentence in test_sentences:
        tokens          = filter_stop_words(sentence)
        bayes_pred      = bayesian_model.predict([model_d2v.infer_vector(tokens)])[0]
        tree_pred       = tree_model.predict([model_d2v.infer_vector(tokens)])[0]
        logistic_pred   = logistic_model.predict([model_d2v.infer_vector(tokens)])[0]
        logistic_pred   = "subject" if logistic_pred == 1 else "no-subject"

        log.info("bayes prediction : %s, tree prediction : %s, logistic prediction : %s , sentence : %s", str(bayes_pred), str(tree_pred), str(logistic_pred), sentence[:200])

def move_to_api(index, log):
    "renaming classifiers and copy new classifiers"
    for f in next(os.walk(model_path))[2]:
    #     if f.startswith(index) and not f.startswith(index+"_w2v"):
    #         src = model_path + '/' + f
    #         log.info("renaming files to current date : %s", src)
    #         if os.path.exists(src):
    #             dest = model_path + "/" + str(today_date) + "_" + f
    #             try:
    #                 os.rename(src, dest)
    #             except:
    #                 if os.path.exists(src):
    #                     try:
    #                         os.remove(dest)
    #                         os.rename(src, dest)
    #                     except:
    #                         shutil.move(src, dest)


        if f.startswith("new-"+index) and not f in [index+"_w2v.model"]:
            src = model_path + '/' + f
            log.info("renaming files (removing new): %s", src)
            if os.path.exists(src):
                dest = model_path + "/" + f.split("new-",1)[1]
                try:
                    os.rename(src, dest)
                except:
                    if os.path.exists(src):
                        try:
                            log.info("os.remove/rename src : %s, dest : %s", src, dest)
                            if os.path.exists(dest):
                                dest_new = model_path + "/" + str(today_date) + "_" + f.split("new-",1)[1]
                                try:
                                    os.rename(dest, dest_new)
                                except:
                                    os.remove(dest)
                            os.rename(src, dest)
                        except:
                            log.info("shutil src : %s, dest : %s", src, dest)
                            shutil.move(src, dest)

    log.debug("model copied succesfully")
    return True

