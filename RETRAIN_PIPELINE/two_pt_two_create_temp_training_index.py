# import elasticsearch
from math import ceil
import threading
from SCHEDULE.get_logger import get_log_object
from CONNECTIONS.ESConnection import *
# es = elasticsearch.Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
log_path = "../LOGS"
recent = "now-16w/w" #120days

## creating the training index

def two_pt_two_create_temp_index(projects):

    for p in projects:
        index_name = p[0] + "-" + p[1]
        process_records(index_name)
        # t = threading.Thread(target=process_records, args=(index_name,))
        # t.start()
    return True

def process_records(index_name):
    """ implementing method to create temporary training index"""
    if True:
        log = get_log_object(log_path, "two_pt_two_temp_training-"+index_name)

        sme_gte_four                    = get_sme_gte_four_count(index_name)
        total_records = sme_lte_three   = sme_gte_four * 9
        percent_25_of_total_records     = total_records * 25/100
        percent_50_total_records        = percent_25_of_total_records*2

        log.info("sme_gte_four :%s, sme_lte_four :%s, percent_25_of_total_records :%s, percent_50_of_total_records :%s",str(sme_gte_four), str(sme_lte_three), str(percent_25_of_total_records), percent_50_total_records)

        ##taking 25 percent of each Diffbot and Webhose
        diffbot_rec = get_source_count(index_name, "Diffbot")
        webhose_rec = get_source_count(index_name, "Webhose")

        compn_records = 0
        compn_records += (ceil(percent_25_of_total_records) - diffbot_rec) if diffbot_rec < ceil(
            percent_25_of_total_records) else 0
        compn_records += (ceil(percent_25_of_total_records) - webhose_rec) if webhose_rec < ceil(
            percent_25_of_total_records) else 0

        if diffbot_rec > ceil(percent_25_of_total_records):
            diffbot_rec = ceil(percent_25_of_total_records)
        if webhose_rec > ceil(percent_25_of_total_records):
            webhose_rec = ceil(percent_25_of_total_records)

        ##compensating records with twitter
        twitter_rec = int(ceil(percent_50_total_records)) + compn_records
        log.info("compan_records : %s", compn_records)
        log.fatal("diffbot : %s, webhose : %s, twitter : %s", diffbot_rec, webhose_rec, twitter_rec)

        log.info("delete existing index first and creating a fresh/new temp training index")
        delete_index("temp-training-"+index_name ,log)
        create_index("temp-training-"+index_name ,log)

        log.info("update sme 4-5 records")
        if sme_gte_four:
            scroll_index_source(index_name, {"_source": {"includes" : ["ml_aggregrate", "type", "sme"]}, "query": {"range": {"sme": {"gte": 4, "lte": 5}}}}, int(sme_gte_four), log)

        log.info("update sme below 3 records, scroll indexes from sources")
        if diffbot_rec:
            scroll_index_source(index_name, {"_source": {"includes" : ["ml_aggregrate", "type", "sme"]}, "query": {"bool": {"must": [{"match": {"type": "Diffbot"}}], "filter": [{"range": {"sme": {"lte": 3}}},{"range": {"date": {"gte": recent, "lt": "now/d"}}}]}}}, int(diffbot_rec), log)
        if webhose_rec:
            scroll_index_source(index_name, {"_source": {"includes" : ["ml_aggregrate", "type", "sme"]}, "query": {"bool": {"must": [{"match": {"type": "Webhose"}}], "filter": [{"range": {"sme": {"lte": 3}}},{"range": {"date": {"gte": recent, "lt": "now/d"}}}]}}}, int(webhose_rec), log)
        if twitter_rec:
            scroll_index_source(index_name, {"_source": {"includes" : ["ml_aggregrate", "type", "sme"]}, "query": {"bool": {"must": [{"match": {"type": "Twitter"}}], "filter": [{"range": {"sme": {"lte": 3}}},{"range": {"date": {"gte": recent, "lt": "now/d"}}}]}}}, int(twitter_rec), log)


def get_source_count(index, source):
    doc = {"query":{"bool":{"must":[{"match":{"type":source}}], "filter": [{"range":{"sme":{"lte": 3}}},{"range": {"date": {"gte": recent,"lt": "now/d"}}}]}}}
    try:
        res = es.count(index=index, body=doc)
        return res['count']
    except:
        return 0


def get_sme_gte_four_count(index):
    doc = {"query": {"range": {"sme": {"gte": 4, "lte": 5}}}} #"query": {"bool": {"must": [{"range": {"date": {"gte": recent, "lt": "now/d"}}},
    try:
        res = es.count(index=index, body=doc)
        return res['count']
    except:
        return 0


def scroll_index_source(index_name, doc, size, log):
    """scrolling index"""
    page = es.search(
        index=index_name,
        body=doc,
        scroll='90m',
        size=10, sort="date:desc",request_timeout = 60)

    sid = page['_scroll_id']
    scroll_size = page['hits']['total']
    total_records = page['hits']['total']

    log.info("scroll_size : %s, total_records :%s", scroll_size, total_records)
    prepare_index(index_name, page, log)
    records = 10
    while (records < size):
        #log.info("index_name : %s,%s ",index_name , "Scrolling...")
        try:
            page = es.scroll(scroll_id=sid, scroll='90m')
            sid = page['_scroll_id']
            scroll_size = len(page['hits']['hits'])

            if scroll_size > 0:
                records += scroll_size
                log.info("Number Of Records Processed : %s", records)
                prepare_index(index_name, page, log)
            else:
                break
        except:
            continue


def prepare_index(index_name, page, log):
    """indexing records"""
    for item in page['hits']['hits']:
        try:
            guid = item['_id']
            #log.fatal("===item guid===%s",guid)
            #log.debug("source : %s", item['_source'])
            try:
                res = es.index(index="temp-training-"+index_name, doc_type="train", id=guid, body=item['_source'],request_timeout=100)
                #log.fatal(res['created'])
            except elasticsearch.exceptions.NotFoundError:
                log.warning("elasticsearch.exceptions.NotFoundError")
                continue
            except elasticsearch.exceptions.ConnectionTimeout:
                log.warning("elasticsearch.exceptions.ConnectionTimeout")
                continue
            except:
                log.error("Undefined Error.")
                continue
        except Exception as e:
            log.error(e.args)
            continue


def create_index(index, log):
    body    = { "settings": { "index": { "number_of_shards": 1, "number_of_replicas": 2 } } }

    if es.indices.exists(index=index):
        return True
    else:
        try:
            es.indices.create(index=index, ignore=400, body=body)
        except:
            log.warning("Error in index creation")
        return True


def delete_index(index, log):
    try:
        if es.indices.exists(index=index):
            es.indices.delete(index=index, ignore=[400, 404])
            log.info("Index deleted : %s", index)
    except:
        log.error("Errror : index not deleted : %s", index)

    return True
