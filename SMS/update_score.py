# import elasticsearch
import datetime
from FILTER.entity_extraction import get_entity
from FILTER.profanity_lang_filter import get_sentiment_score, get_processed_text
from SCHEDULE.get_logger import get_log_object
from FILTER.get_classifier_score import get_scores
from FILTER.get_classifier_score import load_model
from CONNECTIONS.ESConnection import *
import os
os.chdir("..")
# es = elasticsearch.Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
log_path    = "../LOGS"
current_date = datetime.datetime.now().date()

def process_records(index_name):
    """scrolling index to process records"""
    load_model(index_name)
    log = get_log_object(log_path, "five_pt_one_update_ml_score-"+index_name)
    doc = {"_source": {"includes" : ["ml_aggregrate","score_emotion"]}}
    page = es.search(
        index=index_name,
        body=doc,
        scroll='200m',
        size=500, sort="date:desc")

    sid = page['_scroll_id']
    scroll_size = page['hits']['total']
    total_records = page['hits']['total']
    log.fatal("total_records : %s", total_records)

    update_ml_aggr(index_name, page)
    recrods_updated = 500

    while (scroll_size > 0):
        try:
            page = es.scroll(scroll_id=sid, scroll='200m')
            sid = page['_scroll_id']
            scroll_size = len(page['hits']['hits'])

            recrods_updated += scroll_size
            log.fatal("records updated : %s", recrods_updated)

            if scroll_size > 0:
                update_ml_aggr(index_name, page)
        except Exception as e:
           # log.error(e.args)
           # log.error("Error In Scroll API")
            continue


def update_ml_aggr(index_name, page):
    """updating ml_scores"""
    for item in page['hits']['hits']:
        guid = item['_id']
        if item['_source']:

            if "ml_aggregrate" in item['_source']:
                processed_text = str(item['_source']['ml_aggregrate'])
            else:
                processed_text = ''

            if processed_text:
                sentiment_score = str(item['_source']['score_emotion'])
                # processed_text = get_processed_text(raw_aggr)
                # person, org, location = get_entity(raw_aggr)
                #first time processing  -- no models
                tree_score, logistic_score, bayesian_score, score_subject, classification_label = get_scores(processed_text)
                classification_score = 0
                if classification_label.lower() == "subject":
                    classification_score = 1
                ml_score = float(sentiment_score) + float(bayesian_score) + float(tree_score) + float(logistic_score) + float(classification_score)
                print(ml_score)
                data = item['_source']
                data.update({'score_medic': "{:.2f}".format(score_subject)})
                data.update({'score_ml': float(ml_score)})
                data.update({'score_naive': float(bayesian_score)})
                data.update({'score_nn': float(logistic_score)})
                data.update({'predict02': classification_label})
                data.update({'score_svm': float(0)})
                data.update({'score_tree': float(tree_score)})
                # data.update({'entity_person': person})
                # data.update({'entity_org': org})
                # data.update({'entity_location': location})
                #
                # data.update({'ml_aggregrate': processed_text})
                # data.update({'score_emotion': float(sentiment_score)})
                # data.update({'type': 'ics.uci'})
                data.update({'modifiedDate': current_date})
                data.update({'modifiedBy': 'update_score.py'})

                try:
                    es.update(index=index_name, doc_type="processed", body={"doc": data}, id=guid)
                except elasticsearch.exceptions.NotFoundError:
                    #log.error("elasticsearch.exceptions.NotFoundError")
                    continue
                except elasticsearch.exceptions.ConnectionTimeout:
                    #log.error("elasticsearch.exceptions.ConnectionTimeout")
                    continue
                except Exception as e:
                    continue
            else:
                ##log.warning("No Content Found")
                continue
        else:
            continue
           # log.warning("No Data for guid :%s", guid)

process_records('sergio-sms')