import schedule
import time
import os
#from elasticsearch import Elasticsearch
import datetime
from CONNECTIONS.ESConnection import *
os.chdir("..")

from SCHEDULE.get_logger import get_log_object
from RETRAIN_PIPELINE.zero_pt_zero_active_projects import zero_projects_to_retrain , get_active_projects
from RETRAIN_PIPELINE.one_pt_one_sme_four_n_five import one_update_sme
from RETRAIN_PIPELINE.two_pt_two_create_temp_training_index import two_pt_two_create_temp_index
from RETRAIN_PIPELINE.three_pt_zero_w2v_model import three_pt_zero_create_w2v_model
from RETRAIN_PIPELINE.three_pt_one_train_models import three_pt_one_create_models
from RETRAIN_PIPELINE.three_pt_two_test_models import three_pt_two_test_models
from RETRAIN_PIPELINE.five_pt_one_update_ml_score import five_update_ml_score

log = get_log_object("../LOGS", "schedule_rr2")
#es = Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220", timeout=30, max_retries=10, retry_on_timeout=True)

def run_process():
    log.info("schedular starts on : %s", datetime.datetime.now())
    #projects = get_active_projects()
    projects = [('sergio', 'sms', ['sms', 'sms'])]
    log.info("projects to re-train : %s", projects)
    if projects:
        # zero        = three_pt_zero_create_w2v_model(projects)# three_pt_zero  ###word similarity
        # log.info("one pt one sme updation starts")
        # one         = one_update_sme(projects)
        #
        # will change SME
        # log.info("two pt two temp training index creation starts")
        # two         = two_pt_two_create_temp_index(projects)        ## creating the training index

        log.info("three pt one creating models")
        three       = three_pt_one_create_models(projects)   ##sentence + classifiers

        log.info("three_pt_two testing models")
        four        = three_pt_two_test_models(projects)

        log.info("five pt one updating ml scores")
        five        = five_update_ml_score(projects)

        log.info("Process Completed for :%s", projects)
    else:
        log.error("No Projects To Re-Train")

    log.info("process completed on : %s", datetime.datetime.now())

run_process()
#schedule.every().day.at("07:00").do(run_process)

# while True:
#     schedule.run_pending()
#     time.sleep(1)

## Run this before running the code for creating model

# POST /_reindex?pretty
# {
#   "source": {
#     "index": "sergio-sms"
#   },
#   "dest": {
#     "index": "temp-training-sergio-sms"
#   }
# }