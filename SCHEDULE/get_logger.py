import logging
import datetime
import csv
import os

today_date = datetime.datetime.now().strftime("%Y_%m_%d")
formatter = logging.Formatter('%(asctime)s %(levelname)s : %(message)s')
# os.chdir("..")

def setup_logger(logger_name, log_file, level=logging.DEBUG):
    l = logging.getLogger(logger_name)
    if not l.handlers:
        fileHandler = logging.FileHandler(log_file, mode='a')
        fileHandler.setFormatter(formatter)
        streamHandler = logging.StreamHandler()

        streamHandler.setFormatter(formatter)
        l.setLevel(level)
        l.addHandler(fileHandler)
        l.addHandler(streamHandler)



def get_log_object(log_path, log_name):
    logfile = log_path + "/" + today_date + "_"+ log_name + ".log"
    setup_logger(log_name, logfile)
    log = logging.getLogger(log_name)
    return log


def csv_log(path, data):
    if not os.path.exists(path):
        header = ('DATE', 'INDEX NAME', 'RECORDS-FETCHED(FROM)', 'RECORDS-FETCHED(TO)', 'SUCCESS', 'FAILED')
        with open(path, 'a', newline='') as out:
            csv_out = csv.writer(out)
            csv_out.writerow(header)
            csv_out.writerow(data)
    else:
        with open(path, 'a', newline='') as out:
            csv_out = csv.writer(out)
            csv_out.writerow(data)

    return True


def csv_detailed_log(path, data):
    if not os.path.exists(path):
        header = ('DATE', 'INDEX NAME', 'RECORD', 'SUCCESS/FAIL')
        with open(path, 'a', newline='') as out:
            csv_out = csv.writer(out)
            csv_out.writerow(header)
            for d in data:
                csv_out.writerow(d)
    else:
        with open(path, 'a', newline='') as out:
            csv_out = csv.writer(out)
            for d in data:
                csv_out.writerow(d)

    return True
