import os
print(os.getcwd())

if not os.path.exists(".."+"/"+"MODEL"):
    try:
        os.mkdir(".."+"/"+"MODEL")
    except FileExistsError:
        pass

if not os.path.exists(".."+"/"+"DATA"):
    try:
        os.mkdir(".."+"/"+"DATA")
    except FileExistsError:
        pass

if not os.path.exists(".."+"/"+"LOGS"):
    try:
        os.mkdir(".."+"/"+"LOGS")
    except FileExistsError:
        pass
