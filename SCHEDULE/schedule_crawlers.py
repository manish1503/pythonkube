import os
import sys
import time
import schedule
import nltk
print("Running Schedule Crawler")
time.sleep(30)
sys.path.append(os.getcwd())
print(os.getcwd()+"start file")
download = ['stopwords']

for index in download:
    print("stopword downloading")
    nltk.download(index)

nltk.download('all')
print("download complete")

from SCHEDULE.get_logger import get_log_object
from RAW_TO_PROCESS.list_crawler import crawl_index

log = get_log_object("../LOGS", "crawler")
print(os.getcwd()+"after reading log file")


def crawlers_indexing():
    log.info("Schedule starts")
    crawl_index()


# def schedule_crawler():
#     schedule.every(12).hours.do(crawlers_indexing)
#
#     while True:
#         schedule.run_pending()
#         time.sleep(1)


crawlers_indexing()
schedule.every(12).hours.do(crawlers_indexing)

while True:
    schedule.run_pending()
    time.sleep(1)
