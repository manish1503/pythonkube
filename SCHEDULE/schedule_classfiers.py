import schedule
import time
import os
# from elasticsearch import Elasticsearch

from CONNECTIONS.ESConnection import *
print(os.getcwd())
os.chdir("..")
print(os.getcwd())

from SCHEDULE.get_logger import get_log_object
from CLASSIFIER.train_projects import index_to_build
from CLASSIFIER.es_scroll_api import es_scroll_api

log = get_log_object("../LOGS", "schedular")
# es = Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")


def list_projects():
    projects = index_to_build()
    log.info("classfier schedular starts")
    for p in projects:
        try:
            project_name = p
            print(project_name)
            try:
                es_scroll_api(project_name)
                print("Model Created and Tested Successfully : ", project_name)
                log.info("Model Created and Tested Successfully : %s", project_name)
            except:
                log.warning("Error Occured in Training for index : %s", project_name)
                continue
        except:
            print("Some Error Occured, please check.")
            log.warning("Some Error Occured, please check.")
            continue


list_projects()
schedule.every().day.at("07:00").do(list_projects)


while True:
    schedule.run_pending()
    time.sleep(1)
