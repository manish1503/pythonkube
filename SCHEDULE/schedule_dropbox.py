import os
import datetime
import time
import schedule
import sys
# os.chdir("..")
#

print("Running schedule dropbox crawler")
time.sleep(30)
sys.path.append(os.getcwd())


from SCHEDULE.get_logger import get_log_object
from READ_DROPBOX.write_modification_to_fs import write_mod_files
from READ_DROPBOX.prepare_modification_file import compare_dbs
from READ_DROPBOX.raw_n_processed_to_es import raw_n_processed_index_to_es
from READ_DROPBOX.load_txt import reform_to_txt

today_date = datetime.datetime.now().strftime("%Y-%m-%d")
log = get_log_object("../LOGS", "schedular")

def read_dropbox():
    log.info("dropbox schedular started")
    mod_list = compare_dbs() #
    #mod_list = ['paulo-parkinson']
    if mod_list:
        log.info("Modification DB File Prepared, Writing Files to FS")
        write_mod_files(mod_list)
        log.info("Files Downloaded From Dropbox")
        reform_to_txt(mod_list)
        log.info("Files have been reformed to Text")
        raw_n_processed_index_to_es(mod_list)
        log.info("Raw and Processed Indexes Created")
    else:
        log.warning("No Modification Found.")

    log.info("Process Complete.")


schedule.every().day.at("01:00").do(read_dropbox)

while True:
    schedule.run_pending()
    time.sleep(1)


# def dropbox_crawler():
#     read_dropbox()
#     schedule.every().day.at("01:00").do(read_dropbox)
#
#     while True:
#         schedule.run_pending()
#         time.sleep(1)

