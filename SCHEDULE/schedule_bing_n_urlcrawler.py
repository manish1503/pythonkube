import os
import datetime
import logging
import time
import schedule
os.chdir("..")

from SCHEDULE.get_logger import setup_logger
from BING.bing_search_api import index_bing
from URL_CRAWLER.url_crawler import index_url_crawl
today_date = datetime.datetime.now().strftime("%Y-%m-%d")
logfile = "../LOGS"+"/"+today_date+"_scheduler_log"+".log"

setup_logger('scheduler', logfile)
log = logging.getLogger('scheduler')

def crawl_bing():
    log.info("bing-schedular starts")
    index_bing()
    log.info("URL-schedular starts")
    index_url_crawl()
    log.info("Process Complete.")

crawl_bing()
schedule.every().day.at("01:00").do(crawl_bing)
#schedule.every(1).hours.do(crawl_bing)

while True:
    schedule.run_pending()
    time.sleep(1)

