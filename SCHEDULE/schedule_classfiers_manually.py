import os
# from elasticsearch import Elasticsearch

from CONNECTIONS.ESConnection import *
print(os.getcwd())
os.chdir("..")
print(os.getcwd())

from SCHEDULE.get_logger import get_log_object
from CLASSIFIER.es_scroll_api import es_scroll_api

log = get_log_object("../LOGS", "schedular")
# es = Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")


def list_projects():
    project = 'temp-training-boston-med3d'
    log.info("classfier schedular starts")
    es_scroll_api(project)


list_projects()