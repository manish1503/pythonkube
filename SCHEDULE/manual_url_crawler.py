import os
import datetime
import logging
os.chdir("..")

from SCHEDULE.get_logger import setup_logger
from URL_CRAWLER.url_crawler import get_urls

today_date = datetime.datetime.now().strftime("%Y-%m-%d")
logfile = "../LOGS"+"/"+today_date+"_scheduler_log"+".log"

setup_logger('scheduler', logfile)
log = logging.getLogger('scheduler')

def crawl_url(index):
    get_urls(index)

crawl_url("simone-visual_search")
