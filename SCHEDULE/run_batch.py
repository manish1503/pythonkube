from subprocess import Popen
import schedule
import time

def schedule_jobs():
    p = Popen("schedule.bat", cwd=r"D:\PYPROJECTS\CODE\CODE\SCHEDULE")
    stdout, stderr = p.communicate()

schedule_jobs()
schedule.every(6).hours.do(schedule_jobs)

while True:
    schedule.run_pending()
    time.sleep(1)