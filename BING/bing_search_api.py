import uuid
#from elasticsearch import Elasticsearch
import requests
import json
import urllib
import datetime
from DATABASE.raw_index_db import *
from FILTER.utils import update_raw_index_db
from READ_DROPBOX.index_requests import index_check
from MANUAL_PROCESS.read_process_index import read_projects
from SCHEDULE.get_logger import get_log_object
from CONNECTIONS.ESConnection import *
current_date = datetime.datetime.now().date()
log         = get_log_object("../LOGS", "bing")

subscription_key = "63d64990ef7242aebd67f59e6eb1a019"#"6f3f94bd70484b77bc51510f3a25e37c"#"ab1037bc40494c21a7c44a6b67241ca5"#"96d05359d76f4e758906539daeab939e"#
assert subscription_key

search_url = "https://api.cognitive.microsoft.com/bing/v7.0/search"
#es = Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
url = "http://40.71.175.166:5000/getsubject"
diffbot_url = "https://api.diffbot.com/v3/analyze?token=9ade437e27ca6fbb44e57d07415ce806&url="

def index_bing():
    log.info("bing crawlers starts on : %s", str(current_date))
    projects = read_projects()

    log.info("projects : %s", projects)
    for p in projects:
        get_bing_data(" ".join(p[2][1:]), p[0]+"-"+p[1])


def get_bing_data(search_term, index_name):

    headers = {"Ocp-Apim-Subscription-Key" : subscription_key}
    params  = {"q": search_term, "textDecorations":True, "textFormat":"HTML", "limit":"25", \
               "safeSearch":"Moderate", "Pragma": "no-cache", "count" : 50, "offset" : 0}#, "freshness": "Month"}
    response = requests.get(search_url, headers=headers, params=params)
    print(response)
    # response.raise_for_status()
    search_results = response.json()

    log.info("search results : %s", search_results)
    try:
        values = search_results["webPages"]["value"] if "value" in search_results["webPages"] else []
        log.info("search term : %s, webpages count : %s", search_term, len(values)) ###"\n search results : \n", json.dumps(search_results))
    except:
        return
    links = []
    no_of_record = 0
    for val in values:
        no_of_record += 1
        try:
            lable = check_relevance(val['snippet'].replace("<b>","").replace("</b>",""), index_name)
            log.info("=====label====: %s", lable)
            if lable == "subject":
                links.append(val['url'])
        except:
            continue

    log.info("creawling diffbot with URLs %s, length : %s", links, len(links))

    diffbot_url_crawler(links, index_name)
    log.debug("mapping complete for records :%s ", no_of_record)


def check_relevance(sentence, index_name):
    data = {'sentence': sentence, "user":index_name}
    payload = json.dumps(data)
    headers = {'content-type': 'application/json'}

    log.info("requesting api with payload : %s", payload)
    res = requests.post(url=url, data=payload, headers=headers)
    if res.status_code == 404:
        label = "subject"
    else:
        resp = json.loads(res.text)
        if resp[0][1] >= resp[1][1]:
            label = resp[0][0]
        else:
            label = resp[1][1]

    log.info("api response : %s", label)
    return label


def diffbot_url_crawler(urls, index_name):
    last_updated_index = raw_index_db.get(index_name+"-"+"bing")
    if not last_updated_index:
        latest_raw_index = "1-raw-"+index_name+"-bing"
    else:
        index_number = last_updated_index[1].split("-", 1)[0]
        latest_raw_index = str(int(index_number) + 1) + "-raw-" + index_name + "-"+ "bing"

    log.info("latest_raw_index :: %s",latest_raw_index)
    latest_index = {index_name+"-"+"bing" : [str(current_date), latest_raw_index]}

    for u in urls:
        uri = diffbot_url + urllib.parse.quote_plus(u, safe='', encoding=None, errors=None)
        resp = requests.get(url = uri)

        try:
            parse_resp = json.loads(resp.text)
            log.info("******* %s, latest raw index : %s", parse_resp['objects'][0], latest_raw_index)
            index_mapping(latest_raw_index, parse_resp['objects'][0])
        except:
            log.warning("record not indexed , please check for url : %s", str(u))
            pass
    try:
        log.info("Latest Index To Update : %s",latest_index)
        update_raw_index_db(latest_index)
    except:
        log.info("raw index db not updated for index: %s", latest_index)
        pass
    return True


def index_mapping(index_name, body_doc):
    guid = uuid.uuid4()
    index_check(index_name)

    log.info("adding record in index")
    res = es.index(index=index_name, id=guid, doc_type="raw", body=body_doc, request_timeout=100)

    log.info("record status : %s", str(res['created']))
    log.info(res['created'])

    return True
