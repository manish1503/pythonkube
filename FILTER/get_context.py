import os
import gensim
from SCHEDULE.get_logger import get_log_object
from sklearn.externals import joblib
import gensim.downloader as api
glove_twitter_25_model = api.load("glove-twitter-25") 
#context_url = "http://40.71.175.166:5000/tw-gensim?word="
log = get_log_object("../LOGS", "generic_api")
model_path = "../MODEL"
#glove_twitter_25_model = joblib.load(model_path+ "/" + 'glove-twitter-25.model')

model_w2v =  ''


def load_w2v_model(user):
    """loading model on call"""
    try:
        global model_w2v
        model_w2v = gensim.models.Doc2Vec.load(os.path.join(model_path, user + '_w2v.model'), mmap='r')
    except:
        return

def get_context_terms(term, index_name=None):
    similar_terms = []
    log.info("generic api requested for term: %s", term)
    #try:
    if index_name:
        synonyms = model_w2v.most_similar(term, topn=5)
    else :
        log.info("in else")
        synonyms = glove_twitter_25_model.most_similar(term, topn=5)
    log.info(synonyms)
    similar_terms.extend([i[0].lower() for i in synonyms])
    # except:
    #     log.info("No Similar Words Found")
    #     pass

    log.info("similar terms : %s", similar_terms)
    return similar_terms


# def get_context_terms_api(term, index_name=None):
#     similar_terms = []
#     log.info("generic api requested for term: %s", term)
#     try:
#         url = context_url + term + "&model="+index_name
#         resp = requests.get(url=url)
#         if resp.status_code == 404:
#             url = context_url + term
#             resp = requests.get(url=url)
#
#         data = json.loads(resp.text)
#         log.info("url and response : %s, %s", url, data)
#         if len(data) >= 5:
#             similar_terms.append(data[0][0].lower())
#             similar_terms.append(data[1][0].lower())
#             similar_terms.append(data[2][0].lower())
#             similar_terms.append(data[3][0].lower())
#             similar_terms.append(data[4][0].lower())
#     except:
#         log.info("No Similar Words Found")
#         pass
#
#     log.info("similar terms : %s", similar_terms)
#     return similar_terms
