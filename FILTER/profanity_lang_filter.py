from profanity import profanity
import re
import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords  # Import the stop word list
from langdetect import detect
from textblob import TextBlob
import unicodedata

stops = set(stopwords.words("english"))


def filter_stop_words(raw_sentence):
    letters_only = re.sub("[^a-zA-Z0-9\u00E4\u00F6\u00FC\u00C4\u00D6\u00DC\u00df]", " ", raw_sentence)
    words = letters_only.lower().split()
    meaningful_words = [w for w in words if not w in stops]
    return (meaningful_words)


def get_processed_text(raw_text):
    raw_text = str(unicodedata.normalize('NFKD', raw_text).encode('ascii','ignore'))[2:-1].replace("\\x0c", "").replace('\\x80', '').\
        replace('\\x99', '').replace("\\x93","").replace('\\x9d','').replace('\\xe2','').replace("\\x9c","").replace("\\xa6","").replace('\\','') #raw_text.lower().encode("ascii", "ignore").decode()

    ## profanity check ##
    filtered_text = profanity.censor(raw_text)
    ## remove stop words -en ##
    clean_text = filter_stop_words(filtered_text)
    processed_text = " ".join(clean_text)

    return processed_text

def check_en_language(text):
    lang = detect(text)
    if lang == "en":
        return True
    else:
        print("text language detected \n: ", str(lang))
        return False

def calculate_sentiment_score(processed_text):
    try:
        sentiment_analysis = TextBlob(processed_text)
        sentiment_score = sentiment_analysis.sentiment.polarity()
    except:
        sentiment_score = 0

    return sentiment_score

def get_sentiment_score(raw_text):
    try:
        ddshort = raw_text.split()[:2000]
        dds = ' '.join(ddshort)
        sentiment_analysis = TextBlob(dds)  # , analyzer=NaiveBayesAnalyzer())
        sentiment_sc = sentiment_analysis.sentiment

        #print("---*****---", sentiment_sc)
        sentiment_score = sentiment_sc.polarity
    except:
        sentiment_score = 0

    sentiment_score = "{:.2f}".format(sentiment_score)
    #print("!*!*! sentiment-score !*!*!", sentiment_score)
    return sentiment_score
