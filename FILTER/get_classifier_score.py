import datetime
import gensim
import os

from sklearn.externals import joblib
from SCHEDULE.get_logger import get_log_object
from FILTER.profanity_lang_filter import filter_stop_words
azure_url = "http://40.71.175.166:5000/"
headers   = {'content-type' : 'application/json'}

today_date = datetime.datetime.now().strftime("%Y-%m-%d")
log = get_log_object("../LOGS", "dropbox")
model_path = "../MODEL"

model_d2v       = ''
model_logistic  = ''
model_tree      = ''
model_bayesian  = ''


def load_model(user):
    """loading model on call"""
    try:
        global model_d2v
        global model_logistic
        global model_tree
        global model_bayesian
        log.debug("In load model start")
        model_d2v = gensim.models.Doc2Vec.load(os.path.join(model_path, user + '_sentence_d2v.model'), mmap='r')
        model_logistic = joblib.load(os.path.join(model_path, user + '-logistic.model'))
        model_tree = joblib.load(os.path.join(model_path, user + '-tree.model'))
        model_bayesian = joblib.load(os.path.join(model_path, user + '-bayesian.model'))
        log.debug("In load model end")
    except:
        log.debug("In load model except")
        return


def get_scores(text):
    """ getting classifier score on batch mode """
    ##log.debug('In get scores')
    tokens = filter_stop_words(text.lower())
    vector = [model_d2v.infer_vector(tokens)]
    #log.debug('In get scores 2')
    #log.debug(vector)
    res = model_d2v.docvecs.most_similar(vector)

    if float(res[0][1]) >= float(res[1][1]):
        d2v_label = res[0][0]
        d2v_score = res[0][1]
    else:
        d2v_label = res[1][0]
        d2v_score = res[1][1]

    tree_pred    = model_tree.predict(vector)[0]
    tree_score   = 1 if tree_pred == "subject" else 0

    log_score    = model_logistic.predict(vector)[0]

    bayes_pred    = model_bayesian.predict(vector)[0]
    bayes_score   = 1 if bayes_pred == "subject" else 0

    return tree_score, log_score, bayes_score, d2v_score, d2v_label
