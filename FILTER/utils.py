import os
# os.chdir("..")
from DATABASE.raw_index_db import *
from DATABASE.modified_dropbox_db import *
from DATABASE.dropbox_db import *
from DATABASE.processed_index_db import *

retrain_trigger = 100 ##percentage growth as 100 percent

##----------------
def human(size):
    B = "B"
    KB = "KB"
    MB = "MB"
    GB = "GB"
    TB = "TB"
    UNITS = [B, KB, MB, GB, TB]
    HUMANFMT = "%f %s"
    HUMANRADIX = 1024.

    for u in UNITS[:-1]:
        if size < HUMANRADIX: return HUMANFMT % (size, u)
        size /= HUMANRADIX

    return HUMANFMT % (size, UNITS[-1])


##-----------------
def update_dropbox_live_db(d):
    with open("." + "/DATABASE/" + "dropbox_live_db.py", "w") as p:
        p.write("dropbox_live_db=")
        p.write(str(d))
    return True

##-----------------
def create_modified_dropbox_db(path):
    with open(path, "w") as p:
        p.write("dropbox_modified_db=")
        p.write(str({}))
    return True

##-----------------
def update_modified_db(d):
    dropbox_modified_db.update(d)
    with open("." + "/DATABASE/" + "modified_dropbox_db.py", "w") as p:
        p.write("dropbox_modified_db=")
        p.write(str(dropbox_modified_db))
    return True

##-----------------
def update_dropbox_db(d):
    dropbox_db.update(d)
    with open("." + "/DATABASE/" + "dropbox_db.py", "w") as p:
        p.write("dropbox_db=")
        p.write(str(dropbox_db))
    return True

##-----------------
def update_processed_index_db(d):
    processed_index_db.update(d)
    with open("." + "/DATABASE/" + "processed_index_db.py", "w") as p:
        p.write("processed_index_db=")
        p.write(str(processed_index_db))
    return True

##-----------------
print(os.getcwd())
def update_raw_index_db(d):
    raw_index_db.update(d)
    with open("." + "/DATABASE/" + "raw_index_db.py", "w") as p:
        p.write("raw_index_db=")
        p.write(str(raw_index_db))
    return True


##-------------
def reinitialize_dropbox_modified_db():
    with open("." + "/DATABASE/" + "modified_dropbox_db.py", "w") as p:
        p.write("dropbox_modified_db=")
        p.write(str({}))
    return True

##-------------
def reinitialize_dropbox_live_db():
    with open("." + "/DATABASE/" + "dropbox_live_db.py", "w") as p:
        p.write("dropbox_live_db=")
        p.write(str({}))
    return True

##------------------
def dict_comparison(d1, d2):
    d1_keys = set(d1.keys())
    d2_keys = set(d2.keys())
    print(d1_keys,d2_keys)

    intersect_keys = d1_keys.intersection(d2_keys)

    print("intersect_keys",intersect_keys)
    added = d1_keys - d2_keys
    removed = d2_keys - d1_keys
    modified = {o : (d1[o], d2[o]) for o in intersect_keys if d1[o] != d2[o]}
    print("Updated", added ,removed ,modified)
    return added, removed, modified


##-------------------
def compare_size(new, old, nunit, ounit, change_percent):
    per_size = 0.0
    old = 0.21 if old in [0, 0.00] else old
    print("!====!",new, nunit, old, ounit)
    if new > old and nunit == ounit:
        per_size = (( new - old) / old) * 100
    if (nunit in ["mb"] and ounit == "kb"):
        per_size = ( ((new*1024) - old)/(old*1024) ) * 100
    if (nunit in ["gb"] and ounit == "mb"):
        per_size = ( ( (new*1024) - old)/(old*1024) ) * 100

    print("!====!","percent change",per_size,"%")
    if per_size >= int(change_percent):
        return True
    else:
        return False


##---------------------
def update_modified_dropbox_db(modified):
    modified_list = []

    if modified.keys().__len__() > 0:
        #mdb = {}
        for i in modified.keys():
            '''checking for 2 percent chnage'''
            print('\n*--checking for 2 percent chnage--*', i)
            new_size = modified[i][0][1][0:-2]
            old_size = modified[i][1][1][0:-2]
            new_size_unit = modified[i][0][1][-2:]
            old_size_unit = modified[i][1][1][-2:]

            comp_resp = compare_size(float(new_size), float(old_size), new_size_unit, old_size_unit, 2)
            if not comp_resp:
                continue
            # print("--more than 2 percent change found for domain -- : ",i.encode("ascii","ignore").decode())
            # print("********************",i.encode("ascii","ignore").decode(), modified[i][0][0], modified[i][0][1])
            # print("--------------------",i.encode("ascii","ignore").decode(), \
            #       modified[i][0][0], modified[i][0][1], modified[i][1][1])

            update_modified_db({i.encode("ascii", "ignore").decode(): [modified[i][0][0], modified[i][0][1], modified[i][1][1]]})
            modified_list.append(i.encode("ascii", "ignore").decode())
            update_dropbox_db({i.encode("ascii", "ignore").decode(): [modified[i][0][0], modified[i][0][1]]})

    return modified_list


##---------------------
def update_modified_processed_db(modified):
    modified_list = []

    if modified.keys().__len__() > 0:
        for i in modified.keys():
            '''checking for 100 percent chnage'''

            new_size        = modified[i][0][1][0:-2]
            old_size        = modified[i][1][1][0:-2]
            new_size_unit   = modified[i][0][1][-2:]
            old_size_unit   = modified[i][1][1][-2:]

            # print("new size : ", new_size, "unit", new_size_unit)
            # print("old size : ", old_size, "unit", old_size_unit)

            comp_resp = compare_size(float(new_size), float(old_size), new_size_unit, old_size_unit, retrain_trigger)
            if not comp_resp:
                continue

            # print("--more than 100 percent change found for domain -- : ",i.encode("ascii","ignore").decode())
            # print("********************",i.encode("ascii","ignore").decode(), modified[i][0][0], modified[i][0][1])
            # print("--------------------",i.encode("ascii","ignore").decode(), \
            #       modified[i][0][0], modified[i][0][1], modified[i][1][1])

            modified_list.append(i.encode("ascii", "ignore").decode())
            update_processed_index_db({i.encode("ascii", "ignore").decode(): [modified[i][0][0], modified[i][0][1]]})

    return modified_list