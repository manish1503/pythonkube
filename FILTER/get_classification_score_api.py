import requests
import datetime
import json
import os
#from flask import jsonify

from SCHEDULE.get_logger import get_log_object
azure_url = "http://40.71.175.166:5000/"
headers   = {'content-type' : 'application/json'}
model_path = "../MODEL"
today_date = datetime.datetime.now().strftime("%Y-%m-%d")
log = get_log_object("../LOGS", "crawler")

# def is_model_exist(user):
#
#     url = azure_url +"is_model_exist?user="+ str(user)
#     res = requests.get(url=url)
#     return True

def is_model_exist_fs_check(user):
    """ check model file exist in file system"""
    log.info("user : %s", user + '_sentence_d2v.model')
    if os.path.exists(model_path + '/' + user + '_sentence_d2v.model'):
        return True
    else:
        return False


def get_score(classifier_type, user, text):
    """ getting classifier score on api mode """
    url = azure_url + str(classifier_type)
    data = {'user': user, 'sentence':text}
    payload = json.dumps(data)
    score = 0

    try:
        resp = requests.post(url=url, data=payload, headers=headers,)
        #log.info("url : %s, response : %s", url , resp)
        if resp.status_code == 200 and resp.text:
            res  = json.loads(resp.text)
            if res == "subject":
                score = 1
        elif resp.status_code == 404:
            score = 0
    except Exception as e:
        log.error(e.args)

    #log.info("score: %s",score)
    return score


def get_class_label_n_score(user, text):
    """ getting classifier score on api mode """
    url = azure_url + str("getsubject")
    data = {'user': user, 'sentence': text}
    payload = json.dumps(data)
    score = 0
    label = "no-subject"
    try:
        resp = requests.post(url=url, data=payload, headers=headers, )
        #log.info("url : %s, response : %s", url, resp)
        if resp.status_code == 200 and resp.text:
            res  = json.loads(resp.text)
            if float(res[0][1]) >= float(res[1][1]):
                label = res[0][0]
                score = res[0][1]
            else:
                label = res[1][0]
                score = res[1][1]
        elif resp.status_code == 404:
            log.info("Model Not Found")
    except Exception as e:
        log.error(e.args)

    #log.info("score :%s, label:%s", score, label)
    return score, label
