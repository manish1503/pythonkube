from nltk import word_tokenize, pos_tag, ne_chunk, tree2conlltags

def get_entity(sample):
    location = []
    person   = []
    org      = []

    ne_tree = ne_chunk(pos_tag(word_tokenize(sample)))
    iob_tagged = tree2conlltags(ne_tree)

    for i in iob_tagged:
        if i[2] != "O" and not "_" in i[0]:
            try:
                val = i[2]
                if val == "B-PERSON":
                    if i[0] not in person:
                        person.append(i[0])
                elif val == "B-ORGANIZATION":
                    if i[0] not in org:
                        org.append(i[0])
                elif val in ["B-GPE", "B-LOCATION", "B-FACILITY"]:
                    if i[0] not in location:
                        location.append(i[0])
                elif val in ["DATE", "TIME", "MONEY", "PERCENT"]:
                    pass
                else:
                    pass

            except:
                pass

    person = ", ".join(person[:10])
    location = ", ".join(location[:10])
    org = ", ".join(org[:10])

    return person, org, location