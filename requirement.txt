docutils==0.14
nltk==3.2.5
gensim<=3.4.0
pandas==0.22.0
Flask==0.12
Flask-CORSify==0.1.1
Flask-Cors==3.0.3
Jinja2==2.9.5
Keras==0.3.1
Lasagne==0.1
MarkupSafe==1.0
PyFunctional==1.1.3
PyJWT==1.5.3
PyPDF2==1.25.1
PyYAML==3.12
Werkzeug==0.12.2
argparse==1.4.0
asn1crypto==0.22.0
backports.functools-lru-cache==1.4	
blinker==1.4	
bz2file==0.98
chardet==3.0.4
click==6.7
colorama==0.3.9
cryptography==2.0.3
cycler==0.10.0
dill==0.2.5
docutils==0.14
dropbox<=8.5.0
elasticsearch==5.5.2
gitlab3==0.5.8
idna==2.6
itsdangerous==0.24
jmespath==0.9.3
jsonschema==2.6.0
kiwisolver==1.0.1
langdetect==1.0.6
lxml==3.8.0
markalgo==1.0.2
matplotlib==2.2.2
oauthlib==2.0.6
pdfminer.six==20170720
profanity==1.1
psutil==5.4.8
pyOpenSSL==17.2.0
pycparser==2.18
pycryptodome==3.4.11
pyparsing==2.2.0
pyreadline==2.1
python-dateutil==2.6.1
python-docx==0.8.5
python-gitlab==1.2.0
python-purify==1.1.0	
pytz==2017.3
requests==2.17.2
requests-oauthlib==0.8.0
s3transfer==0.1.12
schedule==0.5.0
scikit-learn==0.19.1
scikit-neuralnetwork==0.7
scipy==1.1.0
seaborn==0.9.0
setuptools==36.2.2
six==1.10.0
sklearn-utils==0.0.15	
smart-open==1.5.3
tabulate==0.7.7
textblob==0.15.0
tornado==4.5.2
twython==3.6.0
urllib3==1.21.1
wheel==0.29.0
word2veckeras==0.0.5.2
numpy==1.13.1
statsmodels==0.9.0
