# from elasticsearch import Elasticsearch
import os
from pprint import pprint
from CONNECTIONS.ESConnection import *
model_path = "../../MODEL"
# es = Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
# #res = es.indices.exists(index="sergio-spacex",type = "processed",id =969700564463472600)
# res = es.search(index="sergio-spacex",doc_type = "processed",id =969700564463472600)
# print(res)

def test_record(index_name, query_term):
    doc = {"_source": {"includes" : ["score_ml","sme","score_naive", "score_nn","score_tree","predict02", "date", "description", "entity_person", "entity_location", "entity_org", "type"]},
                 "sort": [{"date": {"order": "desc"}, "score_ml": {"order": "desc"}}],
                 "query": {"bool": {"must": [{"multi_match": {"query": query_term, "type": "most_fields","minimum_should_match": "25%",
                 "fields": ["ml_aggregrate^5","title^5","entity_location^1","entity_org^1","entity_person^1"]}},{"range": {
                 "score_ml": {"gte": 3}}}],"should": {"match": {"ml_aggregrate": query_term}}}}}
    page = es.search(index=index_name, body=doc , size=100, request_timeout=100)
    pprint(page['hits']['hits'])
    resp = [p['_source'] for p in page['hits']['hits'] if p['_source']]
    print(len(resp))

#test_record("sergio-spacex", "falcon")

def is_model_exist_fs_check(user):
    """ check model file exist in file system"""
    if os.path.exists(model_path + '/' + user+ '_sentence_d2v.model'):
        print(True)
        return True
    else:
        print(False)
        return False


def delete_index(index_name):
    try:
        if es.indices.exists(index=index_name):
            es.indices.delete(index=index_name, ignore=[400, 404], request_timeout=100)
    except:
        pass
    return True

def update_index():
    count_index = "api_load_count"
    res = es.get(index=count_index, doc_type="count", id="AWMfcwefJx9yO3re85E3", request_timeout=100)
    print(res['_source'])
    resp = {'2018-04-30': 2, '2018-05-02': 10, '2018-05-01': 5, '2018-05-07': 0}
    es.update(index=count_index, doc_type="count", body={"doc": resp}, id="AWMfcwefJx9yO3re85E3", request_timeout=100)

update_index()
#delete_index("17-raw-boston-med3d-twitter")
#is_model_exist_fs_check("sergio-spacex")
