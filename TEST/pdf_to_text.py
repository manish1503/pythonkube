import PyPDF2
import uuid
import os
import re
from textblob import TextBlob
from profanity import profanity #print(profanity.get_words())

##pdf miner
import io
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
##

full_path = "G:\\PYPROJECTS\\CODE\\brian-bd\\DATA\\2018-02-13_RAW\\google.pdf"

def test1():
    f = open(os.getcwd()+"/"+"test.txt", "rb")
    file_data = str(f.readlines()[3:]).split()[:5000]  # file_data = str(f.readlines()[2:])
    file_text = ' '.join([str(i).replace("\\r\\n", "\n").replace("b''", "").replace(",", "").replace("b'", "")\
                         .replace("'", "")\
                          for i in file_data])
    a = re.sub(' +', ' ', file_text)
    #dd = re.sub(r'\n+(?=\n)', '\n', a)
    dd = re.sub(r'(\n\s*)+\n+', '\n\n', a)

    filtered_text = profanity.censor(dd)
    print(filtered_text)

    ddshort = filtered_text.split()[:1000]
    #ddshort = dd.split()[:1000]
    dds = ' '.join(ddshort)
    #print(dds)
    sentiment_analysis = TextBlob(dds)#, analyzer=NaiveBayesAnalyzer())
    sentiment_score = sentiment_analysis.sentiment
    print(sentiment_score)
    #print(dd)
    print(sentiment_score.polarity)



def pdfparser():
    fp = open(full_path, "rb")
    rsrcmgr = PDFResourceManager()
    retstr = io.StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    # Create a PDF interpreter object.
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    # Process each page contained in the document.
    data = ''
    for page in PDFPage.get_pages(fp):
        interpreter.process_page(page)
        print(retstr.getvalue())
        data = data + "\n" + retstr.getvalue()

    #print(data)
    write_txt(data, full_path)
    print("text file is ready")

def test_pdf():
    if True:
        pdfFileObj = open(full_path, 'rb')
        pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
        pdfText = ""
        for i in range(pdfReader.numPages):
            page = pdfReader.getPage(i)
            pdfText = pdfText + "\n" + page.extractText()
        print("--data--\n", pdfText)
        write_txt(pdfText, full_path)

def write_txt(data, file_orig_path):
    with open(os.getcwd()+"/"+"test.txt", "w", encoding="utf-8") as t:
        t.write(str(file_orig_path))
        t.write("\n")
        t.write(str(uuid.uuid1()))
        t.write("\n")
        t.write(str(data))
        t.close()
    return True

#pdfparser()
test1()
#test_pdf()