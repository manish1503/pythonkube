# import elasticsearch
import datetime
import threading
from CLASSIFIER.utils import most_frequent_words
from SCHEDULE.get_logger import get_log_object
from CONNECTIONS.ESConnection import *
# es = elasticsearch.Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
today_date = datetime.datetime.now().strftime("%Y-%m-%d")

log_path    = "../../LOGS"
count = 0
##  preparing test cases and words for similarity

def get_test_cases(index):
    """ test cases prepared """
    log = get_log_object(log_path, "test_sentences-"+index)
    test_sentences = []

    test_sentences.extend(get_records(index, 5, log))
    test_word_to_vec(test_sentences, log)

    return True

def get_records(index, sme_value, log):
    doc = {"_source": {"includes": ["ml_aggregrate"]}, "query": {"bool": {"filter": {"term": {"sme": sme_value}}}}}
    page = es.search(index=index, body=doc, size=100, scroll='90m', sort="date:asc")
    sid = page['_scroll_id']

    sample_sentences = []
    sample_sentences.extend(get_data(page, log))

    records = 100
    while (records < 5000):
        page = es.scroll(scroll_id=sid, scroll='100m')
        sid = page['_scroll_id']
        scroll_size = len(page['hits']['hits'])
        records += scroll_size
        log.info("No Of Records Processed : %s", records)
        if scroll_size > 0:
            sample_sentences.extend(get_data(page, log))
        else:
            break

    return sample_sentences

def get_data(page, log):
    test_sentences = []
    count = 0
    for p in page['hits']['hits']:
        if "ml_aggregrate" in p['_source']:
            test_sentences.append(p['_source']['ml_aggregrate'])
            log.info(p['_source']['ml_aggregrate'][:300])
            count += 1
            if count >= 50:
                break
    return test_sentences


def test_word_to_vec(test_sentences, log):
    test_words = most_frequent_words(test_sentences)
    for word in test_words:
        log.info(word)

get_test_cases("boston-med3d")