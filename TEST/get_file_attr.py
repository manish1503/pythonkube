import os
import time
from flask import json
from pprint import pprint

model_path = "../../MODEL"
log_path = "../../LOGS"

def get_attr(index):
    models = []
    logs = []
    for f in next(os.walk(model_path))[2]:
        if f.startswith(index):
            created_date = time.ctime(os.path.getmtime(model_path+"/"+f))
            models.append({f : created_date})

    for f in next(os.walk(log_path))[2]:
        if index in f:
            created_date = time.ctime(os.path.getmtime(log_path + "/" + f))
            logs.append({f : created_date})

    resp = {"logs": logs[:5], "models": models}
    pprint(resp)
    pprint(json.dumps(resp))


get_attr("sergio-spacex")