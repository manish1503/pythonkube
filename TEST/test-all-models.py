import os
import re
from sklearn.externals import joblib
import gensim
from nltk.corpus import stopwords
stops = set(stopwords.words("english"))

index_name = "sergio-sms"

##--------------------------
def filter_stop_words(raw_sentence):
    letters_only = re.sub("[^a-zA-Z0-9\u00E4\u00F6\u00FC\u00C4\u00D6\u00DC\u00df]", " ", raw_sentence)
    words = letters_only.lower().split()
    meaningful_words = [w for w in words if not w in stops]
    return (meaningful_words)


##---------------------------
def test_models(sentence=None, word=None):

    if word:
        pass
        # word_model_path = os.getcwd() + "/" + index_name + "_w2v.model"
        # word_2_vec_model = gensim.models.doc2vec.Word2Vec.load(word_model_path)
        # synonyms_w2v = word_2_vec_model.most_similar(word)
        # print(synonyms_w2v)

    if sentence:
        sent_model_path = "../../MODEL"+ "/"+ index_name + "_sentence_d2v.model"
        doc_2_vec_model = gensim.models.doc2vec.Doc2Vec.load(sent_model_path)
        tokens      = filter_stop_words(sentence)
        new_vector  = doc_2_vec_model.infer_vector(tokens)
        synonyms_d2v    = doc_2_vec_model.docvecs.most_similar([new_vector])

        print(synonyms_d2v)
        # tree_model      = joblib.load(os.getcwd()+"/"+ index_name + '-tree.model')
        # bayesian_model  = joblib.load(os.getcwd()+"/"+ index_name + '-bayesian.model')
        # logistic_model  = joblib.load(os.getcwd()+"/"+ index_name + '-logistic.model')
        #
        # pred_tree       = tree_model.predict([doc_2_vec_model.infer_vector(tokens)])[0]
        # pred_bayesian   = bayesian_model.predict([doc_2_vec_model.infer_vector(tokens)])[0]
        # pred_logistic   = logistic_model.predict([doc_2_vec_model.infer_vector(tokens)])[0]
        # print("\n tree : ",pred_tree, "\n bayesian : ",pred_bayesian, "\n logistic : ", pred_logistic)


sent = "medical holography market poised expand robust pace 2026 newsliner biomedical imaging technology bringing numerous innovations medical technology field medical imaging fastest growing industries medic"
test_models(sentence=sent, word=None)

