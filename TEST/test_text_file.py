import uuid
import io
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
full_path = "E:\\PROJECTS\\REDRABBIT3\\PYTHON\\DATA\\ankit-tennis\\DATA\\2018-04-10_RAW\\Rafael Nadal - Wikipedia.pdf"
dest_path = "E:\\PROJECTS\\REDRABBIT3\\PYTHON\\DATA\\ankit-tennis\DATA\\2018-04-10_RAW-TXT\\Rafael Nadal - Wikipedia.txt"

file_type = "pdf"

def convert_to_text():
    if file_type == "txt":
        txtfile = open(full_path, 'r')
        filedata = str(txtfile.readlines())
        write_txt(filedata, full_path, dest_path)

    if file_type == "pdf":
        data_set = open(full_path, 'rb')
        pdfFileObj = data_set
        rsrcmgr = PDFResourceManager()
        retstr = io.StringIO()
        codec = 'utf-8'
        laparams = LAParams()
        device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
        # Create a PDF interpreter object.
        interpreter = PDFPageInterpreter(rsrcmgr, device)
        # Process each page contained in the document.
        pdfText = ""
        for page in PDFPage.get_pages(pdfFileObj):
            interpreter.process_page(page)
            pdfText = pdfText + "\n" + retstr.getvalue()

        write_txt(pdfText, full_path, dest_path)



def write_txt(data, file_orig_path, file_dest_path):
    with open(file_dest_path, "w", encoding="utf-8") as t:
        t.write(str(file_orig_path))
        t.write("\n")
        t.write(str(uuid.uuid1()))
        t.write("\n")
        t.write(str(data))
        t.close()
    return True

convert_to_text()
