import schedule
import time
import os
# from elasticsearch import Elasticsearch

print(os.getcwd())
os.chdir("..")
print(os.getcwd())
from CONNECTIONS.ESConnection import *
from SCHEDULE.get_logger import get_log_object
from RETRAIN_PIPELINE.one_pt_one_sme_four_n_five import one_update_sme
from RETRAIN_PIPELINE.two_pt_two_create_temp_training_index import two_create_temp_index
from RETRAIN_PIPELINE.three_pt_one_train_models import three_pt_one_create_models
from RETRAIN_PIPELINE.three_pt_two_test_models import three_pt_two_test_models
from RETRAIN_PIPELINE.five_pt_one_update_ml_score import five_update_ml_score

log = get_log_object("../LOGS", "test_pipeline")
# es = Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")

def test_pipeline():
    projects = [('sergio','spacex',['echopixel', '3d glassless', 'autostereoscopy'])]

    #one     = one_update_sme(projects)
    # two     = two_create_temp_index(projects)        ## creating the training index
    # three   = three_pt_one_create_models(projects)
    four    = three_pt_two_test_models(projects)
    # five    = five_update_ml_score(projects)

    log.info("process completed on : %s", time.time())

test_pipeline()
