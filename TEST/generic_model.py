from sklearn.externals import joblib
model_path = "../../MODEL"
import gensim.downloader as api
glove_twitter_25_model = api.load("glove-twitter-25")
#glove_twitter_25_model = joblib.load(model_path+ "/" + 'glove-twitter-25.model')
top_words = glove_twitter_25_model.most_similar('spacex', topn=5)
print(top_words)