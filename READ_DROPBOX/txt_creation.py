import docx
import pandas as pd
import uuid
import os
import csv
import shutil
import io
import pickle
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams


def convert_to_txt(file, full_path, dest_path, fail_path, log):
    """
    Creating a txt version of different files/froromats
    :param file: name of file
    :param full_path: path of file in file system
    :param dest_path: path of the text file to written to
    :param fail_path: path to store file in case of error
    :param log: log object (created already)
    :return: True always
    """
    dest_path = dest_path + "/"+ file.split(".")[0] + ".txt"

    if file.endswith('.pdf'):
        try:
            pdfFileObj = open(full_path, 'rb')
            rsrcmgr = PDFResourceManager()
            retstr = io.StringIO()
            codec = 'utf-8'
            laparams = LAParams()
            device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
            # Create a PDF interpreter object.
            interpreter = PDFPageInterpreter(rsrcmgr, device)
            # Process each page contained in the document.
            pdfText = ""
            for page in PDFPage.get_pages(pdfFileObj):
                interpreter.process_page(page)
                ##print(retstr.getvalue())
                pdfText = pdfText + "\n" + retstr.getvalue()

            write_txt(pdfText, full_path, dest_path)
            #log.info("File has been written to disk")
        except:
            log.info("Reading Failed , Bad File Type Error : %s", str(file))
            try:
                shutil.copy(full_path, fail_path)
            except:
                pass

    ####---old code
    # if file.endswith('.pdf'):
    #     try:
    #         pdfFileObj = open(full_path, 'rb')
    #         pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    #         pdfText = ""
    #         for i in range(pdfReader.numPages):
    #             page = pdfReader.getPage(i)
    #             pdfText = pdfText + "\n" + page.extractText()
    #         write_txt(pdfText, full_path, dest_path)
    #         log.info("File has been written to disk")
    #     except:
    #         log.info("Reading Failed , Bad File Type Error : %s", str(file))
    #         try:
    #             shutil.copy(full_path, fail_path)
    #         except:
    #             pass

    elif file.endswith('.docx'):
        try:
            doc = docx.Document(full_path)
            docText = ""
            for i in doc.paragraphs:
                docText = docText + "\n" + i.text
            write_txt(docText, full_path, dest_path)
            #log.info("File has been written to disk")
        except:
            log.info("Reading Failed , Bad File Type Error : %s", str(file))
            try:
                shutil.copy(full_path, fail_path)
            except:
                pass

    elif file.endswith('.txt') or file.endswith(".json"):
        try:
            txtfile = open(full_path, 'r')
            filedata = str(txtfile.readlines())
            write_txt(filedata, full_path, dest_path)
        except:
            log.debug("Reading Failed , Bad File Type Error : %s", str(file))
            try:
                shutil.copy(full_path, fail_path)
            except:
                pass

    elif file.endswith('.xlsx') or file.endswith('.xls'):
        try:
            xl_file = pd.ExcelFile(full_path)
            dfs = {sheet_name: xl_file.parse(sheet_name)
                   for sheet_name in xl_file.sheet_names}
            write_txt(dfs, full_path, dest_path)

        except:
            log.info("Reading Failed , Bad File Type Error : %s", str(file))
            try:
                shutil.copy(full_path, fail_path)
            except:
                pass

    elif file.endswith('.csv'):
        try:
            with open(dest_path + "/" + os.path.splitext(file)[0] + ".txt", "w", encoding="utf-8") as my_output_file:
                with open(full_path, "r") as my_input_file:
                    [my_output_file.write(" ".join(row) + '\n') for row in csv.reader(my_input_file)]
                my_output_file.close()
            #log.info("File has been written to disk")
        except:
            log.info("Reading Failed , Bad File Type Error : %s", str(file))
            try:
                shutil.copy(full_path, fail_path)
            except:
                pass

    else:
        log.info("Reading Failed , Bad File Type Error : %s", str(file))
        try:
            shutil.copy(full_path, fail_path)
        except:
            pass

    return True

def write_txt(data, file_orig_path, file_dest_path):
    with open(file_dest_path, "w", encoding="utf-8") as t:
        t.write(str(file_orig_path))
        t.write("\n")
        t.write(str(uuid.uuid1()))
        t.write("\n")
        t.write(str(data))
        t.close()
    return True

