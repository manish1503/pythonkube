from DATABASE.dropbox_db import *
from FILTER.utils import dict_comparison, \
                         update_modified_dropbox_db, update_modified_db, update_dropbox_db
from READ_DROPBOX.read_box_live_new import read_pds_files
from SCHEDULE.get_logger import get_log_object

log = get_log_object("../LOGS", "dropbox")

def compare_dbs():
    """
    Getting Updates through Comparison
    :return: True
    """
    d1 = read_pds_files()
    d2 = dropbox_db
    add, remove, modified = dict_comparison(d1, d2)
    log.info("*--new added--* %s, %s, %s, %s, %s", add, "\n*--removed--*",remove, "\n*--new modified--*", modified)
    mod_list = []

    if modified:
        m_list = update_modified_dropbox_db(modified)
        mod_list.extend(m_list)
    if add:
        for i in add:
            mod_list.append(i)
            try:
                update_modified_db(
                    {i: [[d1.get(i)[0], d1.get(i)[1], d1.get(i)[1]]]})
                update_dropbox_db({i: [d1.get(i)[0], d1.get(i)[1]]})
            except:
                log.warning("Alert 1.0 please check.")

    ###To Work On Remove
    log.info("mod_list %s: ", mod_list)
    return mod_list






