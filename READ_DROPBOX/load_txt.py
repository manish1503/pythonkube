import os
import time
import datetime
from SCHEDULE.get_logger import get_log_object
from READ_DROPBOX.txt_creation import convert_to_txt
current_date = datetime.datetime.now().strftime("%Y-%m-%d")

def reform_to_txt(mod_list):
    """
    Creating Text Version of Each Files
    Read From DropBox
    """
    for i in mod_list:

        root_path = "../DATA/"+i
        data_path = root_path + "/"+ "DATA"
        log_path = root_path + "/" + "LOGS"

        for d in os.listdir(data_path):
            if not current_date == d.split("_")[0]:
                continue
            proj_path = data_path + "/" + d

            ###Getting Log object To log things
            log = get_log_object(log_path, d.lower())

            txt_path    = data_path+ "/" + current_date+ "_" + "RAW-TXT"
            fail_path   = data_path+ "/" + current_date+ "_" + "RAW-NOTREADABLE"
            #log.info("Iterating through directories.")

            if not os.path.exists(txt_path):
                os.mkdir(txt_path)
            if not os.path.exists(fail_path):
                os.mkdir(fail_path)

            for root, dirs, files in os.walk(proj_path):
                for file in files:
                    # log.info("Reading files from directory : %s", str(d))
                    # log.info("Creating Directory to Put Text files To ")
                    log.info("file : %s", file)
                    if file.endswith(".log"):
                        continue

                    full_path = root + '/' + file
                    #log.info("--full path-- %s",str(full_path))
                    convert_to_txt(file, full_path, txt_path, fail_path, log)
                    time.sleep(10)
                    #print("Txt File Created.")

            log.info("Directory has been Read. %s", str(d))

    log.info("Reading and Writing Complete.")
    return True
