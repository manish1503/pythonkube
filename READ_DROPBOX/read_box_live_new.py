import dropbox
import zipfile
from io import BytesIO
from FILTER.utils import human, update_dropbox_live_db
from SCHEDULE.get_logger import get_log_object


token = "pzXQ6vxQJ1AAAAAAAAAAG0oqa8i4yw2td9GEqdjU6tdNt3S75SK5FdM3h1y4l2kd" ##Manish


dbx = dropbox.Dropbox(token)
print("--account info--", dbx.users_get_current_account().account_type)

def read_pds_files():
    db = {}
    for i in dbx.files_list_folder("").entries:
        if not i.name.startswith("rr2"):
            continue
        if i.sharing_info: ##directory
            total_size = 0
            no_of_records = 0
            log = get_log_object("../LOGS", "dropbox")
            project_name = i.name.split("rr2-")[1]

            log.info("*****---***** %s",i.path_display)
            for f in dbx.files_list_folder(i.path_display).entries:

                if f.name.endswith(".zip"):
                    total_size = total_size + f.size
                    f, metadata = dbx.files_download(i.path_display + "/" + f.name)
                    zipfi = zipfile.ZipFile(BytesIO(metadata.content))
                    zip_names = zipfi.namelist()
                    no_of_records += len(zip_names)
                else:
                    total_size = total_size + f.size
                    no_of_records+=1

            t_size = human(total_size)
            dir_size = t_size.split()
            si = "{:.2f}".format(float(dir_size[0]))
            si_unit = dir_size[1]
            db.update({project_name: [no_of_records, si+" "+si_unit]})
            log.info("FOLDER READ. : %s, %s, %s", str(i.name), total_size, t_size)

        update_dropbox_live_db(db)
        #log.info("Drop box has been Read.")
    #log.info("complete.")
    return db

