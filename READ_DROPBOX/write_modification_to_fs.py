import dropbox
import os
import zipfile
from io import BytesIO
import datetime
from SCHEDULE.get_logger import get_log_object

token  = "pzXQ6vxQJ1AAAAAAAAAAG0oqa8i4yw2td9GEqdjU6tdNt3S75SK5FdM3h1y4l2kd" ##Manish


dbx = dropbox.Dropbox(token)
current_date = datetime.datetime.now().strftime("%Y-%m-%d")


def write_mod_files(mod_list):
    """ Reading modified db to write changes to file system """
    if mod_list:
        for i in mod_list:
            if not os.path.exists("../DATA" +"/"+ i):
                os.mkdir("../DATA"+ "/" + i)

            path = "../DATA" + "/"+ i
            if not os.path.exists(path+"/"+"DATA"):
                os.mkdir(path+"/"+"DATA")
            if not os.path.exists(path+ "/"+ "DATA"+ "/"+ current_date+ "_"+ "RAW"):
                os.mkdir(path+ "/"+ "DATA"+ "/"+ current_date+ "_"+ "RAW")
            if not os.path.exists(path+"/"+"LOGS"):
                os.mkdir(path+"/"+"LOGS")
            if not os.path.exists(path+"/"+"MODEL"):
                os.mkdir(path+"/"+"MODEL")

            data_path = path+ "/"+ "DATA"+ "/"+ current_date+ "_"+ "RAW"
            ##Getting log object to log things
            log_path  = path+ "/" + "LOGS"
            log = get_log_object(log_path, i)

            log.info("******************-------------********************* : %s",i)
            for f in dbx.files_list_folder("/"+"rr2-"+i).entries:
                if f.name.endswith(".zip"):
                    #log.info("extracting zip file : %s", str(f.name))
                    zip_file_name = f.name.split(".zip")[0]

                    if not os.path.exists(data_path + "/" + zip_file_name):
                        os.mkdir(data_path + "/" + zip_file_name)

                    f, metadata = dbx.files_download("/"+"rr2-"+i + "/" + f.name)
                    zipfi = zipfile.ZipFile(BytesIO(metadata.content))
                    zip_names = zipfi.namelist()

                    for name in zipfi.namelist():
                        unzipped_string = zipfi.open(name).read()
                        f = open(data_path + "/" + zip_file_name + "/" + name, 'wb')
                        f.write(unzipped_string)
                        f.close()
                else:
                    log.info("Files inside directory but not in zip : %s", str(f.name))
                    f, metadata = dbx.files_download("/" + "rr2-"+i + "/" + f.name)
                    f = open(data_path + "/" + f.name, 'wb')
                    try:
                        f.write(metadata.content)
                    except:
                        f.close()
                        continue

        print("Drop box has been Read.")
    else:
        print("No Updates Found in Data")

    print("complete.")
    return mod_list

