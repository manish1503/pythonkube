import datetime
import time
import re
from math import ceil
import elasticsearch
# from elasticsearch import Elasticsearch

from FILTER.entity_extraction import get_entity
from FILTER.get_classification_score_api import get_score, get_class_label_n_score
from FILTER.get_classifier_score import get_scores
from FILTER.profanity_lang_filter import check_en_language, get_sentiment_score, get_processed_text
from SCHEDULE.get_logger import get_log_object
from CONNECTIONS.ESConnection import *
# es = Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
current_date = datetime.datetime.now().date()
log = get_log_object("../LOGS", "crawler")


def index_search(index_name):
    resp = ''
    total_records = 0
    try:
        res             = es.search(index=index_name, size = 1000,request_timeout=100)
        resp            = res["hits"]["hits"]
        total_records   = res["hits"]["total"]
        log.info(" Number of records to be process %s for %s", total_records, index_name)
        return resp, total_records
    except elasticsearch.ConnectionError:
        log.info("ConnectionError : %s", index_name)
        return resp, total_records
    except:
        log.info("Error - Not Defined : %s", index_name)
        return resp, total_records


def index_check(index_name):
    body = {"settings": {
        "index": {
            "number_of_shards": 1,
            "number_of_replicas": 2
        }
    }}
    log.info("checking and creating index")
    try:
        es.search(index=index_name)
    except elasticsearch.ConnectionTimeout:
        es.search(index=index_name)
    except elasticsearch.exceptions.NotFoundError:
        log.info("Index Not Found, Creating index")
        try:
            es.indices.create(index=index_name, ignore=400, body=body)
        except elasticsearch.ConnectionTimeout:
            es.indices.create(index=index_name, ignore=400, body=body)
        except:
            es.indices.create(index=index_name, ignore=400, body=body)
    return True


def delete_index(index_name):
    try:
        if es.indices.exists(index=index_name):
            es.indices.delete(index=index_name, ignore=[400, 404], request_timeout=100)
            log.info("Index Found, deleted index : %s", index_name)
    except:
        log.warning("Attempt to Delete - some error occured : %s", index_name)
        pass
    return True


def is_index_found(index_name):
    try:
        es.search(index=index_name,request_timeout=100)
        return True
    except elasticsearch.exceptions.NotFoundError:
        return False


def is_index_exists(index_name):
    if es.indices.exists(index=index_name,request_timeout=100):
        return True
    else:
        return False


def get_index_count(index_name):
    """ getting index count """
    try:
        count = es.count(index=index_name)['count']
    except:
        count = 0
    return count


def is_record_exist(index_name, doc_type, id):
    """ check if record exist """
    try:
        resp = es.exists(index=index_name, doc_type=doc_type, id=id)
    except:
        resp = False

    return resp


def scroll_api_es(index_name, doc_type):
    """ Scroll API """
    page = es.search(
        index=index_name,
        doc_type=doc_type, #'document',
        scroll='20s',
        size=10000)

    sid = page['_scroll_id']
    scroll_size = page['hits']['total']
    log.info("scroll size : %s", scroll_size)

    while (scroll_size > 0):
        page = es.scroll(scroll_id=sid, scroll='20s')
        # Update the scroll ID
        sid = page['_scroll_id']

        # Get the number of results that we returned in the last scroll
        scroll_size = len(page['hits']['hits'])

    log.info("scrolling complete")
    return True

# from raw to process
def index_to_es(index_name, doc_type, guid, doc, root, file, log):
    """ indexing to es """
    resp_flag = False
    try:
        res = es.index(index=index_name, doc_type=doc_type, id=guid, body=doc)
        if res['created'] :
            log.warning(guid)
            log.info('New record is created')
        else :
            log.info('Record has been updated')

        resp_flag = True

    except elasticsearch.ConnectionError:
        log.warning("ConnectionError - Index Creation Failed For File : %s, %s", str(root), str(file))

    except elasticsearch.SerializationError:
        log.warning("SerializationErrorr - Index Creation Failed For File : %s, %s", str(root), str(file))

    except elasticsearch.ConnectionTimeout:
        log.warning("ConnectionTimeout - Index Creation Failed For File : %s, %s", str(root), str(file))

    except elasticsearch.RequestError:
        log.error("ALERT 1.0 - mapping doc : %s", doc)
        log.warning("RequestError - Index Creation Failed For File : %s, %s", str(root), str(file))

    except elasticsearch.TransportError:
        log.warning("TransportError - Index Creation Failed For File : %s, %s", str(root), str(file))

    except elasticsearch.NotFoundError:
        log.warning("NotFoundError - Index Creation Failed For File : %s, %s", str(root), str(file))

    except Exception as e:
        log.warning(e.args)
        log.warning("Undefined Error - Index Creation Failed For File : %s, %s", str(root), str(file))

    if resp_flag:
        #log.info("Raw to Process append Completed")
        return True
    else:
        try:
            time.sleep(0.1)
            doc.update({'date' : current_date})
            doc.update({'processedDate': current_date})
            res = es.index(index=index_name, doc_type=doc_type, id=guid, body=doc)
            log.info("tryng second time raw to processed: %s",res['created'])
        except:
            log.info("Tryng second time: append from raw to process Failed")

        return False


def prepare_mapping_doc(i, mapping, label, project_name, crawler_name, model_exist, search_terms, similar_terms=None):
    ##Getting Raw Text and Cheking Language Content##
    description = mapping['description']
    raw_text = i["_source"][description] if description in i["_source"] else ""

    doc = {}
    modified_by = "crawler_processing.py"

    vars = {
        'project': project_name,
        'modifiedBy': modified_by,
        'modifiedDate': current_date,
        'label': label
    }

    try:
        if not check_en_language(raw_text.encode("ascii", "ignore").decode()):
            log.info("Raw to process - text is not in english language ")
            return doc, False
    except:
        pass

    title  = ''
    author = ''
    entities = []
    '''getting the mappings from config file'''
    for key in mapping:
        if mapping[key]:
            if mapping[key].startswith("##"):
                try:
                    doc.update({key: vars.get(key)})
                except:
                    pass
            elif mapping[key].startswith("#"):
                doc.update({key: mapping[key].split("#")[1]})
            else:
                if "." in mapping[key] and not "[]" in mapping[key]:
                    tmp_resp = mapping[key].split(".")
                    len_val = len(tmp_resp)
                    if len_val == 2:
                        mapping_date = i["_source"][tmp_resp[0]][tmp_resp[1]]
                        if key == "date":
                            if not mapping_date:
                                mapping_date = current_date
                                doc.update({key: mapping_date})
                            else:
                                if "GMT" in mapping_date:
                                    doc.update({key: datetime.datetime.strptime(mapping_date.split('GMT')[0].strip(),
                                                           "%a, %m %b %Y %H:%M:%S").date()}),
                                else:
                                    doc.update({key: mapping_date})
                        else:
                            if key == "title":
                                title = mapping_date
                            doc.update({key: mapping_date})

                    elif len_val == 3:
                        mapping_date = i["_source"][tmp_resp[0]][tmp_resp[1]][tmp_resp[2]]
                        if key == "date":
                            if not mapping_date:
                                mapping_date = current_date
                                doc.update({key: mapping_date})
                            else:
                                doc.update({key: mapping_date})
                        else:
                            if key == "title":
                                title = mapping_date
                            doc.update({key: mapping_date})

                elif "[]" in mapping[key]:
                    tmp_resp_splt = mapping[key].split("[]")[0]
                    tmp_resp = tmp_resp_splt.split(".")

                    field_key = tmp_resp[1]

                    len_val = len(tmp_resp)

                    if len_val == 2:
                        value = i["_source"][tmp_resp[0]][tmp_resp[1]]
                    elif len_val == 3:
                        value = i["_source"][tmp_resp[0][tmp_resp[1]][tmp_resp[2]]]

                    val_list = ''

                    if field_key == "user_mentions":
                        if value:
                            try:
                                for e in value:
                                    if val_list:
                                        val_list = val_list + "," + e['screen_name']
                                    else:
                                        val_list = val_list + e['screen_name']
                            except:
                                pass

                    elif field_key == "hashtags":
                        if value:
                            try:
                                for e in value:
                                    if val_list:
                                        val_list = val_list + "," + e['text']
                                    else:
                                        val_list = val_list + e['text']
                            except:
                                pass

                    else:
                        if value:
                            try:
                                for e in value:
                                    if val_list:
                                        val_list = val_list + "," + e['name']
                                    else:
                                        val_list = val_list + e['name']
                            except:
                                pass

                    if key.startswith("entity"):
                        entities.append(val_list)

                    doc.update({key: val_list})

                elif "indexOf" in mapping[key]:
                    index_val = mapping[key].split("'")[1]
                    try:
                        doc.update({key: raw_text[raw_text.index(index_val):][:200]})
                    except ValueError:
                        doc.update({key: ""})

                else:
                    if key == "tm_aggregate":
                        doc.update({key: ''})
                    elif key == "ml_aggregrate":
                        doc.update({key: ''})
                    elif key == "score_emotion":
                        doc.update({key: ''})
                    elif key == "description":
                        doc.update({key: raw_text[:300]})
                    # elif key == "label":
                    #     doc.update({key: label})
                    elif key == "date":
                        date_val = i["_source"][mapping[key]] if "date" in i["_source"] else current_date
                        if "GMT" in str(date_val):
                            try:
                                date_tz = datetime.datetime.strptime(str(date_val).split('GMT')[0].strip(),
                                                           "%a, %m %b %Y %H:%M:%S").date()
                            except:
                                dd = str(date_val).replace(",", "").split()
                                da = str(dd[1]) + "-" + str(dd[2]) + "-" + str(dd[3])
                                date_tz = datetime.datetime.strptime(da, "%d-%b-%Y").date()

                            doc.update({key: date_tz})
                        else:
                            doc.update({key: date_val})
                    else:
                        o_val = i["_source"][mapping[key]] if mapping[key] in i["_source"] else ""
                        if key == "publisher":
                            author = o_val
                        elif key == "title":
                            title = o_val
                        doc.update({key: o_val})

    if not "date" in doc:
        doc.update({'date': datetime.datetime.now().date()})
    else:
        if not doc["date"]:
            doc.update({'date': datetime.datetime.now().date()})

    if not "modifiedDate" in doc:
        doc.update({'modifiedDate': datetime.datetime.now().date()})
    else:
        if not doc["modifiedDate"]:
            doc.update({'modifiedDate': datetime.datetime.now().date()})

    '''Data Processing & Cleansing - Processing Text and calculating sentiment score'''
    person, org, location = get_entity(raw_text)

    entities_aggr = " ".join(entities)
    entities_aggr = entities_aggr + person + org + location
    raw_text_aggr = title+ author + entities_aggr+ raw_text

    processed_text = get_processed_text(raw_text_aggr)
    log.info("6 log " + processed_text)
    if not processed_text :
         return doc, False

    sentiment_score = get_sentiment_score(raw_text)
    #log.debug('before Model exists')
    if model_exist:
        #log.debug('In Model Exists')
        tree_score, logistic_score, bayesian_score, score_subject, classification_label = get_scores(processed_text)
        #log.debug(project_name)
        #log.debug("tree_score, logistic_score, bayesian_score, score_sentiment, classification_label : %s,%s,%s,%s,%s", tree_score, logistic_score, bayesian_score, sentiment_score, classification_label)

        classification_score = 0
        if classification_label.lower() == "subject":
            classification_score = 1

        # score_subject, classification_label = get_class_label_n_score(project_name, processed_text)
        # bayesian_score  = get_score("bayesian", project_name, processed_text)
        # tree_score      = get_score("tree", project_name, processed_text)
        # logistic_score  = get_score("logistic", project_name, processed_text)
        ml_score        = float(sentiment_score) + float(bayesian_score) + float(tree_score) + float(logistic_score) + float(classification_score)
        log.debug("ml_score : %s", ml_score)
    else:
        classification_label = "no-subject"
        bayesian_score = tree_score = logistic_score = ml_score = score_subject = 0

    if not crawler_name in ["dropbox", "bing", "urlcrawl"]:   # start calculating SME
        """ calculating sme scores """
        total_terms = []
        total_terms.extend(search_terms)
        total_terms.extend(similar_terms)

        sme_four_count = 0
        sme_three_count = 0
        sme_two_count = 0

        total_terms_size = len(total_terms)
        actual_total_terms_size = len(total_terms)

        high_value = 1 / 3 * total_terms_size
        low_value = 1

        ml_aggr_size = len(processed_text.split())
        if ml_aggr_size <= 20:
            high_value = high_value * 2

        ''' term search '''
        for term in search_terms:
            term_match = re.findall("\\b" + term + "\\b", processed_text)
            if term_match:
                total_terms_size -= high_value

        ''' contenxt search '''
        for sim_term in similar_terms:
            sim_match = re.findall("\\b" + sim_term + "\\b", processed_text)
            if sim_match:
                total_terms_size -= low_value

        if total_terms_size <= ceil(actual_total_terms_size * 30 / 100):
            sme = 4
            sme_four_count += 1
        elif ceil(actual_total_terms_size * 30 / 100) <= ceil(total_terms_size) <= ceil(actual_total_terms_size * 60 / 100):
            sme = 3
            sme_three_count += 1
        elif ceil(actual_total_terms_size * 60 / 100) <= ceil(total_terms_size) <= ceil(actual_total_terms_size * 80 / 100):
            sme = 2
            sme_two_count += 1
        else:
            sme = 1

    else:
        sme = 5

    doc.update({'ml_aggregrate' : processed_text})
    doc.update({'score_emotion' : float(sentiment_score)})
    doc.update({'sme': sme})
    doc.update({'predict02': classification_label})

    doc.update({'entity_person': person})
    doc.update({'entity_org': org})
    doc.update({'entity_location': location})

    doc.update({'score_medic': "{:.2f}".format(score_subject)})
    doc.update({'score_ml': float(ml_score)})
    doc.update({'score_naive': float(bayesian_score)})
    doc.update({'score_nn': float(logistic_score)})
    doc.update({'score_svm': float(0)})
    doc.update({'score_tree': float(tree_score)})
    doc.update({'processedDate': current_date})

    return doc, True