# from elasticsearch import  Elasticsearch
from CONNECTIONS.ESConnection import *
import datetime
import os
import time
import re
import json
import unicodedata

from SCHEDULE.get_logger import get_log_object
from READ_DROPBOX.index_requests import index_check, index_to_es, delete_index
from FILTER.get_classification_score_api import get_score, get_class_label_n_score
from FILTER.profanity_lang_filter import check_en_language, get_processed_text, get_sentiment_score
from FILTER.entity_extraction import get_entity
from FILTER.utils import update_raw_index_db, reinitialize_dropbox_live_db, reinitialize_dropbox_modified_db
from DATABASE.raw_index_db import *
data = json.load(open("."+"/RAW_TO_PROCESS/"+"crawler-mapping.json"))

current_date = datetime.datetime.now().strftime("%Y-%m-%d")
# es = Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
dropbox_url = "https://www.dropbox.com/preview/"

def raw_n_processed_index_to_es(mod_list):
    """
    Reading TXT Files To Create RAW &
    Processed Index in ES
    """
    crawler_name = 'dropbox'
    try:
        mapping = data[crawler_name]
    except:
        print("No Mapping Found For Crawler :", str(crawler_name))
        return False

    for i in mod_list:
        raw_text_path = "../DATA/"+ i + "/"+ "DATA" + "/"+ current_date+ "_" + "RAW-TXT"
        project_name = index_name = i.lower()

        ###Getting Log object To log things
        log_path = ".." + "/" + "LOGS"
        log      = get_log_object(log_path, project_name)

        if project_name+"-seed" in raw_index_db:
            i_value = raw_index_db.get(project_name+"-seed")[1].split("-",1)
            i_index = str(int(i_value[0])+1)+ "-" + i_value[1]
            raw_index_name = i_index
            if int(i_value[0]) - 5 > 0:
                index_to_del = str(int(i_value[0]) - 5) + "-" + i_value[1]
                delete_index(index_to_del)
        else:
            raw_index_name = "1"+ "-" + "raw"+ "-"+project_name+"-seed"

        ###Updating Index Count for Project in DB###
        log.info("-----------*--------- %s",{project_name: [current_date, raw_index_name]})
        update_raw_index_db({project_name+"-seed": [current_date, raw_index_name]})
        ###Cheking Index###
        index_check(raw_index_name)

        for root, dirs, files in os.walk(raw_text_path):
            for file in files:
                time.sleep(3)

                path = root + "/" + file

                f = open(path, "rb")
                f_name = f.readlines()[0]
                file_path = str(f_name).split("\\r\\n")[0].replace("b'", "")
                allpath = file_path.split('/')
                f_name = allpath[len(allpath) -1]

                f = open(path, "rb")
                f_data = f.readlines()[1]
                file_guid = str(f_data).split("\\r\\n")[0].replace("b'", "")

                f = open(path, "rb")
                file_data  = str(f.readlines()[2:]).split()[:10000]
                file_text = ' '.join([str(i).replace("\\r\\n", "").replace("b''","").replace(",","").replace("b'","") \
                                     .replace("\\", "").replace("'", "")
                                             for i in file_data])

                cln_text = re.sub(' +', ' ', file_text)
                c_cln_text = re.sub(r'(\n\s*)+\n+', '\n\n', cln_text)

                path = dropbox_url+ 'rr2-' +project_name + "/" + f_name
                guid = file_guid
                raw_text = c_cln_text
                textshort = raw_text.split()[100:400]
                text = ' '.join(textshort)

                ################------------RAW INDEXING--------------################
                raw_doc = {
                    'path': path,
                    'text': str(unicodedata.normalize('NFKD', text).encode('ascii','ignore'))[2:-1], #raw_text[:300].encode("ascii", "ignore").decode(),
                    'guid': guid
                }
                raw_resp = index_to_es(raw_index_name, 'raw', guid, raw_doc, root, file, log)
                raw_text = raw_text + " " + file
                try:

                    log.info("checking language of raw text to en")
                    if not check_en_language(raw_text):
                        log.warning("language is not en")
                        continue

                    processed_text = get_processed_text(raw_text)
                    subject = processed_text[:100]
                except:
                    processed_text = ''
                    subject = ''

                person, org, location = get_entity(raw_text)
                sentiment_score = get_sentiment_score(raw_text)

                ''' getting classfifer scores '''
                bayesian_score = get_score("bayesian", project_name, processed_text)
                tree_score = get_score("tree", project_name, processed_text)
                logistic_score = get_score("logistic", project_name, processed_text)

                score_subject, classification_label = get_class_label_n_score(project_name, processed_text)
                #sent2vec_score = 1 if classification_label == "subject" else 0
                sent2vec_score = 1
                classification_label = "no-subject"

                ''' calculating ml score '''
                ml_score = float(sentiment_score) + float(bayesian_score) + float(tree_score) + \
                           float(sent2vec_score) + float(logistic_score)

                vars_process = {
                    'date'          : datetime.datetime.now().date(),
                    "modifiedDate"  : datetime.datetime.now().date(),
                    "modifiedBy"    : "raw_n_processed_to_es.py",
                    'description'   : str(unicodedata.normalize('NFKD', text).encode('ascii','ignore'))[2:-1].replace("'","").replace('[','').replace(']',''). \
                        replace("\\x0c", "").replace('\\x80', '').replace('\\x99', '').replace("\\x93","").replace('\\x9d',''). \
                        replace('\\xe2', '').replace("\\x9c", "").replace("\\xa6", "").replace('\\','').replace("\\xc2","").replace("\\xa0",""),

                    'identifier'    : path,
                    'label'         : project_name.split("-",1)[1],
                    'ml_aggregrate' : processed_text,
                    'project'       : project_name,
                    'score_emotion' :  sentiment_score,
                    'source'        : path,
                    'subject'       : subject,
                    'title'         : file,

                    'score_medic'   : "{:.2f}".format(score_subject),
                    'score_ml'      : "{:.2f}".format(ml_score),
                    'score_naive'   : bayesian_score,
                    'score_nn'      : logistic_score,
                    'score_svm'     : 0,
                    'score_tree'    : tree_score,
                    'predict02'     : classification_label,
                    'entity_person' : person,
                    'entity_org'    : org,
                    'entity_location' : location
                }

                ''' Preparing mapping '''
                processed_doc = {}
                for key in mapping:
                    if str(mapping[key]).startswith("##"):
                        processed_doc.update({key : vars_process.get(key)})
                    else:
                        processed_doc.update({key : mapping[key]})

                ''' processed indexing '''
                processed_resp =index_to_es(index_name, 'processed', guid, processed_doc, root, file, log)
                if not processed_resp:
                    log.warning("Index Creation Failed")
                    continue
                log.info("Processed Index Created for file: %s", file)

            log.info("All The Files have Been Indexed")


        log.info("Directory Has Been Read. : %s", raw_text_path)

    ##Re-initialize the db's##
    reinitialize_dropbox_modified_db()
    reinitialize_dropbox_live_db()
    print("Cleaned Local DBs")
    return True