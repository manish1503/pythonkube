import psutil
from flask import json

def status():
    running_program_list = []
    for proc in psutil.process_iter(attrs=['pid', 'name']):
        k = proc.info['name']
        if k == 'python' :
            p = psutil.Process(proc.info['pid'])
            command = p.cmdline()
            program = command[1].split('/')
            running_program_list.append(program[len(program)-1])
    k = list(set(running_program_list))
    return json.jsonify(k)

