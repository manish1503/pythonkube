print("Running Api 3")
from flask import Flask
from flask_cors import CORS
from flask import request
from flask import json

# os.chdir("..")
from DATABASE.processed_index_db import processed_index_db
from DATABASE.raw_index_db import raw_index_db
import os
import re
app = Flask(__name__)
CORS(app)
print(os.getcwd())
model_path = os.getenv("MODELS", "../../MODEL")
log_path = os.getenv("LOGS", "../../LOGS")
print("Testing",os.path.dirname(os.path.abspath(model_path)))
database_path = os.getenv("DATABASE", "../DATABASE")
model_path = os.path.join(os.path.dirname(os.path.abspath(model_path)), "model")
log_path = os.path.join(os.path.dirname(os.path.abspath(log_path)), "logs")
print(model_path)

def find(name, path):
    results = []
    for root, dirs, files in os.walk(path):
        for i in files:
            if re.search(name, i):
                rm_path = os.path.join(path, i)
                os.remove(rm_path)
                results.append(i)
    return results


def delete_raw_index_name(project, database,filename):
    print(database)
    doc = {key: value for key, value in database.items() if not re.search(project, key)}
    print(doc)
    database.update(doc)
    db_path = os.path.join(os.path.dirname((os.path.abspath(database_path))), "database")
    file = open(os.path.join(db_path, filename), "w")
    if database == processed_index_db:
        file.write("processed_index_db=" + repr(doc) + '\n')
    else:
        file.write("raw_index_db=" + repr(doc) + '\n')
    file.close()


@app.route("/deletefiles", methods=["POST"])
def deletemodellog():
    resp = request.get_data()
    data = json.loads(resp.decode('utf-8'))
    project_name = data['project']
    user = data['user']
    project = user + "-" + project_name
    print(project)
    delete_raw_index_name(project, processed_index_db, "processed_index_db.py")
    delete_raw_index_name(project, raw_index_db, "raw_index_db.py")
    result = find(project, model_path)
    print(model_path,result)
    result = find(project, log_path)
    print(log_path, result)
    return ""


def main():
    app.run(debug=True)


if __name__ == "__main__":
    main()
