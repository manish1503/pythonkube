
import time
import datetime
import gensim
import logging
import dropbox
import os
import re
import nltk
import sys
import gensim.downloader as api
sys.path.append(os.getcwd())
from flask import Flask
from flask_cors import CORS
from flask import json
from flask import request
from flask import jsonify
from gensim import models
from DATABASE.processed_index_db import processed_index_db
from DATABASE.raw_index_db import raw_index_db
from CONNECTIONS.ESConnection import *
from nltk.corpus import stopwords
from sklearn.externals import joblib
# from SCHEDULE.schedule_crawlers import schedule_crawler
# from SCHEDULE.schedule_rr2 import schedule_training
# from SCHEDULE.schedule_dropbox import dropbox_crawler
# from threading import Thread
from API.statusfile import status
from SCHEDULE.get_logger import get_log_object

nltk.download('all')
nltk.download('stopwords')
print("before load")
glove_twitter_25_model = api.load("glove-twitter-25")
print("after load")

database_path = os.getenv("DATABASE", "./DATABASE")
model_path = os.getenv("MODELS", "../MODEL")
log_path = os.getenv("LOGS", "../LOGS")
print(os.path.join(os.path.dirname(os.path.abspath(model_path)),"model"))
stops = set(stopwords.words("english"))
model_path = os.path.join(os.path.dirname(os.path.abspath(model_path)), "model")
log_path = os.path.join(os.path.dirname(os.path.abspath(log_path)), "logs")
print(log_path)
# current_date = datetime.datetime.now().date()
token = "3_C_Ykj6AfQAAAAAAAAi6bQsDghRNAB1H3DRpPZu4YwxC8h5stkArcVvwafAi9k_"
dbx = dropbox.Dropbox(token)

app = Flask(__name__)
CORS(app)
MAX_API_CALL_LIMIT = 100
python = os.environ.get('PYTHON')
hostname = python.split("//")[1].split(':')[0]
print(hostname)


def sentence_to_words( raw_sentence ):
    letters_only = re.sub("[^a-zA-Z0-9\u00E4\u00F6\u00FC\u00C4\u00D6\u00DC\u00df]", " ", raw_sentence)
    words = letters_only.lower().split()
    meaningful_words = [w for w in words if not w in stops]
    return(meaningful_words)


logfile = get_log_object("../LOGS", "restapi_w2v")


def logfunction():
    # logfile = get_log_object(log_path, "restapi_w2v")
    # logging.basicConfig(filename=logfile,level=logging.DEBUG)
    return ""


@app.route("/domainselect", methods=["POST"])
def domainselect():
    modelWiki_d2v = models.Doc2Vec.load(os.path.join(model_path, 'Sentence_d2v.model'), mmap='r')
    logfunction()

    if not request.json:
        return json.dumps('Please enter arguments in order /domainselect?sentence=''TEST SENTENCE''')
    req_data = request.json
    sent = req_data['sentence']
    text = sent.lower()

    if os.path.exists(model_path + '/' + 'Sentence_d2v.model'):
        tokens = sentence_to_words(text)
        #print(tokens)
        tokens = "test test"
        new_vector = modelWiki_d2v.infer_vector(tokens)
        try:
            best = modelWiki_d2v.docvecs.most_similar([new_vector])[:10]
            return jsonify(best)
        except:
            logging.info('No similar word found for the word : %s', text)
            return json.jsonify(error=404, text=str('No similar word found in domain for the word : '+text)), 404
    else:
        logging.info('No model found for the domain')
        return json.jsonify(error=404, text=str('No model found')), 404


@app.route("/getsubject", methods=["POST"])
def getsubject():
    if not request.json or len(request.json)<2:
        return json.jsonify(error=500, text='Please provide json input as {sentence=''TEST SENTENCE'',user=''name''}'), 500
    req_data = request.json
    user = req_data['user']
    sent = req_data['sentence']
    text = sent.lower()
    logfunction()
    if os.path.exists(model_path + '/' + user+'_sentence_d2v.model'):
        tokens = sentence_to_words(text)
        model_index_d2v = gensim.models.Doc2Vec.load(os.path.join(model_path, user+'_sentence_d2v.model'), mmap='r')
        new_vector = model_index_d2v.infer_vector(tokens)
        try:
            best = model_index_d2v.docvecs.most_similar([new_vector])
            return jsonify(best)
        except:
            logging.info('No similar word found for the word : %s' ,text)
            return json.jsonify(error=404, text=str('No similar word found in domain for the word : '+text)), 404
    else:
        logging.info('No model found for the domain')
        return json.jsonify(error=404, text=str(user+'- No model found')), 404


@app.route("/logistic", methods=["GET","POST"])
def predictlabel():
    if not request.json or len(request.json)<2:
        return json.jsonify(error=500, text='Please provide json input as {sentence=''TEST SENTENCE'',user=''name''}'), 500
    req_data = request.json
    user = req_data['user']
    sent = req_data['sentence']
    text = sent.lower()
    tokens = sentence_to_words(text)
    logfunction()

    if os.path.exists(model_path + '/' + user+'_sentence_d2v.model'):
        try:
            model_index_d2v = gensim.models.Doc2Vec.load(os.path.join(model_path, user + '_sentence_d2v.model'),
                                                         mmap='r')
            loaded_logistic_model = joblib.load(os.path.join(model_path, user + '-logistic.model'))
            logging.info(loaded_logistic_model)
            pred = loaded_logistic_model.predict([model_index_d2v.infer_vector(tokens)])[0]
            if pred == 0:
                best = "no-subject"
            else:
                best = "subject"
            return jsonify(best)
        except:
            logging.info('No model found or prediction made for the sentence : %s' ,text)
            return json.jsonify(error=404, text=str('No model found or prediction made for the sentence : '+text)), 404
    else:
        logging.info('No model found for the user')
        return json.jsonify(error=404, text=str(user+'- No model found')), 404


@app.route("/bayesian", methods=["POST"])
def labelprobability():
    if not request.json or len(request.json)<2:
        return json.jsonify(error=500, text='Please provide json input as {sentence=''TEST SENTENCE'',user=''name''}'), 500
    req_data = request.json
    user = req_data['user']
    sent = req_data['sentence']
    text = sent.lower()
    tokens = sentence_to_words(text)
    logfunction()

    if os.path.exists(model_path + '/' + user+'_sentence_d2v.model'):
        try:
            model_index_d2v = gensim.models.Doc2Vec.load(os.path.join(model_path, user + '_sentence_d2v.model'),
                                                         mmap='r')
            loaded_bayesian_model = joblib.load(os.path.join(model_path, user + '-bayesian.model'))
            logging.info(loaded_bayesian_model)
            pred = loaded_bayesian_model.predict([model_index_d2v.infer_vector(tokens)])[0]
            return jsonify(pred)
        except:
            logging.info('No model found or prediction made for the sentence : %s' ,text)
            return json.jsonify(error=404, text=str('No prediction made for the sentence : '+text)), 404
    else:
        logging.info('No model found for the user')
        return json.jsonify(error=404, text=str(user+'- No model found')), 404


@app.route("/tree", methods=["POST"])
def labeltree():
    if not request.json or len(request.json)<2:
        return json.jsonify(error=500, text='Please provide json input as {sentence=''TEST SENTENCE'',user=''name''}'), 500
    req_data = request.json
    user = req_data['user']
    sent = req_data['sentence']
    text = sent.lower()
    tokens = sentence_to_words(text)
    logfunction()

    if os.path.exists(model_path + '/' + user+'_sentence_d2v.model'):
        try:
            model_index_d2v = gensim.models.Doc2Vec.load(os.path.join(model_path, user+'_sentence_d2v.model'), mmap='r')
            loaded_tree_model = joblib.load(os.path.join(model_path, user+'-tree.model'))
            logging.info(loaded_tree_model)
            pred = loaded_tree_model.predict([model_index_d2v.infer_vector(tokens)])[0]
            return jsonify(pred)
        except:
            logging.info('No model found or no prediction made for the sentence : %s' ,text)
            return json.jsonify(error=404, text=str('No model found or no prediction made for the sent : '+text)), 404
    else:
        logging.info('No model found for the user')
        return json.jsonify(error=404, text=str(user+'- No model found')), 404


@app.route("/similar", methods=["GET"])
def getSimilar():
    if len(request.args) < 2 or request.args.get("word") == '' :
        return json.dumps('Please enter arguments in order /similar?domains=''DOMAIN_NAME''&word=''WORD''')
    text = request.args.get("word")
    text = text.lower()
    domains = request.args.get("domains")
    logfunction()

    if os.path.exists(model_path + '/' + domains + '_w2v.model'):
        modelWiki_w2v = models.Word2Vec.load(os.path.join(model_path,  domains+ '_w2v.model'), mmap='r')
        try:
            synonyms = modelWiki_w2v.most_similar(text, topn=10)
            logging.info('Most similar word for '+domains+ '-' +text+ 'are : %s', synonyms)
            return jsonify(synonyms)
        except:
            logging.info('No similar word found in domain %s for the word : %s' ,domains,text)
            return json.jsonify(error=404, text=str('No similar word found in domain '+domains+' for the word : '+text)), 404
    else:
        logging.info('No model found for the domain %s' , domains)
        return json.jsonify(error=404, text=str(domains+'- No model found')), 404


@app.route("/tw-gensim", methods=["GET"])
def genericSimilar():
    print("Start of getsimilar")
    if len(request.args) < 1 or request.args.get("word") == '' :
        return json.jsonify(error=500, text='Please enter arguments in order /generic-similar?word=''WORD''?model=''project_name'''), 500
    text = request.args.get("word")
    text = text.lower()
    model_name = request.args.get("model")
    logfunction()

    try:
        if not model_name:
            synonyms = glove_twitter_25_model.most_similar(text, topn=10)
            logging.info('Most similar word for '+"glove_twitter_25"+ '-' +text+ 'are : %s', synonyms)
            return jsonify(synonyms)
        else:
            if os.path.exists(model_path + '/' + model_name + '_w2v.model'):
                model_tw_w2v = gensim.models.Word2Vec.load(os.path.join(model_path, model_name + '_w2v.model'), mmap='r')
                try:
                    synonyms = model_tw_w2v.most_similar(text, topn=10)
                    logging.info('Most similar word for ' + model_name + '-' + text + 'are : %s', synonyms)
                    return jsonify(synonyms)
                except:
                    logging.info('No similar word found in domain %s for the word : %s', model_name, text)
                    return json.jsonify(error=404, text=str(
                        'No similar word found in domain ' + model_name + ' for the word : ' + text)), 404
            else:
                logging.info('No model found for the domain %s', model_name)
                return json.jsonify(error=404, text=str(model_name + '- No model found')), 404
    except:
        logging.info('No similar word found in domain %s for the word : %s' ,"glove_twitter_25",text)
        return json.jsonify(error=404, text=str('No similar word found in domain '+"glove_twitter_25"+' for the word : '+text)), 404


@app.route("/usersimilar", methods=["GET"])
def userSimilar():
    if len(request.args) < 2 or request.args.get("word") == '' :
        return json.dumps('Please enter arguments in order /usersimilar?user=''USER NAME''&word=''WORD''')
    text = request.args.get("word")
    text = text.lower()
    user = request.args.get("user")
    model_name = user.lower()
    logfunction()

    if os.path.exists(model_path + '/' + model_name + '_w2v.model'):
        modelWiki_w2v = gensim.models.Word2Vec.load(os.path.join(model_path, model_name + '_w2v.model'), mmap='r')
        try:
            synonyms = modelWiki_w2v.most_similar(text, topn=10)
            logging.info('Most similar word for '+user+ '-' +text+ 'are : %s', synonyms)
            return jsonify(synonyms)
        except:
            logging.info('No similar word found in user %s for the word : %s' ,user, text)
            return json.jsonify(error=404, text=str('No similar word found in user '+user+' for the word : '+text)), 404
    else:
        logging.info('No model found for the user %s' , user)
        return json.jsonify(error=404, text=str(user+'- No model found')), 404

@app.route("/medic", methods=["GET"])
def text():
    modelES_d2v_medic = models.Doc2Vec.load(os.path.join(os.path.join(model_path,"model"), 'train-ES_d2v_medic.model'), mmap='r')
    if len(request.args) < 1 or request.args.get("text") == '' :
        return json.dumps('Please enter arguments in order /medic?text=''TEXT''')
    text = request.args.get("text")
    text = text.lower()
    model_name = 'train-ES_d2v_medic'
    logfunction()

    if os.path.exists(model_path + '/'+ model_name+ '.model'):
        tokens = sentence_to_words(text)
        new_vector = modelES_d2v_medic.infer_vector(tokens)
        try:
            best = modelES_d2v_medic.docvecs.most_similar([new_vector])[:10]
            logging.info(best)
            return jsonify(best)
        except:
            logging.info('No similar word found for the word : %s' ,text)
            return json.jsonify(error=404, text=str('No similar word found in domain for the word : '+text)), 404
    else:
        logging.info('No model found for the domain')
        return json.jsonify(error=404, text=str(model_name+'- No model found')), 404

@app.route("/w2gfunctionalpoint", methods=["GET"])
def wgfunctionalpoint():
    modelES_w2v_medic = models.Word2Vec.load(os.path.join(os.path.join(model_path,"model"), 'w2g_function_point_w2v.model'), mmap='r')
    if len(request.args) < 1 or request.args.get("text") == '' :
        return json.dumps('Please enter arguments in order /medic?text=''TEXT''')
    text = request.args.get("text")
    text = text.lower()
    model_name = "w2g_function_point_w2v"
    logfunction()

    if os.path.exists(model_path + '/'+ model_name + '.model'):
        try:
            words = modelES_w2v_medic.most_similar(text)
            return jsonify(words)
        except:
            logging.info('No similar word found for the word : %s' ,text)
            return json.jsonify(error=404, text=str('No similar word found in domain for the word : '+text)), 404
    else:
        logging.info('No model found for the domain')
        return json.jsonify(error=404, text=str(model_name+'- No model found')), 404


@app.route("/getlogs", methods=["GET"])
def getlogs():
    if len(request.args) < 1 or request.args.get("user") == '':
        return json.dumps('Please enter arguments in order /medic?user=''TEXT''')
    index = request.args.get("user")
    models = []
    logs = []
    for f in next(os.walk(model_path))[2]:
        if f.startswith(index):#index in f:
            created_date = time.ctime(os.path.getmtime(model_path + "/" + f))
            models.append({f: created_date})

    for f in next(os.walk(log_path))[2]:
        if index in f:
            created_date = time.ctime(os.path.getmtime(log_path + "/" + f))
            logs.append({f: created_date})

    resp = {"logs": logs[-5:], "models": models}
    return json.dumps(resp)


@app.route("/getdropbox", methods=["GET"])
def getdropbox():
    if len(request.args) < 1 or request.args.get("user") == '':
        return json.dumps('Please enter arguments in order /getdropbox?user=''TEXT''')
    index = request.args.get("user")

    resp = {}
    try:
        files = []
        for f in dbx.files_list_folder("/rr2-"+index).entries:
            files.append(f.name)
        resp.update({index : files})
    except:
        resp.update({index : []})

    return json.dumps(resp)


@app.route("/top_records", methods=["GET"])
def top_records():
    if len(request.args) < 2:
        return json.dumps('Please enter arguments in order /top_records?user=''user'',project=''project'',query=''query''')

    count_index = "api_load_count"
    res = es.get(index=count_index, doc_type="count", id="AWMfcwefJx9yO3re85E3", request_timeout=100)
    res = res['_source']
    logfunction()

    try:
        current_date = datetime.datetime.now().date()
        count = res[str(current_date)]
        print(current_date)
        print(count)
        res.update({str(current_date) : int(count) + 1})
    except KeyError:
        count = 1
        res.update({str(current_date) : count})

    if count < MAX_API_CALL_LIMIT:
        es.update(index=count_index, doc_type="count", body={"doc" : res}, id="AWMfcwefJx9yO3re85E3", request_timeout=100)

        user    = request.args.get("user")
        project = request.args.get("project")
        query   = request.args.get("query")

        user    = user.lower()
        project = project.lower()
        query   = query.lower()
        index_name = user+"-"+project

        doc = {"_source": {
            "includes": ["score_ml", "sme", "score_naive", "score_nn", "score_tree", "predict02", "date", "description",
                         "entity_person", "entity_location", "entity_org", "type"]},
               "sort": [{"date": {"order": "desc"}, "score_ml": {"order": "desc"}}],
               "query": {"bool": {"must": [{"multi_match": {"query": query, "type": "most_fields", "minimum_should_match": "25%",
                                  "fields": ["ml_aggregrate^5", "title^5", "entity_location^1", "entity_org^1",
                                  "entity_person^1"]}},{"range": {"score_ml": {"gte": 3}}}],
                                  "should": {"match": {"ml_aggregrate": query}}}}}
        try:
            page = es.search(index=index_name, body=doc, size=100, request_timeout=100)
            resp = [p['_source'] for p in page['hits']['hits'] if p['_source']]
            return jsonify(resp)
        except:
            logging.info('No Record Found For The Query Term')
            return json.jsonify(error=404, text=str(query + '- No Records Found')), 404
    else:
        logging.info('Experimental RedRabbit Rest API Limit Reached')
        return json.jsonify(error=402, text=str('Experimental RedRabbit Rest API Limit Reached')), 200

# @app.route("/is_model_exist", methods=["GET"])
# def is_model_exist():
#     print("In is model exist api")
#     if len(request.args) < 1 or request.args.get("user") == '':
#         return json.dumps('Please enter arguments in order /is_model_exist?user=''TEXT''')
#     user = request.args.get("user")
#     print(user)
#     time.sleep((0.1))
#     if os.path.exists(model_path + '/' + user+'_sentence_d2v.model'):
#         return json.dumps({"success" : True})
#     else:
#         time.sleep(0.1)
#         return json.jsonify(error=404, text=str(user+'- No model found')), 404

@app.route("/setmode", methods =["POST"])
def setmode():
    resp = request.get_data()
    data = json.loads(resp.decode('utf-8'))
    project_name = data['project']
    user = data['user']
    print(data)
    active = data['isactive']
    print(active)
    if active == True:
        doc ={"script": {"source": "ctx._source.isactive= false "}, "query": {"bool": {"filter": {"term": {"project" : data["project"]}},"must" :[{"term" : {"user": data["user"]}}]}}}
    else:
        doc ={"script": {"source": "ctx._source.isactive= true "}, "query": {"bool": {"filter": {"term": {"project" : data["project"]}},"must" :[{"term" : {"user": data["user"]}}]}}}
        # doc ={"script": {"inline": "ctx._source.isactive= 'true' "}, "query": {"match": {"guid": data['guid']}}}
    print(doc)
    es.update_by_query(index="crawling-pattern", doc_type="project", body=doc)
    return ""

@app.errorhandler(404)
def page_not_found(e):
    return json.jsonify(error=404, text=str(e)), 404


def find(name, path):
    results = []
    for root, dirs, files in os.walk(path):
        for i in files:
            if re.search(name, i):
                rm_path = os.path.join(path, i)
                os.remove(rm_path)
                results.append(i)
    return results


def delete_raw_index_name(project, database,filename):
    print(database)
    doc = {key: value for key, value in database.items() if not re.search(project, key)}
    print(doc)
    database.update(doc)
    db_path = os.path.join(os.path.dirname((os.path.abspath(database_path))), "database")
    file = open(os.path.join(db_path, filename), "w")
    if database == processed_index_db:
        file.write("processed_index_db=" + repr(doc) + '\n')
    else:
        file.write("raw_index_db=" + repr(doc) + '\n')
    file.close()


@app.route("/deletefiles", methods=["POST"])
def deletemodellog():
    resp = request.get_data()
    data = json.loads(resp.decode('utf-8'))
    project_name = data['project']
    user = data['user']
    project = user + "-" + project_name
    print(project)
    delete_raw_index_name(project, processed_index_db, "processed_index_db.py")
    delete_raw_index_name(project, raw_index_db, "raw_index_db.py")
    result = find(project, model_path)
    print(model_path,result)
    result = find(project, log_path)
    print(log_path, result)
    return ""

@app.route("/aggregate", methods=["GET"])
def aggregate_data():
    if len(request.args) < 2:
        return json.dumps('Please enter arguments in order /aggregate?index-name=''user-project'',keyword=''key-word'',number_of_days=''now-(number_of_days)d/d''')

    count_index = "api_load_count"
    res = es.get(index=count_index, doc_type="count", id="AWMfcwefJx9yO3re85E3", request_timeout=100)
    res = res['_source']

    try:
        current_date = datetime.datetime.now().date()
        count = res[str(current_date)]
        print(current_date)
        print(count)
        # res.update({str(current_date) : int(count) - 1})
    except KeyError:
        count = 1
        res.update({str(current_date) : count})

    if count < MAX_API_CALL_LIMIT:
        # es.update(index=count_index, doc_type="count", body={"doc" : res}, id="AWMfcwefJx9yO3re85E3", request_timeout=100)

        index_name    = request.args.get("index-name")
        no_of_days   = request.args.get("number_of_days")
        keyword = request.args.get("keyword")
        recent = no_of_days
        print("index-name " + index_name + " keyword "+ keyword + " number of days " + no_of_days)
        response ={
            "aggregation":[]
        }
        doc = {
              "query": {
                "bool": {
                  "must": [
                    {
                      "range": {
                        "date": {
                          "gte": recent,
                          "lte": "now/d",
                          "format": "epoch_millis"
                        }
                      }
                    }
                  ],
                  "must_not": [],
                  "filter": {
                    "term": {
                        "ml_aggregrate": keyword
                    }
                  }
                }
              },
              "size": 0,
              "_source": {
                "excludes": []
              },
              "aggs": {
                "2": {
                    "date_histogram": {
                        "field": "date",
                        "interval": "7d",
                        "min_doc_count": 1
                    }
                }
              }
            }

        try:
            page = es.search(index=index_name, body=doc, request_timeout=100)
            for index in page['aggregations']:
                bucket_array = page['aggregations'][index]['buckets']
                print(len(bucket_array))
                for index_in_aggregate in range(0,len(bucket_array)):
                    resp = {
                    "date": "",
                    "doc_count": 0
                }
                    resp['date'] = bucket_array[index_in_aggregate]['key_as_string'].split('T')[0]
                    resp['doc_count'] = bucket_array[index_in_aggregate]['doc_count']
                    response["aggregation"].append(resp)
            response = json.jsonify(response)
            return response
        except:
            logging.info('No Record Found For The Query Term')
            return json.jsonify(error=404, text=str(index_name +" "+ no_of_days + '- No Records Found')), 404
    else:
        logging.info('Experimental RedRabbit Rest API Limit Reached')
        return json.jsonify(error=402, text=str('Experimental RedRabbit Rest API Limit Reached')), 200


@app.route("/getrunningstatus",methods=["GET"])
def runningstatus():
    k = status()
    return k

@app.route("/hello", methods = ["GET"])
def hello():
    print("working")
    return "working"


def main():
    app.run(host=hostname)


if __name__ == "__main__":
    main()

