from flask import Flask
from flask_cors import CORS
from flask import json
from flask import request
from flask import jsonify
import os
import time
import datetime
import gensim
from gensim import models
import logging
import re
import dropbox
# from elasticsearch import Elasticsearch
from nltk.corpus import stopwords
from CONNECTIONS.ESConnection import *
from sklearn.externals import joblib
import gensim.downloader as api
info = api.info()  # show info about available models/datasets
print("Before load")
glove_twitter_25_model = api.load("glove-twitter-25")  # download the model and return as object ready for use
print("After load")


app = Flask(__name__)
model_path = os.getenv("MODELS", "../../MODEL")
log_path = os.getenv("LOGS", "../../LOGS")
stops = set(stopwords.words("english"))

modelWiki_d2v     = models.Doc2Vec.load(os.path.join(model_path, 'Sentence_d2v.model'), mmap='r')
# modelES_d2v_medic = gensim.models.Doc2Vec.load(os.path.join(model_path, 'train-ES_d2v_medic.model'), mmap='r')
# modelES_w2v_medic = gensim.models.Word2Vec.load(os.path.join(model_path, 'w2g_function_point_w2v.model'), mmap='r')
# glove_twitter_25_model = joblib.load(model_path+ "/" + 'glove-twitter-25.model')##api.load("glove-twitter-25")#load glove vectors

#current_date = datetime.datetime.now().date()
# token = "3_C_Ykj6AfQAAAAAAAAi6bQsDghRNAB1H3DRpPZu4YwxC8h5stkArcVvwafAi9k_"
# dbx = dropbox.Dropbox(token)
# print("--account info--", dbx.users_get_current_account().account_type)

# es = Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
logfile = log_path+"/"+"restapi_w2v.log"
logging.basicConfig(filename=logfile,level=logging.DEBUG)
app = Flask(__name__)
CORS(app)
MAX_API_CALL_LIMIT = 100

def sentence_to_words( raw_sentence ):
    letters_only = re.sub("[^a-zA-Z0-9\u00E4\u00F6\u00FC\u00C4\u00D6\u00DC\u00df]", " ", raw_sentence)
    words = letters_only.lower().split()
    meaningful_words = [w for w in words if not w in stops]
    return(meaningful_words)


@app.route("/tw-gensim", methods=["GET"])
def genericSimilar():
    print("In genericSimilar")
    if len(request.args) < 1 or request.args.get("word") == '' :
        return json.jsonify(error=500, text='Please enter arguments in order /generic-similar?word=''WORD''?model=''project_name'''), 500
    text = request.args.get("word")
    text = text.lower()
    model_name = request.args.get("model")
    # print(model_name)
    # sm = model_gen.most_similar(text)
    # print(sm)
    try:
        if not model_name:
            synonyms = glove_twitter_25_model.most_similar(text, topn=10)
            logging.info('Most similar word for '+"glove_twitter_25"+ '-' +text+ 'are : %s', synonyms)
            return jsonify(synonyms)
        else:
            logging.debug(model_name)
            if os.path.exists(model_path + '/' + model_name + '_w2v.model'):
                print("In IF")
                model_tw_w2v = models.Word2Vec.load(os.path.join(model_path, model_name + '_w2v.model'), mmap='r')
                print("before inner try")
                try:
                    synonyms = model_tw_w2v.most_similar(text, topn=10)
                    logging.info('Most similar word for ' + model_name + '-' + text + 'are : %s', synonyms)
                    return jsonify(synonyms)
                except:
                    logging.info('No similar word found in domain %s for the word : %s', model_name, text)
                    return json.jsonify(error=404, text=str(
                        'No similar word found in domain ' + model_name + ' for the word : ' + text)), 404
            else:
                logging.info('No model found for the domain %s', model_name)
                return json.jsonify(error=404, text=str(model_name + '- No model found')), 404
    except:
        logging.info('No similar word found in domain %s for the word : %s' ,"glove_twitter_25",text)
        return json.jsonify(error=404, text=str('No similar word found in domain '+"glove_twitter_25"+' for the word : '+text)), 404


# @app.route("/is_model_exist", methods=["GET"])
# def is_model_exist():
#     print("In is model exist api")
#     if len(request.args) < 1 or request.args.get("user") == '':
#         return json.dumps('Please enter arguments in order /is_model_exist?user=''TEXT''')
#     user = request.args.get("user")
#     print(user)
#     time.sleep((0.1))
#     if os.path.exists(model_path + '/' + user+'_sentence_d2v.model'):
#         return json.dumps({"success" : True})
#     else:
#         time.sleep(0.1)
#         return json.jsonify(error=404, text=str(user+'- No model found')), 404


@app.errorhandler(404)
def page_not_found(e):
    return json.jsonify(error=404, text=str(e)), 404

def main():
    app.run(host='0.0.0.0', debug=True)

if __name__ == "__main__":
    main()

