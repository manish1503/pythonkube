import os
from gensim.models import Doc2Vec
import datetime
import multiprocessing
import matplotlib
matplotlib.use("Agg")
from SCHEDULE.get_logger import get_log_object
from CLASSIFIER.batch_test_sent_vec import start_domain_selection_batch_test

w2v_dimension = 50
w2v_windown = 5
num_workers = multiprocessing.cpu_count()
doc_min = 2
doc_iter = 30

MIN_WORDS = 50  # ignore file with fewer words
sentences_d2v = []  # Initialize an empty list of sentences

today_date = datetime.datetime.now().strftime("%Y-%m-%d")
log = get_log_object("../LOGS", "d2v")
print("StartTime : {}".format(datetime.datetime.now()))

def build_sent_vec_model(sentences_d2v, test_cases, project_name):
    print("************Creating Sentence2Vec Model**********************")

    print("*--------Training Start--------*")
    log.info("*--------Training Start--------*")

    print("------test sentences------------ : ", test_cases)
    train_document_model(sentences_d2v, project_name)
    start_domain_selection_batch_test(test_cases, project_name)

    log.info("*--------Model Created--------*")
    print("*--------Model Created--------*")
    return True


def train_document_model(sentences_d2v, project_name):
    print("***************saving model******************")
    log.debug("***************saving model******************")

    model_d2v = Doc2Vec(sentences_d2v, size=w2v_dimension, window=w2v_windown, workers=num_workers, min_count=doc_min,
                        iter=doc_iter)
    #''' temprarory model training code added'''
    #model_d2v.delete_temporary_training_data(keep_doctags_vectors=True, keep_inference=True)
    model_name = project_name + "_"+ 'sentence_d2v.model'

    ### rename existing file, before replace ###
    for f in next(os.walk("../MODEL"))[2]:
        if f.startswith(project_name+"_"+"sent"):
            log.info(f)
            src = "../MODEL" + '/' + f
            if os.path.exists(src):
                dest = "../MODEL" + "/" + str(today_date) + "_" + f
                try:
                    os.rename(src, dest)
                except:
                    os.remove(dest)
                    os.rename(src, dest)
                log.info("model exist, version updated with date for model %s", str(model_name))

    ###Storing Model in Disk ###
    model_d2v.save(os.path.join("../MODEL", model_name))

    log.info('Model files are created, Model is Ready')
    log.info("\n******* copying model to final destination %s ***********", str(model_name))

    print("Model files are created \n******* copying model to final destination ***********", str(model_name))

    log.info("\n******* model copied to final destination for domain %s *********", str(model_name))
    log.info("Sent2Vec Process finished")
    print("\n******* model copied to final destination for domain *********\n Sent2Vec Process finished", str(model_name))
