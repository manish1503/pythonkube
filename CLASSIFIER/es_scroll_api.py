import datetime
from gensim.models.doc2vec import LabeledSentence
# from elasticsearch import Elasticsearch
from CONNECTIONS.ESConnection import *
from CLASSIFIER.utils import filter_stop_words
from CLASSIFIER.decision_tree_classifier import prepare_feature_set
from CLASSIFIER.sent_to_vec import build_sent_vec_model
from CLASSIFIER.word_to_vec import build_word_vec_model
from SCHEDULE.get_logger import get_log_object

# es = Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
current_date = datetime.datetime.now().date()
log = get_log_object("../LOGS", "scroll_api")

sentence_d2v        = []
words_w2v           = []
test_cases          = []
labeled_data       = []

def es_scroll_api(index_name):
    page = es.search(
        index=index_name,
        scroll='60s',
        size=100,
        request_timeout = 100)

    log.info("scrolling index : %s", index_name)
    sid = page['_scroll_id']
    scroll_size = page['hits']['total']

    print("----", scroll_size)
    log.info("---- %s, %s", scroll_size)

    write_to_file(page)

    while (scroll_size > 0):
        log.info("Scrolling...")
        page = es.scroll(scroll_id=sid, scroll='60s',request_timeout=100)
        sid = page['_scroll_id']
        scroll_size = len(page['hits']['hits'])

        log.info("scroll size: %s" + str(scroll_size))
        print("scroll size: " + str(scroll_size))
        if scroll_size > 0 :
            write_to_file(page)

    print("-----requesting word2vec (training and testing)-------")
    log.info("-----requesting word2vec (training and testing)-------")
    build_word_vec_model(words_w2v, test_cases, index_name)

    print("-----requesting sent2vec (training and testing)-------")
    log.info("-----requesting sent2vec (training and testing)-------")
    build_sent_vec_model(sentence_d2v, test_cases, index_name)

    print("-------requesting decision tree (training and testing)-------")
    log.info("-------requesting decision tree, bayesian and regression (training and testing)-------")
    prepare_feature_set(index_name, labeled_data)

    log.info("----es-scroll_api---Complete---")
    return True


def write_to_file(page):
    iter_count = 0
    log.info("scrolling records and preparing labeled data")

    for item in page['hits']['hits']:
        ml_aggr     = ''
        sme         = 0
        label       = "no-subject"

        if item['_source']:
            if "ml_aggregrate" in item['_source']:
                ml_aggr = str(item['_source']['ml_aggregrate'])
            sme = str(item['_source']['sme'])
        iter_count += 1

        try:
            if int(sme) >= 4:
                label = "subject"
        except:
            pass
        mlwords = filter_stop_words(ml_aggr)

        if 1==1:#len(mlwords) > 200:

            words_w2v.append(mlwords)
            sentence_d2v.append(LabeledSentence(mlwords, [u'{}'.format(label)]))
            labeled_data.append((ml_aggr, label))

            if iter_count<=2:
                test_cases.append(ml_aggr[:200])
        else:
            log.warning("record found with less than 200 words")

    return True