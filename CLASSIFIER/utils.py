import re
from collections import defaultdict
from nltk.corpus import stopwords
stops = set(stopwords.words("english"))

##------------------
def filter_stop_words(raw_sentence):
    letters_only = re.sub("[^a-zA-Z0-9\u00E4\u00F6\u00FC\u00C4\u00D6\u00DC\u00df]", " ", raw_sentence)
    words = letters_only.lower().split()
    meaningful_words = [w for w in words if not w in stops]
    return (meaningful_words)


##------------------
def most_frequent_words(texts):
    resp = []
    frequency = defaultdict(int)
    for text in texts:
        for token in text.split():
            frequency[token] += 1

    t = []
    for i in frequency:
        if frequency[i] > 5:
            t.append(i)

    freq_tokens = list(set(t))
    print("-----freq_tokens----", freq_tokens)

    if len(freq_tokens) < 10:
        print("****size of most frequent words in user (less than ten) is ******", len(freq_tokens))
        for freq_token in freq_tokens:
            resp.append(freq_token)#(model_name, freq_token))

    else:
        print("****size of most frequent words in user are ****** out of total words",len(freq_tokens), len(frequency))
        for freq_token in freq_tokens[:10]:
            resp.append(freq_token)#(model_name, freq_token))

    return resp


##-------------- some text processing ---------------##
def cleanText(corpus):
    punctuation = """.,?!:;(){}[]"""
    corpus = [z.lower().replace('\n','') for z in corpus]
    corpus = [z.replace('<br />', ' ') for z in corpus]

    # treat punctuation as individual words
    for c in punctuation:
        corpus = [z.replace(c, ' %s '%c) for z in corpus]
    corpus = [z.split() for z in corpus]
    return corpus
