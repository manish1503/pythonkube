import time
import csv
import datetime
import os
import gensim
import logging

today_date = datetime.datetime.now().strftime("%Y-%m-%d")
logfile = "../LOGS"+ "/"+ today_date+"_batch_csv_error_log"+ ".log"
logging.basicConfig(filename=logfile, level=logging.DEBUG)

def word_similarity(user_name, word):
    start_time = time.time()
    csv_data = []
    if os.path.exists("../MODEL" + '/' + user_name + '_w2v.model'):
        modelWiki_w2v = gensim.models.Word2Vec.load(os.path.join("../MODEL", user_name + '_w2v.model'), mmap='r')
        try:
            synonyms = modelWiki_w2v.most_similar(word)#, topn=10)
            end_time = time.time()
            time_lapsed = "{:.2f}".format(end_time - start_time)
            dd=[]
            for i in synonyms:
                res = "{:.2f}".format(i[1])
                u = (i[0] + "{" + res + "}")
                dd.append(u)
            csv_data = (today_date, user_name, time_lapsed, word.replace(',',''), str(tuple(dd)).replace(",","|"))#str(synonyms))
            logging.info("Similar Word in Domain: %s for the User : %s",str(synonyms),user_name)
        except:
            end_time = time.time()
            time_lapsed = "{:.2f}".format(end_time - start_time)
            csv_data = (today_date, user_name, time_lapsed, word.replace(',', ''),"Word Not Found-Exception")
            logging.warning("No Simliar Word Found in Domain %s for the word %s",user_name,word)
    else:
        print("No model found for the user :", user_name)
        logging.warning("No Model Found For User: %s",user_name)

    print(csv_data)
    write_csv(csv_data)

##-----------------
def write_csv(data):
    fname =  str(today_date) + "_batch_csv_log" + ".csv"
    path = "../LOGS" + '/' + fname
    header = ('DATE', 'USER NAME', 'TIME LAPSED', 'SENTENCE-WORD', 'RESULT')
    if not os.path.exists(path):
        with open(path, 'a', newline='', encoding="utf-8") as out:
            csv_out = csv.writer(out)
            csv_out.writerow(header)
            csv_out.writerow(data)
    else:
        with open(path, 'a', newline='', encoding="utf-8") as out:
            csv_out = csv.writer(out)
            csv_out.writerow(data)

##------------------
def start_batch_test_w2v(user_name, test_words):
    print("******batch user test starts*********")
    for word in test_words:
        print(word)
        word_similarity(user_name, word)
    print("******test completes*********")
