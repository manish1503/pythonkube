from DATABASE.processed_index_db import *
from FILTER.utils import dict_comparison, \
                         update_modified_processed_db, update_processed_index_db
from CLASSIFIER.list_projects_to_train import get_active_projects
from SCHEDULE.get_logger import get_log_object
log = get_log_object("../LOGS", "classifier")


def index_to_build():
    """
    Getting Updates through Comparison
    :return: True
    """
    d1 = get_active_projects()
    d2 = processed_index_db
    add, remove, modified = dict_comparison(d1, d2)

    print("*--new added--*", add, "\n*--removed--*",remove, "\n*--new modified--*", modified)
    log.info("*--new added--* %s, %s, %s, %s, %s", add, "\n*--removed--*",remove, "\n*--new modified--*", modified)

    mod_list = []
    if modified:
        m_list = update_modified_processed_db(modified)
        mod_list.extend(m_list)
    if add:
        for i in add:
            mod_list.append(i)
            try:
                update_processed_index_db({i: [d1.get(i)[0], d1.get(i)[1]]})
            except:
                print("Alert 1.0 please check.")

    ###To Work On Remove
    print("mod_list : ", mod_list)
    log.info("mod_list %s: ", mod_list)
    return mod_list