# -*- coding: utf-8 -*-
"""
Created on Wed Dec 27 12:23:29 2017

@author: sergio
"""

import requests
import time
import json
import csv
import os
import datetime
from SCHEDULE.get_logger import get_log_object
test_url = "http://40.71.175.166:8082/usersimilar?user="##+user_index+"&word="+word
live_url = "http://52.166.67.3/usersimilar?user="
azure_url = "http://40.71.175.166:5000/usersimilar?user="

csv_data = []
today_date = datetime.datetime.now().strftime("%Y-%m-%d")
filename = 'api_log_user_similarity_'+str(today_date)+'.csv'
log  = get_log_object("../../LOGS", "api_test_word2vec")

def api_test_user_index(user, word, env):
    current_url = ''
    start_time = time.time()

    if env == "live":
        current_url = live_url
    elif env == "azure":
        current_url = azure_url
    elif env == "test":
        current_url = test_url

    headers = {'Authorization': 'Basic YWRtaW46Vm1ScmtueVRqTTJidWJ5Ug=='}
    url = current_url + user + "&word=" + word
    u = ()
    try:
        resp = requests.get(url=url, headers=headers, )
        end_time = time.time()
        log.info("!!!!S!T!A!T!U!S!!!!!C!O!D!E!!!!! %s",resp.status_code)
        time_lapsed = "{:.2f}".format(end_time - start_time)
        if resp.status_code == 404:
            res = json.loads(resp.text)
            u = (today_date, user, time_lapsed, word.replace(',', ''), str(res['text']).replace(",", ""), url)
        else:
            dd = []
            res = json.loads(resp.text)
            for i in res:
                res = "{:.2f}".format(i[1])
                u = (i[0] + "{" + res + "}")
                dd.append(u)
            u = (today_date, user, time_lapsed, word.replace(',', ''), str(tuple(dd)).replace(',', '|'), url)
    except requests.exceptions.ConnectionError:
        u = (today_date, user, "NA", word.replace(',', ''), "Connection Error", url)

    log.info(u)
    write_csv(u)


def write_csv(data):
    path = "../LOGS"+'/'+filename
    if not os.path.exists(path):
        header = ('DATE', 'USER NAME', 'TIME LAPSED', 'SENTENCE-WORD', 'RESULT', 'ENV')
        with open(path, 'a', newline='', encoding="utf-8") as out:
            csv_out = csv.writer(out)
            csv_out.writerow(header)
            csv_out.writerow(data)
    else:
        with open(path, 'a', newline='', encoding="utf-8") as out:
            csv_out = csv.writer(out)
            csv_out.writerow(data)

def start_api_test_user(user, test_words):
    log.info("******* api test starts*********")
    env = "azure"
    for word in test_words:
        print(user, word)
        api_test_user_index(user, word, env)
    log.info("******* completes*********")
