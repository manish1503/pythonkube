import random
from gensim.models import doc2vec
import numpy as np
from math import floor
import os
import datetime

from sklearn.tree import DecisionTreeClassifier
from sklearn.cross_validation import train_test_split
from sklearn.externals import joblib
from  sklearn import preprocessing
from sklearn import linear_model
from sklearn.naive_bayes import GaussianNB
from SCHEDULE.get_logger import get_log_object

today_date  = datetime.datetime.now().date()
log         = get_log_object("../LOGS", "classifiers")


def prepare_feature_set(index_name, labled_data):
    model = doc2vec.Doc2Vec.load("../MODEL" + "/" + index_name+'_sentence_d2v.model')
    random.shuffle(labled_data)
    log.info("preparing train and test data set for index : %s", index_name)

    train_corpus = labled_data[0 : floor(3*len(labled_data)/4)]
    test_corpus  = labled_data[floor(3*len(labled_data)/4) : ]

    train_targets, train_regressors = zip(*[(doc[0], doc[1]) for doc in train_corpus])
    test_targets, test_regressors   = zip(*[(doc[0], doc[1]) for doc in test_corpus])

    X = []
    for i in range(len(train_targets)):
        X.append(model.infer_vector(train_targets[i]))

    train_x  = np.asarray(X)
    Y = np.asarray(train_regressors)

    le = preprocessing.LabelEncoder()
    le.fit(Y)
    train_y = le.transform(Y)

    logreg = linear_model.LogisticRegression()
    logreg.fit(train_x, train_y)

    clf_nb = GaussianNB()
    clf_nb.fit(X, Y)
    GaussianNB(priors=None)

    ''' Scikit-learn method to implement the decsion tree classifier '''
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.3, random_state=100)

    ''' Decision model with Gini index critiria '''
    decision_tree_model = DecisionTreeClassifier(criterion="gini", random_state=100, max_depth=3, min_samples_leaf=5)
    decision_tree_model.fit(X_train, y_train)

    log.info("--train_x shape-- : %s",train_x.shape)
    log.info("train_x.dtype.names : %s",train_x.dtype.names)
    log.info("----Y----: %s \n", Y)
    log.info(np.mean(train_y))
    log.info("Decision Tree classifier :: %s", decision_tree_model)

    log.info("storing classifiers")
    for f in next(os.walk("../MODEL"))[2]:
        if f.startswith(index_name+"-"):
            src = "../MODEL" + '/' + f
            log.info("renaming files : %s", src)
            if os.path.exists(src):
                dest = "../MODEL" + "/" + str(today_date) + "_" + f
                try:
                    os.rename(src, dest)
                except:
                    os.remove(dest)
                    os.rename(src, dest)

    joblib.dump(logreg, "../MODEL"+"/"+index_name+'-logistic.model')
    joblib.dump(clf_nb, "../MODEL"+"/"+index_name+'-bayesian.model')
    joblib.dump(decision_tree_model, "../MODEL"+"/"+index_name+'-tree.model')

    log.info("classifiers built")

    test_list = []
    for i in range(len(test_targets)):
        test_list.append(model.infer_vector(test_targets[i]))
    test_x = np.asarray(test_list)

    test_Y = np.asarray(test_regressors)
    test_y = le.transform(test_Y)

    preds = logreg.predict(test_x)

    log.info(np.mean(test_y))
    print(sum(preds == test_y) / len(test_y))
    print("LR Classification Completes")
    log.info("|----|----|----|--LR Classification Completes--|----|----|----|----|")
