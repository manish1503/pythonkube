import logging
import os
from gensim.models import Word2Vec ##dynamic based in 15,16
import datetime
import multiprocessing
from CLASSIFIER.utils import most_frequent_words
from CLASSIFIER.batch_test_word_vec import start_batch_test_w2v

w2v_dimension = 50 ###100 ##dynamic based on 15,16 (size)
w2v_windown = 5
doc_min = 5
doc_iter = 30 ###dynamic based on 15,16 (size)
num_workers = multiprocessing.cpu_count()

model_type = 'word2vec' ##above 5GB
#model_type = 'glov2vec' ##below 5GB

number_of_files = 0
min_words_files = 0
MIN_WORDS = 100
words_w2v = []
texts = []
logfile = "../LOGS" + "/"+ "word2vec_log.log"
logging.basicConfig(filename=logfile, level=logging.DEBUG)
today_date = datetime.datetime.now().strftime("%Y-%m-%d")
dest_model_path = "../MODEL" ##for api test

##-----------------
def build_word_vec_model(words_w2v, texts, project_name):
    print("Training Model Invoked")
    logging.info("Training Model Invoked")

    test_words = most_frequent_words(texts)
    print("********----!!!!!!!!!!!!most frequent!!!!!!!!!!!!!!!!!!!!----*******",test_words)
    logging.info("********----!!!!!!!!!!!!most frequent!!!!!!!!!!!!!!!!!!!!----******* %s", test_words)

    """ training starts """
    train_res = train_model(words_w2v, project_name)

    print("*******batch testing starts for user : **************", project_name)
    logging.info("*******batch testing starts for user : ************** %s", project_name)
    start_batch_test_w2v(project_name, test_words)

    #print("*******api testing starts for user : **************", project_name)
    #start_api_test_user(project_name, test_words)
    logging.info("Done !!")
    print("Done!!")


##-------------------
def train_model(words_w2v, model_file_name):
    """stores the model in disk for
        future use"""
    ### rename existing file, before replace ###
    for f in next(os.walk("../MODEL"))[2]:
        if f.startswith(model_file_name+"_"+"w2v"):
            logging.info("renaming file : %s",f)

            src = "../MODEL" + '/' + f
            if os.path.exists(src):
                dest = "../MODEL" + "/" + str(today_date) + "_" + f
                try:
                    os.rename(src, dest)
                except:
                    os.remove(dest)
                    os.rename(src, dest)
                logging.info("model exist, version updated with date for model %s", str(model_file_name))


    model_w2v = Word2Vec(words_w2v, size=w2v_dimension, window=w2v_windown, workers=num_workers, iter=doc_iter)
    model_w2v.save(os.path.join("../MODEL", model_file_name + '_w2v.model'))

    logging.info('Model has been created')
    return True