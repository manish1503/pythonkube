import time
import csv
import datetime
import os
import gensim
import logging
from CLASSIFIER.utils import filter_stop_words
from SCHEDULE.get_logger import setup_logger

today_date = datetime.datetime.now().strftime("%Y-%m-%d")
logfile = "../LOGS"+ "/" +today_date+"_sent_vec_test.log"
setup_logger('wiki', logfile)
log = logging.getLogger('wiki')

model_path = os.getenv("MODEL", "../MODEL")


def domain_selection_wiki(sentence, project_name):
    start_time = time.time()
    csv_data = []
    sentence = sentence.lower()
    model_name = project_name + "_sentence_d2v"
    if os.path.exists(model_path + '/' + model_name + '.model'):
        log.info("-------------***********nnnnnn************--------------")
        modelWiki_d2v = gensim.models.Doc2Vec.load(os.path.join(model_path, model_name + '.model'), mmap='r')
        try:
            tokens = filter_stop_words(sentence)
            new_vector = modelWiki_d2v.infer_vector(tokens)
            synonyms = modelWiki_d2v.docvecs.most_similar([new_vector])
            log.debug("<---result---> %s", synonyms)
            end_time = time.time()
            time_lapsed = "{:.2f}".format(end_time - start_time)
            dd=[]
            for i in synonyms:
                res = "{:.2f}".format(i[1])
                u = (i[0] + "{" + res + "}")
                dd.append(u)
            csv_data = (today_date, model_name, time_lapsed, sentence[:200].replace(',',''), str(tuple(dd)).replace(",","|"))#str(synonyms))
            log.info("Similarity in Domain: %s for the sentence : %s",str(synonyms),sentence)
        except:
            end_time = time.time()
            time_lapsed = "{:.2f}".format(end_time - start_time)
            csv_data = (today_date, model_name, time_lapsed, sentence[:200].replace(',',''), "Similarity Not Found-Exception")
            log.warning("No Simliarity Found in Domain %s for the sentence %s",model_name,sentence)
    else:
        log.warning("No Model Found For Sentence: %s",model_name)

    log.info("****result****%s",csv_data)
    write_csv(csv_data)

def write_csv(data):
    fname =  str(today_date) + "_domain_similar_batch_csv_log" + ".csv"
    header = ('DATE', 'MODEL NAME', 'TIME LAPSED', 'SENTENCE', 'RESULT')
    if not os.path.exists("../LOGS" + '/' + fname):
        with open(model_path + '/' + fname, 'a', newline='', encoding="utf-8") as out:
            csv_out = csv.writer(out)
            csv_out.writerow(header)
            csv_out.writerow(data)
    else:
        with open("../LOGS" + '/' + fname, 'a', newline='', encoding="utf-8") as out:
            csv_out = csv.writer(out)
            csv_out.writerow(data)


def start_domain_selection_batch_test(test_cases, project_name):
    log.info("***********sent2vec :: Batch test(domain select) starts**************")
    for case in test_cases:
        domain_selection_wiki(case, project_name)
    log.info("*******sent2vvec :: completes*************")