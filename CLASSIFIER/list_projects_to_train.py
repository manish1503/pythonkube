import datetime
# import elasticsearch
from SCHEDULE.get_logger import get_log_object
from CONNECTIONS.ESConnection import *

# es = elasticsearch.Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
current_date = datetime.datetime.now().date()
log = get_log_object("../LOGS", "classifier")


def get_active_projects():
    doc = {"query": {"bool": {"filter": {"term": {"isactive": True } } } } }
    try:
        resp_data = es.search(index="crawling-pattern", body=doc,request_timeout=100)
    except:
        return

    log.info("searching active projects")
    indexes = []
    for res in resp_data["hits"]["hits"]:
        user    = res['_source']['user']
        project = res['_source']['project']
        indexes.append((user, project))


    processed_db = get_index_db(indexes)
    log.info("list of indexes : %s, and processed db : %s", indexes, processed_db)
    print(indexes, "\n",processed_db)

    return processed_db

##------------------
def get_index_db(indexes):
    live_processed_db = {}

    for i in indexes:
        no_records, size = get_index_size(i[0]+"-"+i[1])
        if int(no_records) > 0:
            live_processed_db.update({i[0]+"-"+i[1] : [int(no_records), size]})

    return live_processed_db

##-------------------
def get_index_size(index):
    try:
        log.info("getting size of index : %s", index)
        res = es.cat.indices(index=index,request_timeout=100)
        log.info(res)
        data = res.split()
        return data[6], data[8]
    except:
        return 0, '0mb'