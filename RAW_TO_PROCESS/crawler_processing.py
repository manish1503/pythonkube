import datetime
import json
import time
# from elasticsearch import Elasticsearch

from FILTER.get_context import get_context_terms, load_w2v_model
from READ_DROPBOX.index_requests import index_to_es, index_search, prepare_mapping_doc, is_record_exist
from FILTER.get_classifier_score import load_model
from SCHEDULE.get_logger import csv_log, csv_detailed_log

from CONNECTIONS.ESConnection import *
# es = Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
print(os.getcwd())
data = json.load(open("."+"/RAW_TO_PROCESS/"+"crawler-mapping.json"))
current_date = datetime.datetime.now().date()


def _crawler(crawler_name, index_name, user, project, search_terms, log, model_exist):
    """ Doing Raw TO Process and Pushing Processed Data to ES  """
    label           = "no-" + project
    project_name    = processed_index   = user + "-"+ project
    if model_exist:
        load_model(processed_index)
        load_w2v_model(processed_index)
    try:
        mapping = data[crawler_name]
    except:
        log.warning("No Mapping Found For Crawler :%s", str(crawler_name))
        return False

    resp, total_records = index_search(index_name) #raw index
    #load_model(index_name)
    if resp:
        #csv_detailed_logs = []
        hit_count = 0
        not_indexex = 0
        if(model_exist):
            similar_terms = get_context_terms(search_terms[0], project_name)
        else:
            similar_terms = get_context_terms(search_terms[0])
        for i in resp:
            hit_count += 1
            guid = i['_id']

            ''' checking if record already exist in processed index '''
            is_record = is_record_exist(processed_index, "processed", guid)
            if is_record:
                log.warning("record already exist : %s in processed index", guid)
                continue

            log.info("!-!-!-------- adding from raw record no :------!-!-! %s , %s", hit_count, crawler_name)
            ''' data cleansing and es mapping '''
            doc, val = prepare_mapping_doc(i, mapping, label, project_name, crawler_name, model_exist, search_terms, similar_terms)
            #log.warning("doc : %s, %s, %s", processed_index, doc, guid)

            if not val:
                continue
            success_flag = index_to_es(processed_index, 'processed', guid, doc, "", "", log) #adding a row from raw to process

            #sf_val = "SUCCESS" if success_flag else "FAILED"
            #uu = (str(current_date), index_name, guid, sf_val)
            #csv_detailed_logs.append(uu)

            if  not success_flag:
                not_indexex += 1
                log.warning("!.......record not appended from raw to process.......! %s", not_indexex)
                continue

        log.info("indexed count :%s, not indexed count : %s, Records processed for : %s", str(hit_count), not_indexex, str(index_name))
        #u = (str(current_date), index_name, 0, int(total_records) + int(0), hit_count, not_indexex)

        '''logging in csv files'''
        #csv_log("../LOGS"+"/"+str(current_date)+"_crawlers_details.csv", u)
        #csv_detailed_log("../LOGS"+"/"+str(current_date)+"_crawlers_detailed_log.csv", csv_detailed_logs)

        #log.info("CSV log appended.")
        return True
    else:
        log.warning("No records to process")
        return False

