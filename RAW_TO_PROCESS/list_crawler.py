import json
#from elasticsearch import Elasticsearch
import datetime
import os
import sys
import time
import threading
sys.path.append(os.getcwd())
from FILTER.get_classification_score_api import is_model_exist_fs_check
from RAW_TO_PROCESS.crawler_processing import _crawler
from FILTER.utils import update_raw_index_db
from DATABASE.raw_index_db import *
from READ_DROPBOX.index_requests import delete_index, is_index_exists
from SCHEDULE.get_logger import get_log_object
from CONNECTIONS.ESConnection import *
print(os.getcwd())
#es = Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")

# data = json.load(open("/CODE/RAW_TO_PROCESS/"+"crawler-mapping.json"))
crawlers = ["diffbot", "webhose", "twitter" ,"bing", "urlcrawl"]
current_date = str(datetime.datetime.now().date())


def list_projects():
    #es.cluster.health(wait_for_status='yellow', request_timeout=100)
    doc = {"query": {"bool": { "filter": {"term": {"isactive": True}}}}}
    try:
        resp_data = es.search(index="crawling-pattern", body=doc, size = 100, request_timeout=100)
    except:
        return

    projects = []
    for res in resp_data["hits"]["hits"]:    
        user = res['_source']['user']
        project = res['_source']['project']
        keywords = res['_source']['keyword-search']
        contexts = res['_source']['context-search']

        key_contexts = []
        key_contexts.extend([i.lower().replace('"', '').strip() for i in keywords.split(",") if i])
        key_contexts.extend([i.lower().replace('"', '').strip() for i in contexts.split(",") if i])

        projects.append((user, project, key_contexts))

    print("----",projects)
    return projects


def crawl_index():
    try:
        projects = list_projects()
        #projects = [('sergio', 'luminary', ['naval ravikant', 'block chain', 'internet 3.0', 'angel', 'startup']),('sergio', 'spacex', ['spacex', 'aerospace', 'falcon', 'falcon heavy', 'elon']),('sergio', 'briight', ['diabetes', 'device', 'diabetes', 'briight']),('lou', 'spacex', ['spacex', 'aerospace', 'falcon', 'elon', 'heavy'])]
        #projects = [('paulo', 'parkinson', ['parkinson', 'tremor control', 'freezing of gait', 'gait'])]
        #projects =  [('mark', 'changemaking',['changemakers', 'integrate', 'integral', 'inner', 'conscious', 'practice'])]

    except:
        projects = list_projects()
    if not os.path.exists("../LOGS"):
        os.mkdir("../LOGS")
    if not projects:
        return
    for crawler in crawlers:
        # time.sleep(10)
        for p in projects:
            log = get_log_object("../LOGS",p[0]+"-"+p[1]+"_"+crawler)
            project_name = p[0]+"-"+p[1]+"-"+crawler
            if project_name in raw_index_db:  # trying to find the next index if not present
                i_value     = raw_index_db.get(project_name)[1].split("-", 1)
                i_index     = str(int(i_value[0]) + 1) + "-" + i_value[1]
                raw_index_name = i_index
                # time.sleep(10)
                index_check = is_index_exists(raw_index_name)
                if not index_check:
                    for i in range(2,20):
                        raw_index_name = str(int(i_value[0]) + i) + "-" + i_value[1]
                        index_check = is_index_exists(raw_index_name)
                        if not index_check:
                            continue
                        else:
                            break
            else:
                raw_index_name = "1" + "-" + "raw" + "-" + project_name

            log.info("*!*!*index name!*!* : %s", raw_index_name)

            '''checking if index exists'''
            index_check = is_index_exists(raw_index_name)
            if not index_check:
                log.error("----index not exists---  %s", raw_index_name)
                continue
            log.warning("New index start -----------*------------- %s", {project_name: [current_date, raw_index_name]})

            model_exist = is_model_exist_fs_check(p[0]+"-"+p[1])
            log.info("Model exists %s %s" , p[0]+"-"+p[1] , model_exist )
            scheduling_crawlers(crawler, raw_index_name, p[0], p[1], p[2], project_name, log,model_exist)
            # t = threading.Thread(target=scheduling_crawlers, args=(crawler, raw_index_name, p[0], p[1], p[2], project_name, log))
            # t.start()
            log.warning("Process completed for  index -----------*-------------: %s", raw_index_name)
    log.info("Now waiting for next schedule. . . .")
    return True


def scheduling_crawlers(crawler, raw_index_name, p0, p1, p2, project_name, log,model_exist):
    """scheduling crawlers, crawling indexes"""

    try:
        _crawler(crawler, raw_index_name, p0, p1, p2, log, model_exist)
    except:
        log.warning("Some Error Occured, please check : %s", raw_index_name)
    finally:
        log.warning("updating index to db : %s", raw_index_name)
        print(os.getcwd())
        update_raw_index_db({project_name: [current_date, raw_index_name]})
        raw_index  = raw_index_name.split("-",1)
        index_num  = raw_index[0]
        index_name = raw_index[1]
        log.debug("index num : %s, index name: %s", index_num, index_name)
        del_index  = str(int(index_num)-4)+"-"+ index_name  # keeping the last 5 index
        log.debug("deleting index : %s", del_index)
        delete_index(del_index)
