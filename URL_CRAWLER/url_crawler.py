import uuid
# from elasticsearch import Elasticsearch
import requests
from CONNECTIONS.ESConnection import *
import json
import urllib
import datetime

from DATABASE.raw_index_db import *
from FILTER.utils import update_raw_index_db
from READ_DROPBOX.index_requests import index_check
from MANUAL_PROCESS.read_process_index import read_projects
from SCHEDULE.get_logger import get_log_object

current_date = datetime.datetime.now().date()
log         = get_log_object("../LOGS", "urlcrawler")
# es = Elasticsearch("http://med3d.eastus.cloudapp.azure.com:9220")
diffbot_url = "https://api.diffbot.com/v3/analyze?token=9ade437e27ca6fbb44e57d07415ce806&url="

def index_url_crawl():
    log.info("bing crawlers starts on : %s", str(current_date))
    projects = read_projects()
    #projects = [('boston', 'therapeutic', ['therapeutic', 'vr', '3d glassless', 'autostereoscopic', 'true3d', 'aquirre', 'automultiscopic', 'volumetric 3d'])]
    print("projects : %s", projects)
    for p in projects:
        project = p[0]+"-"+p[1]
        get_urls(project)

    #log.info("process completed")

def get_urls(index):
    links = []
    res = es.search(index="meta-" + index + "-urllist")
    pages = res['hits']['hits']

    for page in pages:
        guid = page['_id']
        if "url" in page['_source']:
            links.append((guid, page['_source']['url']))

    log.info("links :%s",links)
    diffbot_url_crawler(links, index)


def diffbot_url_crawler(urls, index):
    last_updated_index = raw_index_db.get(index+"-"+"urlcrawl")
    if not last_updated_index:
        latest_raw_index = "1" + "-" + "raw" + "-" +index+ "-urlcrawl"
    else:
        index_number = last_updated_index[1].split("-", 1)[0]
        latest_raw_index = str(int(index_number) + 1)+"-raw-"+index+"-urlcrawl"

    log.info("latest_raw_index : %s",latest_raw_index)
    latest_index = {index+"-"+"urlcrawl" : [str(current_date), latest_raw_index]}

    for u in urls:
        parse_resp = req_diffbot(u[1])
        links = []

        try:
            if(len(parse_resp['objects']) == 0) :
                update_es("meta-" + index + "-urllist", u[0], [])
                continue;
            index_mapping(latest_raw_index, parse_resp['objects'][0], u[0])
            tags = []
            if ('tags' in parse_resp['objects'][0] ):
                tags = parse_resp['objects'][0]['tags']
            for tag in tags:
                if float(tag['score']) >= float(0.65):
                    #print("tag score: ", tag['score'])
                    guid = uuid.uuid4()
                    tag_url = tag['uri']

                    resp = req_diffbot(tag_url)
                    try:
                        index_mapping(latest_raw_index, resp['objects'][0], guid)
                        links.append(guid, tag_url)
                    except:
                        continue
        except:
            continue
        ##update urls in es##
        #print(index, u[0],u[1])]
        update_es("meta-" + index + "-urllist", u[0], links)

    try:
        update_raw_index_db(latest_index)
    except:
        pass
    return True


def req_diffbot(url):
    uri = diffbot_url + urllib.parse.quote_plus(url, safe='', encoding=None, errors=None)
    resp = requests.get(url=uri)
    parse_resp = json.loads(resp.text)
    #print(" url", url, "\n response :", parse_resp)
    return parse_resp


def index_mapping(index_name, body_doc, guid):
    index_check(index_name)
    print("In Index Mapping")
    res = es.index(index=index_name, id=guid, doc_type="raw", body=body_doc,request_timeout=100)
    print("after raw created")
    log.info(res['created'])

    return True


def update_es(index, guid, links):
    print(index)
    print(guid)
    print(links)

    try:
        res = es.get(index=index, doc_type='meta', id=guid)
        log.debug("%s,%s", index, guid)

        data = res['_source']
        data.update({'depth_urls' : links})
        data.update({'crawled': "yes"})
        data.update({'lasttimecrawled': current_date})

        es.update(index=index, doc_type="meta", body={"doc": data}, id=guid)
        log.info("urls udpated in es")
    except Exception as e:
        log.error("Error :%s", e.args)
