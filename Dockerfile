FROM python:3.5-slim
RUN apt-get update && apt-get -y install libc-dev \
    && apt-get -y install build-essential \
    && apt-get remove --purge -y libc-dev build-essential && apt-mark showauto && rm -rf /var/lib/apt/lists/* \
	&& mkdir python && cd python && mkdir CODE && mkdir MODEL \
	&& mkdir LOGS && mkdir DATA
COPY requirement.txt ./python/CODE
WORKDIR /python/CODE
RUN pip install --no-cache-dir -r requirement.txt
COPY . .
ARG private_ES_path
ARG PYTHON
ARG RR_TRAINING
ARG CRAWLING_DROPBOX
ENV private_ES_path $private_ES_path
ENV PYTHON $PYTHON
ENV RR_TRAINING $RR_TRAINING
ENV CRAWLING_DROPBOX $CRAWLING_DROPBOX
RUN echo $private_ES_path
EXPOSE 5000
CMD ["sh", "init"]
